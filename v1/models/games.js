var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gamesSchema = new Schema({
	// _id         : {type: Schema.ObjectId},
// 	team		: {type: Number},
    userId 		: {type: Schema.Types.ObjectId,ref:'users'},
    // division 	: {type: Number},
    countPlayers: {type: Number},
    plan		: {
    	type    : {type: Number},
    	winner	: {type: Number},
    	loser	: {type: Number}
    },
    stadium		: {type: String, trim: true},
    date 		: {type: Date},
    fromHours   : {type: Number},
    toHours     : {type: Number},
    text        : {type: String},
//     created		: {
// 		by		: {type: Schema.Types.ObjectId,ref:'users'},
//     	date	: {type: Date, default: Date.now}
//     },
    connectedUsers   : [{type: Schema.Types.ObjectId,ref:'users'}],
    // opponent    : {type: Boolean, default: false },
    opponentId   : {type: Schema.Types.ObjectId,ref:'users'},
    comments    : [{type: Schema.Types.ObjectId,ref:'comments'}],
    
    status 		: {type: Number, default: 0},
    create_date : {type: Date, default: Date.now}
});

module.exports = mongoose.model('games',gamesSchema);