app.controller('AppController',function($http,$scope,$state,$location,$filter,$rootScope,DOMAIN,authService,$timeout,socket){
    $scope.subscribedRooms = [];
    socket.on('subscribed-rooms',function(data){
    	$scope.subscribedRooms.push(data);
    });
    
    
    $scope.notifications = [];
    $scope.logout = function(){
		authService.logOut()
		$state.go('dashboard', {}, { reload: true });
	}
	
	$rootScope.$on('newNotification', function (event, data) {
		$scope.notifications.push({'title': data.title,'alert': data.alert});
		$timeout(function() {
	        $scope.notifications.splice(0,1);
	    }, 5000);	  	
	});
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if(toState.name == 'index'){ 
        	$scope.bodyClass = 'landing-page' 
        	
        }else if(toState.name == 'login'){
        	$scope.bodyClass = 'signup-page'	
        }else if(toState.name == 'dashboard' || toState.name == 'dashboard.index' || toState.name == 'dashboard.game'){
        	$scope.bodyClass = 'profile-page'	
        }else{
        	$scope.bodyClass = ''	
        }
        $scope.isStateLoading = true;
	});	
	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
		if(toState.name == 'dashboard.game'){
			$scope.showBack = true;	
		}else{
			$scope.showBack = false;	
		}
		if(fromState.name == 'dashboard.game'){
			socket.emit('leave-room',fromParams.id);
		}
        delete $scope.isStateLoading;
	});
    
    
	$scope.authentication = authService.authentication;
	// $scope.acquaintances = acquaintancesService.acquaintances.list;
	return $scope;
});