app.controller('dashboardController',function($http,$scope,$filter,$uibModal,$rootScope,API,socket,$sce,$state,authService){
	// $scope.getPostText = function(text,gameId){
	// 	return $sce.trustAsHtml(text.replace('_gameId_', gameId));
	// }
	
	$(function(){
		$('.nav-tabs a:not(.back-button)').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})
	})
	
	// $(function(){
	// 	$('#createGameModal .datepicker').datepicker({
	// 		weekStart:1,
	// 		orientation: "top-left",
	// 		format: 'yyyy-mm-dd',
	// 	});
	// });
	
	$scope.game = {
		'memberCount': 0,
		'payment': 0,
		'fromHours': 0,
		'toHours': 0,
		// 'date': null
	}
	
	
	$scope.memberCount = {
		0: 'Tutum',
		1: '10 nəfər',
		2: '12 nəfər',
		3: '14 nəfər',
		4: '16 nəfər',
		5: '22 nəfər'
	}
	$scope.payment = {
		0: 'Ödəniş',
		1: 'Yarı-yarı',
		2: 'Yer pulu'
	}
	
	$scope.hours = {
		// 0: 'Saat (-dan)',
		1: '00:00',
		2: '01:00',
		3: '02:00',
		4: '03:00',
		5: '04:00',
		6: '05:00',
		7: '06:00',
		8: '07:00',
		9: '08:00',
		10: '09:00',
		11: '10:00',
		12: '11:00',
		13: '12:00',
		14: '13:00',
		15: '14:00',
		16: '15:00',
		17: '16:00',
		18: '17:00',
		19: '18:00',
		20: '19:00',
		21: '20:00',
		22: '21:00',
		23: '22:00',
		24: '23:00'
	}
	
	$scope.selectMemberCount = function(key){
		$scope.game.memberCount = key;
	}
	$scope.selectPayment = function(key){
		$scope.game.payment = key;
	}
	$scope.selectFromHours = function(key){
		$scope.game.fromHours = key;
	}
	$scope.selectToHours = function(key){
		$scope.game.toHours = key;
	}
	
	$scope.submit = function(){
		if(!$scope.game.date
			|| $scope.game.memberCount == 0 || $scope.game.fromHours == 0
			|| $scope.game.toHours == 0
			|| $scope.game.payment == 0
			|| ($scope.game.payment == 2 && (!$scope.game.winner || !$scope.game.loser)) 
			|| !$scope.game.stadium 
		){
			$scope.error = 'Məlumatlarda yanlışlıq var. Bütün sahələri dəqiqliklə doldurun.';
			return false;
		}
		$http.post(API+'games/new',{
			game: $scope.game
		}).success(function(data){
			if(data.success==1){
				// $state.go("game",{id:data._id});
				// $state.go("games");
				$scope.success = data.message;
				setTimeout(function() {
					alert("close modal")
				}, 3000);
			}
		}).error(function(err){
			$scope.error = err;
		});
	}
	
	if($scope.authentication.isAuth){
		$scope.onlineUsers = [];
		//get online users
			$http.get(API+'index/online-users').success(function(data){
				if(data.success == true){
					$scope.onlineUsers = data.users;
				}
			}).error(function(err){
				// // alert(err);
			});
		//user connected	
			socket.on('set-online',function(data){
				if($scope.onlineUsers.filter(function (users) { return users._id === data._id }).length > 0 ) return false;
				$scope.onlineUsers.unshift(data);
			})
		//user disconnected	
			socket.on('set-offline',function(data){
				$scope.onlineUsers = $scope.onlineUsers.filter(function (users) {
					return users._id !== data._id;
				});
			})
		
		// $state.go('index', {}, { reload: true });
		
		
	}else{
		$state.go('login', {}, { reload: true });
		//get games
		// $http.get(API+'games/shortlist/1').success(function(data){
		// 	$scope.games = data;
		// });
		// $http.get(API+'games/shortlist/2').success(function(data){
		// 	$scope.mixgames = data;
		// });
	}
	
	// socket.on('new-post',function(data){
	// 	var post = data.post;
	// 	post.userId = data.userId;
	// 	if(!$scope.posts){
	// 		$scope.posts = [];
	// 	}
	// 	$scope.posts.unshift(post);
	// })
	
	// alert('index');
	// $http.get(DOMAIN+'users/info').success(function(data){
	// 	if(data.isAuthenticated){
	// 		$scope.isAuthenticated = data.isAuthenticated;
	// 		$rootScope.isAuthenticated = $scope.isAuthenticated;
	// 		//get posts 
	// 		$scope.posts = [{id:1},{id:2}];
	// 	}else{
	// 		$rootScope.isAuthenticated = false;
	// 		$http.get(DOMAIN+'games/shortlist/1').success(function(data){
	// 			$scope.games = data;
	// 		}).error(function(err){
	// 			alert(err);
	// 		});

	// 		$http.get(DOMAIN+'games/shortlist/2').success(function(data){
	// 			$scope.mixgames = data;
	// 		}).error(function(err){
	// 			alert(err);
	// 		});
	// 	}
	// }).error(function(err){
	// 	$scope.isAuthenticated = false;
	// 	$rootScope.isAuthenticated = false;
	// 	$http.get(DOMAIN+'games/shortlist/1').success(function(data){
	// 		$scope.games = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});

	// 	$http.get(DOMAIN+'games/shortlist/2').success(function(data){
	// 		$scope.mixgames = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});
	// 	alert(err);
	// });

	return $scope;
});