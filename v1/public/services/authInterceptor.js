'use strict';
app.factory('authInterceptorService', ['$q', '$location', 'localStorageService','$rootScope', function ($q, $location, localStorageService,$rootScope) {
 
    var authInterceptorServiceFactory = {};
 
    var _request = function (config) {
 
        config.headers = config.headers || {};
 
        var authData = localStorageService.get('authorizationData');
        if (authData) {
            config.headers['x-access-token'] = authData.token;
        }
 
        return config;
    }
 
    var _responseError = function (rejection) {
        if (rejection.status === 403) {
            // $location.path('/login');
            // alert('please, log in!');
            $rootScope.$broadcast('openLoginModal');
        }
        return $q.reject(rejection);
    }
 
    authInterceptorServiceFactory.request = _request;
    authInterceptorServiceFactory.responseError = _responseError;
 
    return authInterceptorServiceFactory;
}]);