var express = require('express');
var socket = require('socket.io');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file

var mongoose = require('mongoose');
var fs = require('fs');
var app = express();
var server = require('http').Server(app);
var io = socket.listen(server);

//mongoose connection 
	mongoose.connect(config.database);
	//models
		var usersModel = require('./models/users.js');
		var gamesModel = require('./models/games.js');
		// var teamsModel = require('./models/teams.js');
		// var teamsUsersModel = require('./models/teamsUsers.js');
		// var gamesModel = require('./models/games.js');
		// var acquaintancesModel = require('./models/acquaintances.js');
		// var stadionModel = require('./models/stadion.js');
		// var stadiumsModel = require('./models/stadiums.js');
		// var scheduleModel = require('./models/schedule.js');
//
app.set('superSecret', config.secret);

// app.use(morgan('dev'));
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({ extended: true },{limit: '10mb'}));

//enable CORS
	app.all('/*', function(req, res, next) {
	  // CORS headers
	  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
	  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	  // Set custom headers for CORS
	  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	  if (req.method == 'OPTIONS') {
	    res.status(200).end();
	  } else {
	    next();
	  }
	});
//

// get an instance of the router for api routes
	var apiRoutes = express.Router(); 
	app.use('/api', apiRoutes);
	
	//important gets and posts
		apiRoutes.post('/login',function(req, res){
			usersModel.findOne({ 'teamName': req.body.teamName},'teamName _id password',function (err, user) {
			  	if (err) throw err;
			  	if(!user){
			  		if(req.body.teamName =='' || req.body.password ==''){
			  			return res.status(401).send({ success: false, message: 'Zəhmət olmasa məlumatlarınızı daxil edin' });
			  		}
			  		
			  		var newUser = new usersModel({
		  				teamName: req.body.teamName,
		  				password: req.body.password,
		            });
			  		newUser.save(function(err,callback){
		                if(err){
		                    res.status(400).send({message: 'Xəta N4'});
		                }else{
		                	// res.send({callback: 'success',id: callback._id});
		                	
		                	callback.password = '';
					        var token = jwt.sign({'user':callback,'isAuthenticated': true}, app.get('superSecret'), {
					          expiresIn: 3600*24 // expires in 24 hours
					        });
					
					        // return the information including token as JSON
					        res.json({
					          success: true,
					          message: 'Uğurla daxil oldunuz! Yönlədirilir...',
					          token: token,
					          data:{
					          	'teamName' : callback.teamName,
					          	'_id' : callback._id
					          }
					        });
		                	
		                	
		                }
		            });
			  	}else{
		  			if(user.password != req.body.password){
						return res.status(401).send({ success: false, message: 'Daxil etdiyiniz şifrə bu komanda üçün yanlışdır' });
		  			}else{
		  				// if user is found and password is right
				        // create a token
				        user.password = '';
				        var token = jwt.sign({'user':user,'isAuthenticated': true}, app.get('superSecret'), {
				          expiresIn: 3600*24 // expires in 24 hours
				        });
				
				        // return the information including token as JSON
				        res.json({
				          success: true,
				          message: 'Uğurla daxil oldunuz! Yönlədirilir...',
				          token: token,
				          data:{
				          	'teamName' : user.teamName,
				          	'_id' : user._id
				          }
				        });
		  			}
			  	}
			})
		});
		apiRoutes.get('/logout', function(req, res) {
			// req.logout();
			// res.redirect('/');
			res.send('ok');
		});
	//
	//socket settings
		apiRoutes.use(function(req, res, next) { req.io = io; next();	});
		var users = [];
		
		//Important functions
			module.exports.getUserSocket = function(sockets, userId){
				var userSockets = Object.keys(sockets).filter(function (key) { 
			        return sockets[key].userData && sockets[key].userData._id == userId;
			    }).map(function(key) {
			        return sockets[key];
			    });
				return userSockets;
		    }
		    module.exports.joinRoom = function(socket, roomId){
				if(socket){
					if(socket.rooms[roomId] == undefined){
						socket.join(roomId)
					}
					
					gamesModel.update({_id: roomId},{$addToSet: {connectedUsers: socket.userData._id}},{upsert:true},function(err){
				        if(err){
				                console.log(err);
				        }else{
				        	socket.currentRoom = roomId;
				                console.log("Successfully added");
				        }
					});
					// io.in(roomId).emit('new-user',{userData: socket.userData, roomId: roomId})
					io.emit('joined-room',{userData: socket.userData, roomId: roomId})
					console.log(socket.userData.teamName+ ' joined to '+ roomId)
				}
				return true;
		    }
			// module.exports.prepareEmit = function(data,action){
			// 	console.log('okdsaasa');
		 //       if (users['u_'+data.opponentId]) {
		 //           for(socketId in users['u_'+data.opponentId]){
		 //               users['u_'+data.opponentId][socketId].emit(action, data);
		 //           }
		 //       }
		 //   }
	  //      module.exports.prepareEmitMultipleUsers = function(data,action){
		 //       data.acquaintances.forEach(function(element){
		 //           if (users['u_'+element]) {
		 //               for(socketId in users['u_'+element]){
		 //                   users['u_'+element][socketId].emit(action,data.data);
		 //               }
		 //           }
		 //       });
		 //   }
			// function count(arr){
		 //       var i = 0;
		 //       for (var id in arr) {
		 //           i++;
		 //       }
		 //       return i;
		 //   }
		//
		io.sockets.on('connection', function (socket) {
			// var userId = null; // istifadeci id-i
		 //   var token = socket.handshake.query.token; // istifadeci id-i
		 //   var u_userId = null;
			console.log('+++ '+socket.id + ' connected new user');
			
			if(socket.userData == undefined) socket.emit('call-authenticate');
			
			socket.on('authenticate', function(token) {
				if (socket.userData != undefined) return false;
				if (token) {
				    // verifies secret and checks exp
				    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
				      	if (err) {
				        	console.log('Failed to authenticate token');
				      	} else {
				        	socket.userData = decoded.user;
				        	console.log(socket.userData.teamName+ ' connected');
				        	
		        	        socket.broadcast.emit('set-online', socket.userData);
		        	        usersModel.findOne({'_id': decoded.user._id},'subscribedRooms',function(err, userModel){
		        	        	if(userModel.subscribedRooms.length > 0){
		        	        		socket.subscribedRooms = [];
			        	        	userModel.subscribedRooms.forEach(function(roomId){
			        	        		if(socket.rooms[roomId] == undefined){
											socket.join(roomId)
											socket.subscribedRooms.push(roomId)
											console.log(socket.userData.teamName+ ' joined to '+ roomId)
											// socket.emit('subscribed-rooms',roomId);
										}
			        	        	})
		        	        	}
		        	        });
				      	}
			    	});
			  	} else {
				    console.log('No token provided')
				}	 
			});
			socket.on('log-out', function(token) {
				if (token) {
				    // verifies secret and checks exp
				    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
				      	if (err) {
				        	console.log('Not logged user');
				      	} else {
				        	socket.disconnect();
				      	}
			    	});
			  	} else {
				    console.log('No token provided to log out')
				}	 
			});
			
			
			socket.on('leave-room', function(roomId) {
				if(socket.subscribedRooms && socket.subscribedRooms.indexOf(roomId) == -1) socket.leave(roomId)
				console.log(socket.userData.teamName+' left from '+ roomId)
				socket.broadcast.emit('left-room',{'roomId': roomId, 'userData': socket.userData })
				gamesModel.update({_id: roomId},{$pull: {connectedUsers: socket.userData._id}},{upsert:true},function(err){
			        if(err){
			                console.log(err);
			        }else{
			        	socket.currentRoom = undefined;
		                console.log("Successfully removed");
			        }
				});
			});
			
			socket.on('disconnect', function () {
		      setTimeout(function(){
				
				if(socket.userData){
					var onlineUsers = Object.keys(io.sockets.sockets).filter(function (key) { 
				        return io.sockets.sockets[key].userData !== undefined && io.sockets.sockets[key].userData._id == socket.userData._id;
				    }).map(function(key) {
				        return io.sockets.sockets[key].userData;
				    });
					console.log('--- '+socket.id+ ' disconnected')
					// var onlineUsers = Object.keys(io.sockets.sockets).map(function (key) { return io.sockets.sockets[key].userData });
					if((onlineUsers.length > 0 )) return false;
					
					if(socket.currentRoom != undefined){
						gamesModel.update({_id: socket.currentRoom},{$pull: {connectedUsers: socket.userData._id}},{upsert:true},function(err){
					        if(err){
					                console.log(err);
					        }else{
					        	socket.broadcast.emit('left-room',{'roomId': socket.currentRoom, 'userData': socket.userData })
					        	socket.currentRoom = undefined;
				                console.log("Successfully removed");
					        }
						});
					}
					
					
					console.log(socket.userData.teamName+ ' disconnected'); 
					socket.broadcast.emit('set-offline', {userData: socket.userData,subscribedRooms: socket.subscribedRooms });
		    	}
				else	console.log(socket.id+ ' non-authenticated user disconnected');
		      },3000)
			});
		});
	//
	//route middleware to verify a token
		module.exports.checkUser = function(req, res, next) {
		  // check header or url parameters or post parameters for token
		  var token = req.body.token || req.query.token || req.headers['x-access-token'];
		
		  // decode token
		  if (token) {
		
		    // verifies secret and checks exp
		    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
		      if (err) {
		        return res.status(403).send({ success: false, message: 'Failed to authenticate token.' });    
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.user = decoded.user;    
		        req.isAuthenticated = decoded.isAuthenticated;    
		        next();
		      }
		    });
		
		  } else {
		
		    // if there is no token
		    // return an error
		    return res.status(403).send({ 
		        success: false, 
		        message: 'No token provided.' 
		    });
		  }
		};
		// module.exports.acquaintances = function(req, res, next) {
		//   	var acquaintances = [];
		//   	var query = {
		// 	    "filtered" : {
		// 	        "query" : {
		// 	            "term" : { "offer.confirmed" : true }
		// 	        },
		// 	        "filter" : {
		// 	            "or" : [
		// 	                {
		// 	                    "term" : { "senderId" : req.user._id }
		// 	                },
		// 	                {
		// 	                    "term" : { "receiverId" : req.user._id }
		// 	                }
		// 	            ]
		// 	        }
		// 	    }
		// 	}
		// 	acquaintancesModel.search(query, {}, function(err, users) { /* ... */
		//  		var i = 0;
		// 		if(users.hits.hits.length){
		// 			users.hits.hits.forEach(function(user){
		// 				i++;
		// 				if(user._source.senderId == req.user._id){
		// 					acquaintances.push(user._source.receiverId);
		// 				}else{
		// 					acquaintances.push(user._source.senderId);
		// 				}
		// 				if(i == users.hits.hits.length){
		// 					req.acquaintances = acquaintances
		// 	 				next()
		// 				}
		// 			})
		// 		}else{
		// 			req.acquaintances = acquaintances
	 //				next()
		// 		}
		//  	})
		// };
		// module.exports.acquaintancesWithRequests = function(req, res, next) {
		//   	var acquaintances = [];
		//   	var query = {
		// 	    "filtered" : {
		// 	        "filter" : {
		// 	            "or" : [
		// 	                {
		// 	                    "term" : { "senderId" : req.user._id }
		// 	                },
		// 	                {
		// 	                    "term" : { "receiverId" : req.user._id }
		// 	                }
		// 	            ]
		// 	        }
		// 	    }
		// 	}
		// 	acquaintancesModel.search(query, {}, function(err, users) { /* ... */
		//  		var i = 0;
		// 		if(users.hits.hits.length){
		// 			users.hits.hits.forEach(function(user){
		// 				i++;
		// 				if(user._source.senderId == req.user._id){
		// 					acquaintances.push(user._source.receiverId);
		// 				}else{
		// 					acquaintances.push(user._source.senderId);
		// 				}
		// 				if(i == users.hits.hits.length){
		// 					req.acquaintances = acquaintances
		// 	 				next()
		// 				}
		// 			})
		// 		}else{
		// 			req.acquaintances = acquaintances
	 //				next()
		// 		}
		//  	})
		// };
		// module.exports.posttypes = function(req, res, next) {
		//   	var posttypes = [];
		// 	postTypesModel.find()
		// 	.exec(function(err,items){
		// 		var i = 0;
		// 		items.forEach(function(item){
		// 			i++;
		// 			posttypes[item._id] = item;
		// 			if(i == items.length){
		// 				req.posttypes = posttypes;
		//  				next()
		// 			}
		// 		})
		// 	});
		// };
		module.exports.isGuest = function(req, res, next) {
		  // check header or url parameters or post parameters for token
		  var token = req.body.token || req.query.token || req.headers['x-access-token'];
		
		  // decode token
		  if (token) {
		
		    // verifies secret and checks exp
		    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
		      if (err) {
		        req.isAuthenticated = false;    
		        next();  
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.user = decoded.user;    
		        req.isAuthenticated = decoded.isAuthenticated;    
		        next();
		      }
		    });
		
		  } else {
		    req.isAuthenticated = false;    
			next();  
		  }
		};
	//
	//routes requieres token
		apiRoutes.use('/index', require('./routes/index'));
		// apiRoutes.use('/users', require('./routes/users'));
		// apiRoutes.use('/stadion', require('./routes/stadion'));
		// apiRoutes.use('/teams', require('./routes/teams'));
		apiRoutes.use('/games', require('./routes/games'));
		// apiRoutes.use('/requests', require('./routes/requests'));
		// apiRoutes.use('/conversations', require('./routes/conversations'));
		// apiRoutes.use('/posts', require('./routes/posts'));
	//
//


// load our public/index.html file
	app.get('*', function(req, res) {
	    res.sendFile(__dirname+'/public/index.html'); 
	});

/********************************/


//checking users status
	// setInterval(function () {
	//     var existSockets = [];
	//     io.sockets.sockets.forEach(function (s) {
	//         existSockets.push(s.id)
	//     });
	//     for (var id in users) {
	//         var i = 0;
	//         var userIsOnline = false;
	//         for(socketId in users[id]){
	//             i++;
	//             if(i != count(users[id])){
	//                 if(existSockets.indexOf(users[id][socketId].id) >= 0) {
	//                     userIsOnline = true;
	//                 }else{
	//                     delete users[id][socketId];
	//                     i--;
	//                     userIsOnline = false;
	//                 }
	//             }else{
	//                 if (existSockets.indexOf(users[id][socketId].id) >= 0) {
	//                     userIsOnline = true;
	//                 }else{
	//                     if(userIsOnline != true){
	//                         delete users[id];
	//                         console.log('- ' + id.split("_")[1] + ' disconnected');
	//                         io.sockets.emit('set-offline', id.split("_")[1] ); // broadcast edilir
	//                         // Profile.update( { 'online': 0, 'last_time' : sequelize.fn('NOW')},{ where: { 'user_id': parseInt(id.split("_")[1]) } } );
	//                     }else{
	//                         delete users[id][socketId];
	//                         i--;
	//                         userIsOnline = false;
	//                     }
	//                 }
	//             }
	//         }
	//     }
	// }, 30000);
//


server.listen(8080, function(){ 
    console.log('listening on *:8080');
});