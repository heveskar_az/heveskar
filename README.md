# Heveskar.Az

heveskar.az

## Getting Started


### Prerequisites

What things you need to install the software and how to install them

```
MongoDB
Elastic-Search
```

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
sudo service elasticsearch start
```
```
mongod
```
```
node app.js
```

End with an example of getting some data out of the system or using it for a little demo

## Built With

* ExpressJs
* cloud9
* Socket.io
* MongoDB
* AngularJs
* JWT
* ElasticSearch
* Cloudinary

