var express = require('express');
var	router = express.Router();
var checkUser = require("../app").checkUser;
var isGuest = require("../app").isGuest;
var teamsModel = require('../models/teams.js');
var teamsUsersModel = require('../models/teamsUsers.js');

// cloudinary
	var cloudinary = require('cloudinary');
	cloudinary.config({ 
	  cloud_name: 'heveskar-az', 
	  api_key: '696691114969959', 
	  api_secret: 'vhoJSa5CaIdb-IzqMuy2wazbwb8' 
	});
//

router.post('/members/removeJoined',checkUser, function(req, res) {
	if(req.body.userId && req.body.teamId){
		teamsUsersModel
		.findOne({
			teamId :req.body.teamId,
			userId :req.body.userId
		}, '_id teamId offer')
		.exec(function (err, data){
		  	if(!err){
			  	if(data){ 
		  			//teklif movcuddur
		  			teamsUsersModel.findOne({
		  				'teamId':data.teamId,'userId':req.user._id,'captain': true
		  			},'_id', function(err,data2){
		  				if(err){
		  					res.status(400).send('Xəta N2');
		  				}else{
		  					if(data2){
		  						data.remove(function(err,response){
	  								if(err){
  										res.status(400).send('Xəta N3')
	  								}else{
  										teamsModel.findOne({'_id':req.body.teamId},'members').exec(function(err, dataTeam) {
						  					if(err) res.status(400).send('Xəta N4');
						  					if(dataTeam){
						  						dataTeam.members.splice(dataTeam.members.indexOf(req.body.userId) ,1);
						  						dataTeam.save(function(){
						  							res.send(true);	
						  						});
						  					}else{
						  						res.status(400).send('Xəta N5');
						  					}
							  			});
	  								}
		  						});
		  					}else{
		  						res.status(400).send('- Əməliyyatı yerinə yetirməyə hüququnuz yoxdur');
		  					}
		  				}
		  			});
			  	}else{
			  		res.status(400).send('- Komandaya qoşulma təklifi təyin olunmadı');
			  	}
		  	}else{
		  		res.status(400).send('- Xəta N1');
		  	}
		});
	}else{
		res.status(400).send('- Məlumat düzgün ötürülmədi');
	}
});

router.post('/members/checkIsJoined',checkUser,function(req,res){
	if(req.isAuthenticated){
		teamsUsersModel
		.findOne({
			'userId':req.user._id,'teamId':req.body.teamId
		}, '_id offer')
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xeta N1');
			}else{
				if(data){
					if(data.offer.confirmed==true){
						res.send('1'); //is member
					}else{
						if(data.offer.from==false){
							res.send('2'); // user has sent request
						}else{
							res.send('3'); // team has sent request 
						}
					}
				}else{
					res.send(false);
				}
			}
		});
	}else{
		res.send(false);
	}
});
router.post('/members/join', checkUser, function(req, res) {
		
		if(req.body.teamId){
			teamsUsersModel
			.findOne({
				'userId':req.user._id,'teamId':req.body.teamId
			}, '_id offer captain teamId')
			.exec(function (err, data){
			  	if(data){
			  		if(data.captain == true){
			  			teamsModel.findOne({'_id':data.teamId},'members').exec(function(err, data2) {
		  					if(data2.members.length > 1){
		  						data.remove(function(err,removed){
		  							teamsUsersModel.findOne({'teamId': data.teamId},'captain').exec(function(err,doc){
		  								if(err){
		  									res.status(400).send('Xəta N1');
		  									return false;
		  								}
		  								console.log(doc);
		  								doc.captain = true;
		  								doc.save(function(){
		  									res.status(200).send('deleted');		
		  								})
		  							});
		  						});
		  					}else{
		  						res.status(400).send('Komandanızda digər üzv olmadığından kapitan komandanı tərk edə bilməz')
		  					}
			  			});
			  		}else{
			  			if(data.offer.confirmed == true || !data.offer.from){
		  					data.remove(function(err,removed){
	  							teamsModel.findOne({'_id':req.body.teamId},'members').exec(function(err, data2) {
				  					if(err) res.status(400).send('Xəta var');
				  					if(data2){
				  						data2.members.splice(data2.members.indexOf(req.user._id) ,1);
				  						data2.save(function(){
				  							res.status(200).send('deleted');	
				  						});
				  					}else{
				  						res.status(400).send('Xəta var');
				  					}
					  			});
	  						});
			  			}else{
			  				data.offer.confirmed = true;
			  				data.offer.confirmDate = new Date();
			  				data.save(function(err,saved){
			  					if(err){
			  						res.status(400).send('Xəta var');
			  					}else{
				  					teamsModel.findOne({'_id':req.body.teamId},'members').exec(function(err, data2) {
					  					if(err) res.status(400).send('Xəta var');
					  					if(data2){
					  						data2.members.push(req.user._id);
					  						data2.save(function(){
					  							res.send('isMember');	
					  						});
					  					}else{
					  						res.status(400).send('Xəta var');
					  					}
						  			});
			  					}
			  				});
			  			}
			  		}
			  	}else{
			  		//teklif gonder
			  		var offer = { date: new Date(), confirmed: false,by:req.user._id,from:false };
					var newReg = new teamsUsersModel({
			  				userId: req.user._id,
			  				teamId: req.body.teamId,
			  				captain: false,
			  				offer: offer
                        });
                    newReg.save(function(err){
                        if(err){
                            res.status(400).send('Xəta N4');
                        }else{
                            res.status(200).send('sentRequest');
                        }
                    });
			  	}
			});
		}else{
			res.status(400).send('- Komanda seçilməyib');
		}
});


router.post('/members/acceptJoining',checkUser, function(req, res) {
	if(req.body.reqId){
		teamsUsersModel
		.findOne({
			_id:req.body.reqId
		}, '_id teamId offer')
		.exec(function (err, data){
		  	if(!err){
			  	if(data){ 
		  			//teklif movcuddur
		  			teamsUsersModel.findOne({
		  				'teamId':data.teamId,'userId':req.user._id,'captain': true
		  			},'_id', function(err,data2){
		  				if(err){
		  					res.status(400).send('Xəta N2');
		  				}else{
		  					if(data2){
		  						data.offer.confirmed = true;
		  						data.offer.by = req.user._id;
		  						data.offer.confirmDate = new Date();
		  						data.save(function(){
		  							teamsModel.findOne({'_id':data.teamId},'members').exec(function(err, dataTeam) {
					  					if(err) res.status(400).send('Xəta var');
					  					if(dataTeam){
					  						dataTeam.members.push(data.userId);
					  						dataTeam.save(function(err,responseTeam){
					  							if(err) res.status(400).send('Xəta var')
					  							else res.send('isMember');	
					  						});
					  					}else{
					  						res.status(400).send('Xəta var');
					  					}
						  			});
		  						});
		  					}else{
		  						res.status(400).send('- Əməliyyatı yerinə yetirməyə hüququnuz yoxdur');
		  					}
		  				}
		  			});
			  	}else{
			  		res.status(400).send('- Komandaya qoşulma təklifi təyin olunmadı');
			  	}
		  	}else{
		  		res.status(400).send('- Xəta N1');
		  	}
		});
	}else{
		res.status(400).send('- Komanda seçilməyib');
	}
});

router.post('/members/rejectJoining', checkUser, function(req, res) {
	if(req.body.reqId){
		teamsUsersModel
		.findOne({
			_id:req.body.reqId
		}, '_id teamId offer')
		.exec(function (err, data){
		  	if(!err){
			  	if(data){ 
		  			//teklif movcuddur
		  			teamsUsersModel.findOne({
		  				'teamId':data.teamId,'userId':req.user._id,captain:true
		  			},'_id', function(err,data2){
		  				if(err){
		  					res.status(400).send('Xəta N2');
		  				}else{
		  					if(data2){
		  						data.remove();
		  						res.send(true);
		  					}else{
		  						res.status(400).send('- Əməliyyatı yerinə yetirməyə hüququnuz yoxdur');
		  					}
		  				}
		  			});
			  	}else{
			  		res.status(400).send('- Komandaya qoşulma təklifi təyin olunmadı');
			  	}
		  	}else{
		  		res.status(400).send('- Xəta N1');
		  	}
		});
	}else{
		res.status(400).send('- Komanda seçilməyib');
	}
});


router.get('/list/', function(req, res) {

	teamsModel
	.find({},'_id img name members statistics.points statistics.played location address',{sort: {'statistics.points': 1 }})
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data.length){
				res.json(data);
			}else{
				res.send(false);
			}
		}
	});
});

router.get('/myteams/', checkUser, function(req, res) {
	teamsUsersModel
	.find({
		'userId': req.user._id, 'offer.confirmed': true
	}, 'teamId')
	.populate('teamId','_id img name statistics members')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data.length){
				res.json(data);
			}else{
				res.send(false);
			}
		}
	});
});

router.get('/myteams/list/:name', checkUser, function(req, res) {
	if(req.params.name){
		teamsUsersModel
		.find({
			'userId': req.user._id, 'offer.confirmed': true
		}, 'teamId')
		.populate('teamId','_id img name statistics',{ 'name': {'$regex' : req.params.name} })
		.limit(7)
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N3');
			}else{
				if(data.length){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});
	}
});

router.get('/myteams/list/',checkUser, function(req, res) {
	if(!req.isAuthenticated){
		res.status(403).send('- Komandalarınızın siyahısına baxmaq üçün sayta həvəskar kimi daxil olmalısız');
	}else{
		teamsUsersModel
		.find({
			'userId': req.user._id, 'offer.confirmed': true
		}, 'teamId')
		.populate('teamId','_id img name statistics')
		.limit(7)
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N4');
			}else{
				if(data.length){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});
	}
});


router.post('/delImg', function(req, res) {
	teamsModel
	.findOne({name : req.body.teamName}, 'img',function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(err){
			res.status(400).send('Xəta N1!');
		}else{
			if(data.img!="http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/teams/noTeamPhotoHeveskarAz.png"){
				cloudinary.uploader.destroy('teams/'+req.body.teamName, function(result) {
				 	data.img = "http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/teams/noTeamPhotoHeveskarAz.png"
	            	data.save();
	            	res.send(true);
				});
			}else{
				res.status(400).send('Sizin komanda şəkliniz yoxdur');
			}	
		}
	});
});

router.post('/uploadImg', function(req, res) {
	if(req.isAuthenticated){
		if(req.body.data && req.body.teamName){
			cloudinary.uploader.upload(req.body.data, function(result) { 
				teamsModel
				.findOne({name: req.body.teamName},'img',function(err,doc){
					if(err){
						res.status(400).send('Xəta N2!');
					}else{
						doc.img = result.url;
						doc.save();
						res.send(result.url);
					}
				});
			},{ public_id: "teams/"+req.body.teamName});
		}else{
			res.status(400).send('Xəta var! Lütfən, şəkil seçin!');
		}
	}else{
		res.status(400).send('Xəta N1');
	}
});

router.post('/update', function(req, res) {
	if(req.isAuthenticated){
		if(req.body.teamName && req.body.teamId && req.body.teamName.length >= 4 && req.body.teamName.length <= 15){
			req.body.teamName = req.body.teamName.toLowerCase();
			teamsUsersModel
			.findOne({'userId': req.user._id,'teamId': req.body.teamId,'offer.confirmed':true}, '_id')
			.exec(function (err, data) {
				if(!err){
					if(data){
						teamsModel
						.find({_id: { $ne : req.body.teamId },
							name:req.body.teamName
						}, '_id')
						.exec(function (err, data2) {
						  	if(data2.length){
								res.status(400).send('- Bu adda komanda mövcuddur.Lütfən, başqa ad seçin.');
							}else{
								if(err){
									res.status(400).send('Xəta N3!');
								}else{
									teamsModel
									.findOne({_id: req.body.teamId},'name',function(err,doc){
										if(err){
											res.status(400).send('Xəta N2!');
										}else{
											doc.name = req.body.teamName;
											doc.save();
											res.send('- Dəyişiklik uğurla yerinə yetirildi.');
										}
									});
								}
							}	
						});
					}else{
						res.status(400).send('- Sizin əməliyyatı yerinə yetirmək hüququnuz yoxdur');	
					}
				}else{
					res.status(400).send('Xəta N4!');
				}
			});
		}else{
			res.status(400).send('- Komanda adı ən az 4, ən çox 15 hərfdən ibarət olmalıdır.')
		}
	}else{
		res.status(400).send('- Əməliyyatı yerinə yetirmək üçün qeydiyyatdan keçməyiniz və ya sayta həvəskar kimi daxil olmanız mütləqdir.')
	}
});

router.get('/info/:name', isGuest, function(req, res) {
	if(!req.params.name){
		res.status(400).send('Xəta N1');
	}else{
		req.params.name = req.params.name.toLowerCase();
		teamsModel
		.findOne({
			name: req.params.name
		}, '_id img createdBy name statistics')
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N3');
			}else{
				if(data){
					teamsUsersModel
					.find({ 'teamId': data._id },'userId captain offer')
					.populate(
						{path:'userId',select: '_id username img position'}
					)
					.exec(function (err2, data2) {
						var members = [];
						var wantedMembers = [];
						var captain = false;
						if(err2){
							res.status(400).send('Xəta N2');
						}else{
							if(data2){
								var i=0;
								data2.forEach(function(item){
									i++;
									if(item.offer.confirmed==true){
										members.push({userId:item.userId,captain:item.captain})
									}
									if(item.offer.confirmed==false && item.offer.from==false){
										wantedMembers.push({userId:item.userId,reqId:item._id})
									}
									if(req.isAuthenticated){
										if(captain==false && item.captain==true && req.user._id==item.userId._id){
											captain = true;
										}
									}
									if(i==data2.length){
										res.json({
											captain:captain,dataTeam:data,dataMembers: members,
											wantedMembers: wantedMembers
										});
									}
								});
							}
						}
					});
				}else{
					res.json(null);
				}
			}
		});
	}
});
router.post('/checkTeamName', checkUser, function(req, res) {
	if(req.body.name){
		if(req.body.name.length < 5){
			res.status(400).send('Komanda adını düzgün və ən azı 5 simvoldan ibarət olmaqla daxil edin');
			return false;
		}
	}else{
		res.status(400).send('Komanda adını düzgün və ən azı 5 simvoldan ibarət olmaqla daxil edin');	
		return false;
	}
	req.body.name = req.body.name.toLowerCase();
	teamsModel
	.find({name: req.body.name}, '_id')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
	  		res.status(400).send('Belə adlı komanda artıq mövcuddur');	
	  		return false;
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});

router.post('/register', checkUser, function(req, res) {
	if(req.body.name.length >= 4 && req.body.name.length <= 15){
		req.body.name = req.body.name.toLowerCase();
		teamsModel
		.find({name: req.body.name}, '_id')
		.exec(function (err, data){
		  	// if (err) return done(null, null);//handleError(err);
		  	if(data.length){
				res.status(400).send('- Bu adda komanda mövcuddur.Lütfən, başqa bir ad seçin.');
			}else{
				if(err){
					res.status(400).send('Xəta N3!');
				}else{
					//qeydiyyat mumkundur
					var newReg = new teamsModel({
			  				name: req.body.name,
			  				img: "http://res.cloudinary.com/heveskar-az/image/upload/teams/noTeamPhotoHeveskarAz.png",
			  				createdBy: req.user._id,
			  				members: [req.user._id],
			  				statistics : { played: 0,won:0,lost:0,draw: 0,points: 0 },
                            createdDate: new Date(),
                            location: req.user.location,
                            address: req.user.address
                        });
                    newReg.save(function(err,callback){
                        if(err){
                            res.status(400).send('Xəta N4');
                        }else{

                        	var offer = { confirmed: true };
							var newReg2 = new teamsUsersModel({
					  				teamId: callback._id,
					  				userId: req.user._id,
					  				captain: true,
					  				offer: offer
		                        });
		                    newReg2.save(function(err){
		                        if(err){
		                            res.status(400).send('Xəta N5');
		                        }else{
		                            res.send(req.body.name);
		                        }
		                    });
                        }
                    });
				}
			}	
		});
	}else{
		res.status(400).send('- Komanda adı ən az 4, ən çox 15 hərfdən ibarət olmalıdır.');
	}
});

router.post('/addUser',checkUser, function(req, res) {
	if(req.body.userId && req.body.teamId){
		teamsUsersModel
		.find({teamId: req.body.teamId,userId: req.body.userId}, '_id')
		.exec(function (err, data){
		  	if(data.length){
				res.status(500).send('- Seçilmiş oyunçu bu komandanın heyətində var və ya təklif göndərilib');
			}else{
				if(err){
					res.status(500).send('Xəta N3!');
				}else{
					var offer = { from: true, by: req.user._id, date: new Date(), confirmed: false };
					var newReg = new teamsUsersModel({
			  				teamId: req.body.teamId,
			  				userId: req.body.userId,
			  				captain: false,
			  				offer: offer
                        });
                    newReg.save(function(err){
                        if(err){
                            res.status(500).send('Xəta N4');
                        }else{
                            res.send('- Oyunçuya komandaya qoşulmaq üçün təklif göndərildi');
                        }
                    });
				}
			}	
		});
	}else{
		res.status(500).send('- Komanda və ya oyunçu seçilməyib');
	}
});

/*
router.post('/checkPhone', function(req, res) {
	usersModel
	.find({phone: req.body.phone}, 'phone')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
			res.send(false); //qeydiyyat mumkun deyil
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});
router.post('/checkEmail', function(req, res) {
	usersModel
	.find({email: req.body.email}, 'email')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
			res.send(false); //qeydiyyat mumkun deyil
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});


/* GET home page. *
router.get('/info', function(req, res) {
	res.json({
		isAuthenticated: req.isAuthenticated,
		user: req.user
	});
});

*/
module.exports = router;
