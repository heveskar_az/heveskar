var express = require('express');
var	router = express.Router();
var mongoose = require('mongoose');
mongoose.set('debug', false);
var checkUser = require("../app").checkUser;
var acquaintances = require("../app").acquaintances;
var acquaintancesWithRequests = require("../app").acquaintancesWithRequests;
var prepareEmit = require("../app").prepareEmit;
var usersModel = require('../models/users.js');

usersModel.createMapping(function(err, mapping) {
  if (err) {
    console.log('error creating mapping (you can safely ignore this)');
    console.log(err);
  } else {
    console.log('mapping created!');
    console.log(mapping);
  }
});


var acquaintancesModel = require('../models/acquaintances.js');
acquaintancesModel.createMapping(function(err, mapping) {
  if (err) {
    console.log('error creating mapping (you can safely ignore this)');
    console.log(err);
  } else {
    console.log('mapping created!');
    console.log(mapping);
  }
});
var teamsUsersModel = require('../models/teamsUsers.js');
var notificationsModel = require('../models/notifications.js');
var notificationTypeModel = require('../models/notification-type.js');


// cloudinary
	var cloudinary = require('cloudinary');
	cloudinary.config({ 
	  cloud_name: 'heveskar-az', 
	  api_key: '696691114969959', 
	  api_secret: 'vhoJSa5CaIdb-IzqMuy2wazbwb8' 
	});
//


router.post('/delImg', function(req, res) {
	usersModel
	.findOne({_id : req.user._id}, 'img',function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(err){
			res.status(400).send('Xəta N1!');
		}else{
			if(data.img!="http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/users/avatar.png"){
				cloudinary.uploader.destroy('users/'+req.user.username, function(result) {
				 	data.img = "http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/users/avatar.png"
	            	data.save();
	            	res.send(true);
				});
			}else{
				res.status(400).send('Sizin avatarınız yoxdur');
			}	
		}
	});
});

router.post('/uploadImg', checkUser, function(req, res) {
	if(req.body.data){
		cloudinary.uploader.upload(req.body.data, function(result) { 
			usersModel
			.findOne({_id: req.user._id},'img',function(err,doc){
				if(err){
					res.status(500).send('Xəta N2!');
				}else{
					doc.img = result.url;
					doc.save();
					res.send(result.url);
				}
			});
		},{ public_id: "users/"+req.user.username});
	}else{
		res.send('Xəta var! Lütfən, şəkil seçin!');
	}
});

router.post('/update', checkUser, function(req, res) {
	if(req.body.data.newPassword){
		usersModel
		.findOne({_id: req.user._id}, 'password')
		.exec(function (err, data) {
			if(data && data.password == req.body.data.currentPassword){
				if(req.body.data.newPassword == req.body.data.repeatNewPassword){

					req.body.data.email = req.body.data.email.toLowerCase();
					req.body.data.username = req.body.data.username.toLowerCase();

					usersModel
					.find({_id: { $ne : req.user._id },
						email:req.body.data.email,
						phone:req.body.data.phone,
						username:req.body.data.username,
					}, '_id')
					.exec(function (err, data) {
					  	if(data.length){
							res.status(500).send('Xəta N2!');
						}else{
							if(err){
								res.status(500).send('Xəta N3!');
							}else{
								usersModel
								.findOne({_id: req.user._id},'_id',function(err,doc){
									if(err){
										res.status(500).send('Xəta N4!');
									}else{
										doc.username = req.body.data.username;
										doc.password = req.body.data.newPassword;
										doc.name = req.body.data.name;
										doc.phone = req.body.data.phone;
										doc.email = req.body.data.email;
										doc.birthday = req.body.data.birthday;
										doc.gender = req.body.data.gender;
										doc.address = req.body.data.address;
										doc.lat = req.body.data.lat;
										doc.lon = req.body.data.lon;
										doc.position = req.body.data.position;
										doc.level = req.body.data.level;
										doc.save();
										res.send(true);
									}
								});
							}
						}	
					});
				}else{
					res.status(500).send('Xəta N5!');	
				}
			}else{
				res.status(500).send('Cari şifrə doğru deyil!');
			}
		});
	}else{
		req.body.data.email = req.body.data.email.toLowerCase();
		req.body.data.username = req.body.data.username.toLowerCase();

		usersModel
		.find({_id: { $ne : req.user._id },
			email:req.body.data.email,
			phone:req.body.data.phone,
			username:req.body.data.username,
		}, '_id')
		.exec(function (err, data) {
		  	if(data.length){
				res.status(500).send('Xəta N2!');
			}else{
				if(err){
					res.status(500).send('Xəta N3!');
				}else{
					usersModel
					.findOne({_id: req.user._id},'_id',function(err,doc){
						if(err){
							res.status(500).send('Xəta N4!');
						}else{
							doc.username = req.body.data.username;
							doc.name = req.body.data.name;
							doc.phone = req.body.data.phone;
							doc.email = req.body.data.email;
							doc.birthday = req.body.data.birthday;
							doc.gender = req.body.data.gender;
							doc.address = req.body.data.address;
							doc.lat = req.body.data.lat;
							doc.lon = req.body.data.lon;
							doc.position = req.body.data.position;
							doc.level = req.body.data.level;
							doc.save();
							res.send(true);
						}
					});
				}
			}	
		});
	}
});

router.post('/checkUsername', function(req, res) {
	if(req.isAuthenticated){
		req.body.username = req.body.username.toLowerCase();
		usersModel
		.find({_id:{$ne:req.user._id},username: req.body.username}, 'username')
		.exec(function (err, data) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(data.length){
				res.send(false); //qeydiyyat mumkun deyil
			}else{
				if(err){
					res.status(500).send('Xəta var!');
				}else{
					res.send(true); //qeydiyyat mumkundur
				}
			}	
		});
	}else{
		req.body.username = req.body.username.toLowerCase();
		usersModel
		.find({username: req.body.username}, 'username')
		.exec(function (err, data) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(data.length){
				res.send(false); //qeydiyyat mumkun deyil
			}else{
				if(err){
					res.status(500).send('Xəta var!');
				}else{
					res.send(true); //qeydiyyat mumkundur
				}
			}	
		});
	}	
});

router.post('/checkPhone', function(req, res) {
	usersModel
	.find({phone: req.body.phone}, 'phone')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
			res.send(false); //qeydiyyat mumkun deyil
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});
router.post('/checkEmail', function(req, res) {
	req.body.email = req.body.email.toLowerCase();
	usersModel
	.find({email: req.body.email}, 'email')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
			res.send(false); //qeydiyyat mumkun deyil
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});

router.post('/register', function(req, res) {
	if(
		req.body.user.password==req.body.user.samepassword &&
		req.body.user.username && req.body.user.email &&
		req.body.user.phone && req.body.user.adsoyad && 
		req.body.user.password && req.body.user.samepassword &&
		req.body.user.address && req.body.user.lat &&
		req.body.user.lon
	){
		req.body.user.email = req.body.user.email.toLowerCase();
		req.body.user.username = req.body.user.username.toLowerCase();

		usersModel
		.find({
			$or:[
				{email: req.body.user.email},
				{username: req.body.user.username},
				{phone: req.body.user.phone},
			]
		}, '_id')
		.exec(function (err, data) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(data.length){
				res.status(500).send('Xəta N2');
			}else{
				if(err){
					res.status(500).send('Xəta N3!');
				}else{
					//qeydiyyat mumkundur
					var newReg = new usersModel({
			  				username: req.body.user.username,
			  				email: req.body.user.email,
			  				name: req.body.user.adsoyad,
			  				phone: req.body.user.phone,
			  				img: 'http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/users/noAvatarOfUserHeveskarAz.png',
			  				password: req.body.user.password,
                            createdDate: new Date(),
                            location: [req.body.user.lat,req.body.user.lon],
                            address: req.body.user.address
                        });
                    newReg.save(function(err){
                        if(err){
                            res.status(500).send('Xəta N4');
                        }else{
					    	res.send('Qeydiyyat tamamlandı');
                        }
                    });
				}
			}	
		});
	}else{
		res.status(500).send('Məlumatlarda yalnışlıq var');
	}
});


/* GET home page. */
router.get('/info', function(req, res) {
	res.json({
		isAuthenticated: req.isAuthenticated,
		user: req.user
	});
});

router.get('/profile/:username', function(req, res) {
	if(!req.params.username){
		res.status(500).send('Xəta N1');
	}else{
		req.params.username = req.params.username.toLowerCase();

		usersModel
		.findOne({
			username: req.params.username
		}, 'username name email phone address location gender birthday position level img')
		.exec(function (err, data) {
			if(req.isAuthenticated){
				if(req.user.username==req.params.username){
					isOwnProfile=true;
				}else{
					isOwnProfile=false;
				}
			}else{
				isOwnProfile=false;
			}
			res.json({isOwnProfile:isOwnProfile,data:data});
		});
	}
});


router.post('/teams/list', function(req, res) {
	console.log('asda');
	teamsUsersModel
	.find({
		'userId': req.body.userId, 'offer.confirmed': true
	}, 'teamId captain')
	.populate('teamId','_id img name')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data.length){
				res.json(data);
			}else{
				res.send(false);
			}
		}
	});
});
router.post('/teams/list/count', function(req, res) {
	teamsUsersModel.count(
		{'userId': req.body.userId, 'offer.confirmed': true}, 
		function( err, count){
	    	if(err){
				res.json(0);
			}else{
				res.json(count);
			}
		}
	)
});

router.post('/acquaintances/list',checkUser,function(req,res){
	acquaintancesModel
	.find({
		$and:[
			{
				$or:[
					{'senderId':req.body.userId},
					{'receiverId':req.body.userId},
				] 
			},
			{
				'offer.confirmed':true
			}
		]
	}, '_id senderId receiverId')
	.populate([{path:'senderId',select:'_id username img'},{path:'receiverId',select:'_id username img'}])
	.exec(function (err, acData) {
		if(err){
			res.status(400).send('Xeta N1');
		}else{
			if(acData){
				var acquaintances = [];
				acData.forEach(function(item){
					if(item.senderId._id!=req.body.userId){
						acquaintances.push(item.senderId);
					}
					if(item.receiverId._id!=req.body.userId){
						acquaintances.push(item.receiverId);
					}
				});
				res.send(acquaintances);
			}else{
				res.send(false);
			}
		}
	});
});


router.post('/acquaintances/ids', checkUser,function(req,res){
	acquaintancesModel
	.find({
		$and:[
			{
				$or:[
					{'senderId':req.user._id},
					{'receiverId':req.user._id},
				] 
			},
			// {
			// 	'offer.confirmed':true
			// }
		]
	}, '_id senderId receiverId offer')
	.populate([{path:'senderId',select:'_id username img'},{path:'receiverId',select:'_id username img'}])
	.exec(function (err, acData) {
		if(err){
			res.status(400).send('Xeta N1');
		}else{
			if(acData){
				res.send(acData);
			}else{
				res.send(false);
			}
		}
	});
});

router.post('/acquaintances/check', checkUser, function(req,res){
	acquaintancesModel
	.findOne({
		$or:[
			{'senderId':req.user._id,'receiverId':req.body.userId},
			{'receiverId':req.user._id,'senderId':req.body.userId},
		] 
	}, '_id offer receiverId')
	.exec(function (err, acData) {
		if(err){
			res.status(400).send('Xeta N1');
		}else{
			if(acData){
				if(acData.offer.confirmed==true){
					res.send('added');
				}else{
					if(acData.receiverId==req.user._id){
						res.send('receivedRequest');
					}else{
						res.send('requestSent');
					}
				}
			}else{
				res.send('deleted');
			}
		}
	});
});
router.post('/acquaintances/add', checkUser,function(req, res) {
	if(req.body.userId){
		acquaintancesModel
		.findOne({
			$or:[
				{'senderId':req.user._id,'receiverId':req.body.userId},
				{'receiverId':req.user._id,'senderId':req.body.userId},
			] 
		}, '_id offer senderId')
		.exec(function (err, data){
		  	if(data){
		  		if(data.offer.confirmed==true || (data.senderId == req.user._id && data.offer.confirmed == false) || req.body.reject == true ){
		  			//tanishlardan chixart
		  			acquaintancesModel.findOneAndRemove({
		  				$or:[
							{'senderId':req.user._id,'receiverId':req.body.userId},
							{'receiverId':req.user._id,'senderId':req.body.userId},
						] 
		  			}, function(err,removed){
		  				if(err){
		  					res.status(400).send('Xəta N3');
		  				}else{
		  					if(removed){
		  						res.send({'msg': 'deleted'});	
		  					}else{
		  						res.status(400).send('Təklif tapılmadı');
		  					}
		  				}
		  			});
		  		}else{
		  			//teklif gonderilib ve teklif qebul edilir
		  			acquaintancesModel.findOne({
		  				'receiverId':req.user._id,
		  				'senderId':req.body.userId
		  			},'_id senderId offer')
		  			.populate([{path:'senderId',select:'_id username img'}])
		  			.exec(function(err, acData){
		  				if(err){
		  					res.status(400).send('Xəta N4');
		  					return false;
		  				}
		  				
		  				if(acData){
		  					acData.offer.confirmed = true;
		  					acData.offer.confirmDate = new Date();
		  					acData.save(function(err, saved){
		  						if(err){
		  							res.status(400).send(err);	
		  							return false;
		  						}
		  						
		  						var notificationTypeId = "56e356a575bc1ff3e6d6e249";
								var newNotif = new notificationsModel({
									type: notificationTypeId,
									senderId: req.user._id,
									to: [data.senderId],
									seenBy: [],
									date: new Date()
								});
								
								newNotif.save(function(err,saved){
									notificationTypeModel.findOne({'_id': notificationTypeId})
									.exec(function(err, typeData) {
										var dataForSocket = {
											'opponentId': data.senderId,
											'type':typeData,
											'senderId': {
												'_id': req.user._id,
												'img': req.user.img,
												'username': req.user.username
											}
										}
										prepareEmit(dataForSocket,'notifications-new');
										res.send({
					  						'msg': 'added',
					  						'data': {
					  							'userData' : acData.senderId,
					  							'offer':{
					  								'sender': true,
					  								'confirmed': true
					  							}
					  						}
					  					});	
									});
								});
		  					});
		  				}else{
		  					res.status(400).send('Xəta N5');
		  					return false;
		  				}
		  			});
		  		}
		  	}else{
		  		//teklif gonder
		  		var offer = { date: new Date(), confirmed: false };
				var newReg = new acquaintancesModel({
		  				senderId: req.user._id,
		  				receiverId: req.body.userId,
		  				offer: offer
                    });
                newReg.save(function(err,saved){
                    if(err){
                        res.status(400).send('Xəta N4');
                    }else{
                    	acquaintancesModel
						.findOne({
							'_id' : saved._id 
						}, '_id senderId receiverId offer')
						.populate([{path:'senderId',select:'_id username img'},{path:'receiverId',select:'_id username img'}])
						.exec(function (err, acData) {
							if(err){
								res.status(400).send('Xeta N1');
							}else{
								if(acData){
									var notificationTypeId = "56e3405d75bc1ff3e6d6e248";
									var newNotif = new notificationsModel({
										type: notificationTypeId,
										senderId: req.user._id,
										to: [req.body.userId],
										seenBy: [],
										date: new Date()
									});
									
									newNotif.save(function(err,saved){
										var dataForClient = {
											'msg': 'requestSent',
											'data': {
												'userData' : acData.receiverId,
												'offer' : { 
											        'sender': false,
											        'confirmed': acData.offer.confirmed
											    }	
											}
										};
										notificationTypeModel.findOne({'_id': notificationTypeId})
										.exec(function(err, typeData) {
											var dataForSocket = {
												'opponentId': req.body.userId,
												'type':typeData,
												'senderId': {
													'_id': req.user._id,
													'img': req.user.img,
													'username': req.user.username
												}
											}
											prepareEmit(dataForSocket,'notifications-new');
											res.send(dataForClient);			 
										});
									});
								}else{
									res.status(400).send(false);
								}
							}
						});
                    }
                });
		  	}
		});
	}else{
		res.status(400).send('- Tanışlara əlavə etmək üçün həvəskar seçilməyib');
	}
});


router.post('/list/', function(req, res) {
	if(!req.body.username){
		res.json({data:false});
	}else{
		req.body.username = req.body.username.toLowerCase();
		
		usersModel
		.find({'username' : {'$regex' : req.body.username} }
		, 'username img')
		.exec(function (err, data) {
			if(req.isAuthenticated){
				if(req.user.username==req.body.username){
					// isOwnProfile=true;
				}else{
					// isOwnProfile=false;
				}
			}else{
				// isOwnProfile=false;
			}
			res.json({
				// isOwnProfile:isOwnProfile,
				data:data
			});
		});
	}
});

router.get('/notifications/list', checkUser,function(req, res) {
	console.log(req.user._id);
	notificationsModel
	.find({ 'to': { $in : [req.user._id] },'seenBy': { $in: [req.user._id]} })
	.populate([
		{path:'type',select: 'text state'},
		{path:'senderId',select:'_id username img'},
		{path:'to',select:'_id username img'}
	])
	.exec(function (err, data) {
		res.json(data);
	});
});
router.get('/notifications/not-read-list', checkUser,function(req, res) {
	console.log(req.user._id);
	notificationsModel
	.find({ 'to': { $in : [req.user._id] },'seenBy': { $nin: [req.user._id]} })
	.populate([
		{path:'type',select: 'text state'},
		{path:'senderId',select:'_id username img'},
		// {path:'to',select:'_id username img'}
	])
	.exec(function (err, data) {
		res.json(data);
	});
});
router.post('/notifications/read-all', checkUser,function(req, res) {
	notificationsModel
	.update(
		{ 'to': { $in : [req.user._id] },'seenBy': { $nin: [req.user._id]} },
		{
			$push : {'seenBy': req.user._id} 
		}, 
		{
			multi: true
		},
		function(err, num) {
	    	if(err){
	    		res.status(500).send('Error');
	    		return false;
	    	}
	        res.send(num.nModified);
	    }
	)
});

router.get('/search/:keyword',function(req, res) {
	usersModel.search( {
		query_string: {
			fields: ['username','name'],
			query: '*'+req.params.keyword+'*'
		} 
	},{hydrate:true, hydrateOptions: {select: 'username img name _id address'}}, function(err, results) {
	    res.send(results);
  	});
});

router.get('/maybe-acquaintances',checkUser,acquaintancesWithRequests,function(req, res) {
	var query = {    
		"filtered": {
			"query": {             
				"bool": {                 
					"must_not": [                     
						{                         
							"term": {                             
								"_id": req.user._id
							},
						},
						{
							"terms" : { 
								"_id" : req.acquaintances,
				         	}
						}
					]             
				}         
			}, 
		} 
	};  
	
	var opts = {     
		"sort": [{             
			"_geo_distance": {                 
				"location": [                     
					req.user.location[0],                     
					req.user.location[1]
				],                 
				"order":"asc",
	            "unit":          "km",                 
	            "distance_type": "plane"             
	        }         
	    }],
	    hydrate:true, 
	    hydrateOptions: {select: 'username img address'}
	 //   "script_fields": {         
	 //   	"distance": "doc[\u0027location\u0027].distanceInMiles(41.2708115, -70.0264952)"     
		// } 
	};
 	usersModel.search(query, opts, function(err, res2) { /* ... */
 		res.json(res2);
 	})
});


module.exports = router;
