var express = require('express');
var	router = express.Router();
var checkUser = require("../app").checkUser;
var teamsModel = require('../models/teams.js');
var teamsUsersModel = require('../models/teamsUsers.js');
var acquaintancesModel = require('../models/acquaintances.js');


router.get('/myrequests/requestsCount', checkUser,function(req, res) {
	var requestsCount = 0;
	acquaintancesModel
	.find({
		'receiverId': req.user._id, 'offer.confirmed': false
	}, '_id')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data.length){
				requestsCount = requestsCount + data.length;
			}
			teamsUsersModel
			.find({
				'userId': req.user._id, 'offer.confirmed': false, 'offer.from': true
			}, '_id')
			.exec(function (err, data) {
				if(err){
					res.status(400).send('Xəta N3');
				}else{
					if(data.length){
						requestsCount = requestsCount + data.length;
					}
					res.json(requestsCount);
				}
			});
		}
	});
});

router.get('/myrequests/:page', checkUser, function(req, res) {
	if(req.params.page == 'oyun'){
		
	}else if(req.params.page == 'komanda'){
		teamsUsersModel
		.find({
			'userId': req.user._id, 'offer.confirmed': false, 'offer.from': true
		}, 'teamId offer')
		.populate([{path:'teamId',select:'_id name'},{path:'offer.by',select:'_id username img position'}])
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N3');
			}else{
				if(data.length){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});
	}else{
		acquaintancesModel
		.find({
			'receiverId': req.user._id, 'offer.confirmed': false
		}, 'senderId offer')
		.populate([{path:'senderId',select:'_id username img'}])
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N3');
			}else{
				if(data.length){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});	
	}
});

router.post('/myrequests/acceptUserRequest',checkUser, function(req, res) {
	acquaintancesModel
	.findOne({
		'receiverId': req.user._id, 'offer.confirmed': false
	}, '_id')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data){
				data.offer.confirmed = true;
				data.offer.confirmedDate = new Date();
				data.save();
				res.send('- Təklif sizin tərəfinizdən uğurla qəbul edildi.')
			}else{
				res.status(400).send('- Təklif təyin olunmadı.');
			}
		}
	});
});

router.post('/myrequests/rejectUserRequest', checkUser, function(req, res) {
	acquaintancesModel
	.findOneAndRemove({
		'receiverId': req.user._id, 'offer.confirmed': false
	}, '_id')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data){
				res.send('- Təklif sizin tərəfinizdən rədd edildi.')
			}else{
				res.status(400).send('- Təklif təyin olunmadı.');
			}
		}
	});
});


router.post('/myrequests/acceptTeamRequest', checkUser, function(req, res) {
	teamsUsersModel
	.findOne({
		'userId': req.user._id, 'offer.confirmed': false, 'offer.from': true,'_id':req.body.reqId
	}, '_id teamId')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data){
				data.offer.confirmed = true;
				data.offer.confirmedDate = new Date();
				data.save();
				teamsModel.findOne({ '_id': data.teamId },'members',function(err,data2){
					data2.members.push(req.user._id);
					data2.save();
				});
				res.send('- Təklif sizin tərəfinizdən uğurla qəbul edildi.')
			}else{
				res.status(400).send('- Təklif təyin olunmayıb.');
			}
		}
	});
});

router.post('/myrequests/rejectTeamRequest', checkUser, function(req, res) {
	teamsUsersModel
	.findOneAndRemove({
		'userId': req.user._id, 'offer.confirmed': false, 'offer.from': true,'_id':req.body.reqId
	}, '_id')
	.exec(function (err, data) {
		if(err){
			res.status(400).send('Xəta N3');
		}else{
			if(data){
				res.send('- Təklif sizin tərəfinizdən rədd edildi.')
			}else{
				res.status(400).send('- Təklif təyin olunmayıb.');
			}
		}
	});
});

module.exports = router;
