var express = require('express');
var	router = express.Router();
var checkUser = require("../app").checkUser;
var acquaintances = require("../app").acquaintances;
var posttypes = require("../app").posttypes;
var prepareEmit = require("../app").prepareEmit;
var prepareEmitMultipleUsers = require("../app").prepareEmitMultipleUsers;

var usersModel = require('../models/users.js');
var postsModel = require('../models/posts.js');
var postTypesModel = require('../models/post-type.js');
// var commentsModel = require('../models/comments.js');

// cloudinary
	// var cloudinary = require('cloudinary');
	// cloudinary.config({ 
	//   cloud_name: 'heveskar-az', 
	//   api_key: '696691114969959', 
	//   api_secret: 'vhoJSa5CaIdb-IzqMuy2wazbwb8' 
	// });
//
router.get('/posts', checkUser, function(req, res) {
	postsModel.find({
		'to': { $in: [req.user._id] }
	},'_id date imgs text type userId teamId gameId')
	.sort({'date': 'descending'})
	.limit(5)
	.populate([
		{path:'userId',select: '_id username img'},
		{path:'teamId',select: '_id name members'},
		{path:'type',select: '_id text icon'}
	])
	.exec(function(err, items) {
	    if(err){
	    	res.status(400).send('Xeta 1');
	    }else{
	    	if(items){
	    		usersModel.populate(
					items, 
					{ path: 'teamId.members',model: 'users',select: '_id username img' }, 
					function (err,callback3){
						res.json(callback3);
					}
				);
	    	}else{
	    		res.status(400).send('Xeta 2');	
	    	}
	    }
	})
	
});


router.post('/post', checkUser,acquaintances,posttypes, function(req, res) {
	if(!req.body.text && req.body.imgs){
		res.send(400).send('Boş elan paylaşmaq mümkün deyil');
		return false;
	}
	if(req.body.text.trim() == ''){
		res.send(400).send('Boş elan paylaşmaq mümkün deyil');
		return false;
	}
	req.acquaintances.push(req.user._id);
	var newItem = new postsModel({
  		type: "56d747a289ee3d057686297a",
		userId: req.user._id,
		to: req.acquaintances,
		seenBy: [],
		text: req.body.text
  	});			
  	newItem.save(function(err,saved){
        if(err){
        	console.log(err);
            res.status(400).send('Xəta N4');
        }else{
        	if(saved){
        		var data = {
        			acquaintances: req.acquaintances,
        			data: {
        				userId: {
            				_id: req.user._id,
            				username: req.user.username,
            				name: req.user.name,
            				img: req.user.img
            			},
            			post: {
            				text: saved.text,
            				// imgs: saved.imgs,
            				type: req.posttypes[saved.type],
            				date: saved.date
            			}
        			}
        		}
        		prepareEmitMultipleUsers(data,'new-post');
        		res.json({ post: data.post, sender: data.sender });
        	}else{
        		res.status(400).send('Xəta N5');
        	}
        }
    });
});

module.exports = router;
