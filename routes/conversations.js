var express = require('express');
var	router = express.Router();
var checkUser = require("../app").checkUser;
var prepareEmit = require("../app").prepareEmit;
var conversationsModel = require('../models/conversations.js');
var messagesModel = require('../models/messages.js');


router.get('/messages/:conversationId/:opponentId', checkUser,function(req, res) {
	var conversationId = req.params.conversationId;
	var opponentId = req.params.opponentId;
	if(req.params.conversationId == 0){
	    conversationsModel
	    .findOne({ 
	        'users':{ $all: [req.user._id,req.params.opponentId] }
	    })    
	    .exec(function (err, data) {
	        if(err){
	            res.send(false);
	        }else{
	            if(data){
        	        conversationId = data._id;
        	        messagesModel
        	        .find({ 'conversationId': conversationId })
        	        .populate([{path:'sender',select: '_id username img'}])
            		.sort([['date', 'descending']])
            		.limit(10)
        	        .exec(function(err, messages) {
        	            if(err){
        	                res.send(false);
        	            }else{
        	                if(messages){
						        res.json({'messages': messages});
        	                }else{
        	                    res.send(false);
        	                }
        	            }
        	        });
	            }else{
	                res.send(false);
	            }
	        }
	        
	    });
	}else{
		if(conversationId != 0){
		    conversationsModel
		    .findOne({ 
		        '_id' : conversationId,
		        'users': { $in : [req.user._id]}
		    })    
		    .exec(function (err, data) {
		        if(err){
		            res.send(false);
		        }else{
		            if(data){
	        	        conversationId = data._id;
	        	        messagesModel
	        	        .find({ 'conversationId': conversationId })
	        	        .populate([{path:'sender',select: '_id username img'}])
	            		.sort([['date', 'descending']])
	            		.limit(10)
	        	        .exec(function(err, messages) {
	        	            if(err){
	        	                res.send(false);
	        	            }else{
	        	                if(messages){
	        	                	res.json({'messages': messages});
	        	                }else{
	        	                    res.send(false);
	        	                }
	        	            }
	        	        });
		            }else{
		                res.send(false);
		            }
		        }
		        
		    });
		}else{
			res.send(false);
		}
	}
});

router.post('/send/', checkUser,function(req, res) {
	var conversationId = req.body.conversationId;
	var peerId = req.body.peerId;
	if(conversationId == 0){
	    conversationsModel
	    .findOne({ 
    		'users':{ $all: [req.user._id,req.body.peerId] }
	    })    
	    // .populate([{path:'sender',select: '_id username img'}])
	    .exec(function (err, data) {
	        if(err){
	            res.send(false);
	        }else{
	            if(data){
        	        conversationId = data._id;
        	        insertNewMessage();
	            }else{
	                //new conversation
	                var newConversation = new conversationsModel({
			  				users: [req.user._id,peerId],
                        });
                    newConversation.save(function(err,newCon){
                        if(err){
                            res.status(500).send('Xəta N4');
                        }else{
                            conversationId = newCon._id;
                            insertNewMessage();
                        }
                    });
	            }
	        }
	        
	    });
	}else{
	    conversationsModel
	    .findOne({ 
	        '_id' : conversationId,
	        'users': { $in : [req.user._id]}
	    })    
	    .exec(function (err, data) {
	        if(err){
	            res.send(false);
	        }else{
	            if(data){
	            	data.users.splice(data.users.indexOf(req.user._id),1)
	            	peerId = data.users[0];
        	        insertNewMessage();
	            }else{
	                res.send(false);
	            }
	        }
	        
	    });
	}
	
	function insertNewMessage(){
        var newMessage = new messagesModel({
                conversationId: conversationId,
                sender: req.user._id,
                message: req.body.message,
                seenBy: []
            });
        newMessage.save(function(err,newMsg){
            if(err){
                res.status(500).send('Xəta N4');
            }else{
            	var lastMessage = {
            		'_id' : newMsg._id,
            		'message': newMsg.message,
            		'date': newMessage.date,
            		'sender': req.user._id
            	}
                conversationsModel.findOneAndUpdate(
                	{'_id': newMsg.conversationId},
                	{ $set: 
                		{ 
                			lastMessage: lastMessage._id
                		} 
                	}
                	
                	,function(err,updated){
                	// console.log('===============')
                	// console.log(err);
                	// console.log(updated);
                	// console.log('===============')
                })
                var dataForSocket = {
					'opponentId': peerId,
					'newMsg': newMsg,
					'sender': {
						'_id': req.user._id,
						'username': req.user.username,
						'img': req.user.img
					}
				}
				prepareEmit(dataForSocket,'messages:new');
				res.send(newMsg)
            }
        });
	}
});

router.get('/list/', checkUser,function(req, res) {
    conversationsModel
    .find({ 
        'users':{ $in: [req.user._id] }
    },'lastMessage _id date')
    .sort({'lastMessage': -1})
    .select({ users: { $elemMatch: {$nin: [req.user._id]} } })
    .populate([{path:'users',select: '_id username img'},{path:'lastMessage'}])
    // .sort({'date': 'descending'})
    .exec(function (err, data) {
        if(err){
            res.send(false);
        }else{
            if(data){
            	// delete data.users[data.users.indexOf(req.user._id)];
            	console.log('==========')
            	data.forEach(function(item){
            		console.log(item._id);
            	})
    	        res.json(data);
            }else{
                res.send(false);
            }
        }
        
    });
});

router.get('/count', checkUser,function(req, res) {
    conversationsModel
    .find({ 
        'users': { $in: [req.user._id] }
    })
  	.distinct('_id')
    .exec(function (err, myConvs) {
        if(err){
            res.send(false);
        }else{
    		messagesModel
		    .find({ 
		        'seenBy': {$nin: [req.user._id] },
		        'sender': {$ne: req.user._id },
		        'conversationId': { $in: myConvs }
		    },'_id conversationId',{'group': 'conversationId'})
		    .count(function (err, count) {
		        if(err){
		            res.send(false);
		        }else{
		            res.json(count);
		        }
		        
		    });
        }
    });
});

router.post('/read-all', checkUser,function(req, res) {
	var opponentId = 0;
	conversationsModel
    .findOne({ 
        '_id' : req.body.conversationId,
        'users': { $in : [req.user._id]}
    })    
    .exec(function (err, data) {
        if(err){
            res.send(false);
        }else{
            if(data){
            	data.users.splice(data.users.indexOf(req.user._id),1)
            	opponentId = data.users[0];
    	        readAllMessages();
            }else{
                res.send(false);
            }
        }
        
    });
	
	function readAllMessages(){
		messagesModel.update({
	    	conversationId: req.body.conversationId, 
	    	seenBy: { $nin: [req.user._id] 	}
	    }, 
		{
			$push : {'seenBy': req.user._id} 
		}, 
		{
			multi: true
		}, 
		    function(err, num) {
		    	if(err){
		    		res.send(false);
		    		return false;
		    	}
		        var dataForSocket = {
		        	'opponentId' : opponentId,
		        	'conversationId': req.body.conversationId,
		        	'readerId':  req.user._id
		        }
		        prepareEmit(dataForSocket,'messages:read');
		        res.json(num.nModified);
		    }
		);
	}
});


module.exports = router;
