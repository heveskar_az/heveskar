var express = require('express');
var	router = express.Router();

var extend = require('node.extend');

var stadionModel = require('../models/stadion.js');
var scheduleModel = require('../models/schedule.js');
var stadiumsModel = require('../models/stadiums.js');

// cloudinary
	var cloudinary = require('cloudinary');
	cloudinary.config({ 
	  cloud_name: 'heveskar-az', 
	  api_key: '696691114969959', 
	  api_secret: 'vhoJSa5CaIdb-IzqMuy2wazbwb8' 
	});
//

// var paypal = require('paypal-rest-sdk');

// paypal.configure({
//   'host': 'api.sandbox.paypal.com',
//   'client_id': 'AS4iJOih8B3RAoH2JpweJarsH6aI79LEyMttnJ0Sldnn89WoyvvXT-4V8v3_76s_OW0PlnfJTwV4AFSa',
//   'client_secret': 'EPIiQ-SZTAm1rZgEVlxfZFkxjXUISdvEgelJar3CwjHsb7608nB0fg45daWHp2Z8dF4fOPdatiw8u1Qq'
// });
// paypal.generate_token(function(error, token){
//   if(error){
//     console.error(error);
//   } else {
//     console.log(token);
//   }
// });



router.get('/list', function(req, res) {
	// if(req.isAuthenticated()){
		stadionModel
		.find({}, '_id address controllerName mobile phone name')
		.exec(function (err, data) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(data){
				res.json(data);
			}else{
				if(err){
					res.status(500).send('Xəta var!');
				}else{
					res.json(null);
				}
			}	
		});
	// }else{
	// }
});
router.get('/stadiums/list', function(req, res) {
	// if(req.isAuthenticated()){
		stadiumsModel
		.find({status:true},'_id name type stad_id')
		.exec(function (err, data) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(data){
		  		var newArr = []; var i = 1;
		  		data.forEach(function(row){
		  			if(newArr[row.stad_id]==undefined){
		  				newArr[row.stad_id] = [];
		  			}
		  			newArr[row.stad_id].push(row);
		  			if(i==data.length){
		  				res.json(extend({}, newArr));
		  			}
		  			i++;
		  		});
			}else{
				if(err){
					res.status(500).send('Xəta var!');
				}else{
					res.json(null);
				}
			}	
		});
	// }else{
	// }
});

router.get('/stadium/images/:id', function(req, res) {
	stadiumsModel
	.findOne({_id: req.params.id}, 'images')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data){
			res.json(data);
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.json(null);
			}
		}	
	});
});
router.get('/stadium/info/:id', function(req, res) {
	stadiumsModel
	.findOne({_id: req.params.id}, '_id name schedule stad_id')
	.populate('stad_id','name')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data){
			res.json(data);
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.json(null);
			}
		}	
	});
});

router.post('/schedule', function(req, res) {
	// if(req.isAuthenticated()){	
		scheduleModel.find({ stadium: req.body.id,'date': req.body.date}, function (err, schedule) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(schedule){
				res.json(schedule);
			}else{
				if(err){
					res.status(500).send('Xəta var!');
				}else{
					res.json(null);
				}
			}	
		});
	// }else{
	// }
});
router.post('/schedule/register', function(req, res) {
	// if(req.isAuthenticated()){
		scheduleModel.findOne({ 
			'stadium': req.body.stadium,'date': req.body.date,
			'time': req.body.saat
		},
		function (err, schedule) {
		  	// if (err) return done(null, null);//handleError(err);
		  	if(schedule){
				res.json(schedule);
			}else{
				if(err){
					res.status(500).send('Xətar var!');
				}else{
					res.json(null);
				}
			}	
		});
	// }else{
	// }
});
router.post('/schedule/update', function(req, res) {
	if(req.isAuthenticated()){
		scheduleModel.findOne(
			{
				date: req.body.item.date,
				time: req.body.item.time,
				stadium: req.body.item.stadium,
				stad_id: req.body.item.stad_id
		 	}, 
			function (err, doc){
				if(doc){
	            	if(req.body.item.status)				doc.status = req.body.item.status;
            		if(req.body.item.client_id)				doc.client_id = req.body.item.client_id;
	            	// if(req.body.item.client_id.name)		doc.client_id.name = req.body.item.client_id.name;
	            	// if(req.body.item.client_id.phone)		doc.client_id.phone = req.body.item.client_id.phone;
	            	// if(req.body.item.client_id.email)		doc.client_id.email = req.body.item.client_id.email;
	            	if(req.body.item.price){
	            		doc.price = req.body.item.price;
	            	}else{
	            		doc.price = '';
            		}
	            	if(req.body.item.payment)				doc.payment = req.body.item.payment;
	            	doc.updatedDate = new Date();
	            	doc.save();
	            	if (err) {
	            		res.send('Xəta var')
	            	}else{
				  		res.send('Dəyişiklik yadda saxlanıldı');
			  		}
			  	}else{
			  		var newReg = new scheduleModel({
			  				stad_id: req.body.item.stad_id,
			  				date: req.body.item.date,
							time: req.body.item.time,
							stadium: req.body.item.stadium,
                            status: req.body.item.status,
                            client_id: req.body.item.client_id,
                            price: req.body.item.price,
                            payment: req.body.item.payment,                          
                            createdDate: new Date(),
                            updatedDate: new Date()
                        });
                    newReg.save(function(err){
                        if(err){
                            res.send('Xəta var');
                        }else{
                            res.send('Dəyişiklik yadda saxlanıldı');
                        }
                    });
			  	}
        	}
        );
	}else{
		res.redirect('/');
	}
});
router.post('/schedule/delete', function(req, res) {
	if(req.isAuthenticated()){
		scheduleModel.findOne(
			{
				date: req.body.item.date,
				time: req.body.item.time,
				stadium: req.body.item.stadium,
				stad_id: req.body.item.stad_id 	
		 	}).remove(function(err){
	 			if(err){
	 				res.status(500).send('Xəta var');
 				}else{
 					res.send('Dəyişiklik yadda saxlanıldı');
 				}
		 	});
	}else{
		res.status(500).send('Xəta var');
	}
});

module.exports = router;