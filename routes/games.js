var express = require('express');
var	router = express.Router();
var checkUser = require("../app").checkUser;
var acquaintances = require("../app").acquaintances;
var posttypes = require("../app").posttypes;
var prepareEmitMultipleUsers = require("../app").prepareEmitMultipleUsers;

var gamesModel = require('../models/games.js');
var gameOffersModel = require('../models/gameOffers.js');
var teamsUsersModel = require('../models/teamsUsers.js');
var commentsModel = require('../models/comments.js');

var postsModel = require('../models/posts.js');
// cloudinary
	// var cloudinary = require('cloudinary');
	// cloudinary.config({ 
	//   cloud_name: 'heveskar-az', 
	//   api_key: '696691114969959', 
	//   api_secret: 'vhoJSa5CaIdb-IzqMuy2wazbwb8' 
	// });
//



router.get('/connectedUsers/:id', function(req, res) {
	if(req.params.id){
		gamesModel
		.findOne({_id: req.params.id}, '_id connected')
		.populate([{path:'connected',select: '_id username img'}])
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N4');
			}else{
				if(data){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});
	}else{
		res.status(400).send('Oyun tapılmadı');
	}
});

router.post('/deleteComment', checkUser,function(req, res) {
	var removerIsGameCreator = false;
	if(!req.body.commentId || !req.body.gameId){
		res.status(400).send('Məlumatlarda yanlışlıq var.');
	}else{
		gamesModel
		.findOne({_id: req.body.gameId}, '_id created.by comments')
		.exec(function(err,data){
			console.log(data);
			console.log(req.user._id);
			console.log('------------');
			console.log(req.body.commentId);
			if(data.created.by == req.user._id){
				removerIsGameCreator = true;
			}

			commentsModel
			.findOne({_id: req.body.commentId},'senderId')
			.exec(function(err,data2){
				if(err){
                    res.status(400).send('Xəta N4');
                }else{
                	if(data2){
                		if(removerIsGameCreator != true){
                			if(data2.senderId == req.user._id){
                				data2.remove(function(err){
                    				gamesModel.findOneAndUpdate({ _id: req.body.gameId }, 
	                    				{$pull: {comments: req.body.commentId}}, function(err, data){
								        if(err) {
								          return res.status(500).json({'error' : 'error in deleting address'});
								        }

								        res.json(req.body.commentId);

							      	});
                    			});
                			}
                		}else{

                			data2.remove(function(err){
                				gamesModel.findOneAndUpdate({ _id: req.body.gameId }, 
                    				{$pull: {comments: req.body.commentId}}, function(err, data){
							        if(err) {
							          return res.status(500).json({'error' : 'error in deleting address'});
							        }

							        res.json(req.body.commentId);

						      	});
                			});
                		}
					}
                }
			});
		});
	}
});
router.post('/newComment', checkUser, function(req, res) {
	if(!req.body.comment || !req.body.gameId){
		res.status(400).send('Məlumatlarda yanlışlıq var.');
	}else{
		gamesModel
		.findOne({_id: req.body.gameId,status: 0}, '_id connected comments')
		.exec(function(err,data){
			if(!data.opponent){
				// console.log(data);
				// // return false;
				// var newComment = {
				// 	senderId 	: req.user._id,
				// 	comment 	: req.body.comment,
				// 	date 		: new Date(),
				// 	replies		: []
				// }
				// data.comments.push(newComment);
				// console.log(data);
				// data.save(function(err){
				// 	if(err){
    //                     res.status(400).send('Xəta N4');
    //                 }else{
    //                 	res.send({callback: 'success'});
    //                 }
				// });
			}else{
				if(data.connected.indexOf(req.user._id) != -1 ){
					
				}else{
					res.status(400).send('- Sizin bu oyuna rəy yazmaq üçün icazəniz yoxdur!');
					return false;
				}
			}
			var newComment = new commentsModel({
  				senderId 	: req.user._id,
				comment 	: req.body.comment,
				date 		: new Date(),
				replies		: []
            });

            newComment.save(function(err,callback){
                if(err){
                    res.status(400).send('Xəta N4');
                }else{
                	if(req.body.commentId){
						commentsModel
						.findOne({_id: req.body.commentId}, '_id replies')
						.exec(function(err,dataComment){
							if(dataComment){
								dataComment.replies.push(callback._id);
								dataComment.lastUpdate = new Date();
								dataComment.save(function(err,dataCommentData){
									commentsModel.populate(callback, {path: 'senderId', model: 'users'},function(err,finalData){
										if(finalData){
											res.send(finalData);
										}
		                        	});
								});
							}
						});
            		}else{
            			data.comments.push(callback._id);
						data.save(function(err){
							if(err){
	                            res.status(400).send('Xəta N4');
	                        }else{
	                        	commentsModel.populate(callback, {path: 'senderId', model: 'users'},function(err,finalData){
									if(finalData){
										res.send(callback);
									}
	                        	});
	                        }
						});
            		}
                }
            });
		});
	}
});



router.post('/acceptUserRequest', checkUser,function(req, res) {
	if(req.body.offerId && req.body.gameId){
		gamesModel
		.findOne({_id: req.body.gameId,'created.by': req.user._id, opponent: false, status: 0 })
		.exec(function(err,gameData){
			if(gameData){
				gameOffersModel
				.findOne({_id: req.body.offerId, gameId: req.body.gameId, 'offer.confirmed': false })
				.exec(function(err,data){
					if(data){
						var offer = {
							date: data.offer.date,
							confirmed: true,
							confirmedDate: new Date()
						}
						data.offer = offer;
						data.save(function(err){
							if(err){ 
								res.status(400).send('- Xəta N1!');
							}else{
								gameData.opponent = true;
								gameData.oppTeamId = data.senderTeamId;
								gameData.save(function(err){
									if(err){
										res.status(400).send('- Xəta N2!');
									}else{
										res.send( {callback: 'success'} );
									}
								});
							}
						});
					}else{
						res.status(400).send('- Təklif tapılmadı.');
					}
				});
			}else{
				res.status(400).send('- Oyun tapılmadı və ya artıq rəqib seçilib.');
			}
		});
	}else{
		res.status(400).send('- Məlumatlarda yanlışlıq var.');
	}
	// acquaintancesModel
	// .findOne({
	// 	'receiverId': req.user._id, 'offer.confirmed': false
	// }, '_id')
	// .exec(function (err, data) {
	// 	if(err){
	// 		res.status(400).send('Xəta N3');
	// 	}else{
	// 		if(data){
	// 			data.offer.confirmed = true;
	// 			data.offer.confirmedDate = new Date();
	// 			data.save();
	// 			res.send('- Təklif sizin tərəfinizdən uğurla qəbul edildi.')
	// 		}else{
	// 			res.status(400).send('- Təklif təyin olunmadı.');
	// 		}
	// 	}
	// });
});


router.get('/showOffers/:id', checkUser, function(req, res) {
	if(!req.params.id){
		res.status(400).send('Məlumatlarda yanlışlıq var. Komandanızı seçin.');
	}else{

		gamesModel
		.find({_id: req.params.id,'created.by': req.user._id }, '_id')
		.count(function(err,count){
			if(count<0){
				res.status(400).send({});
			}else{
				gameOffersModel
				.find({gameId: req.params.id,'offer.confirmed': false })
				.populate([{path:'senderTeamId',select: '_id name img'},{path:'senderId',select: '_id username img'}])
				.sort([['offer.date', 'descending']])
				.exec(function(err,data){
					if(data){
						res.send(data);
					}
				});
			}
		});
	}
});


router.post('/newOffer', checkUser, function(req, res) {
	if(!req.body.offer.teamId || !req.body.offer.gameId){
		res.status(400).send('Məlumatlarda yanlışlıq var. Komandanızı seçin.');
	}else{
		teamsUsersModel
		.find({teamId: req.body.offer.teamId, userId: req.user._id, 'offer.confirmed': true}, '_id')
		.count(function (err, count1){
		  	// if (err) return done(null, null);//handleError(err);
		  	if(count1<0){
				res.status(400).send('- Belə komanda mövcud deyil və ya siz heyətində deyilsiniz.');
			}else{
				if(err){
					res.status(400).send('Xəta N3!');
				}else{
					gamesModel
					.find({_id: req.body.offer.gameId,status: 0, opponent: false}, '_id')
					.count(function(err,count2){
						if(count2<0){
							res.status(400).send('- Oyun tapılmadı və ya artıq rəqib tapılıb!');
						}else{
							gameOffersModel
							.find({gameId: req.body.offer.gameId,senderTeamId: req.body.offer.teamId}, '_id')
							.count(function(err,countOffers){

								if(countOffers>0){
									res.status(400).send('- Seçdiyiniz komanda tərəfindən artıq təklif göndərilib!');
								}else{

									var newOffer = new gameOffersModel({
						  				gameId  	: 		req.body.offer.gameId,
						  				senderId    : 		req.user._id, 
						  				senderTeamId: 		req.body.offer.teamId,
						  				text		: 		req.body.offer.text
			                        });
				                    newOffer.save(function(err,callback){
				                        if(err){
				                            res.status(400).send('Xəta N4');
				                        }else{
				                        	res.send({callback: 'success'});
				                        }
				                    });
								}
							});
						}
					});
				}
			}	
		});
	}
});



router.post('/new', checkUser,acquaintances,posttypes, function(req, res) {
	if(!req.body.game.team  || !req.body.game.countMembers || !req.body.game.countMembers || 
		!req.body.game.plan.type ||
			(req.body.game.plan.type == 2 && (!req.body.game.plan.winner || !req.body.game.plan.loser)) || 
			(req.body.game.team == 1 && !req.body.game.teamName) || 
			(req.body.game.team == 2 && !req.body.game.division) 
			// || [5, 6, 7, 8].indexOf(req.body.game.countMembers) == -1
	){
		res.status(400).send('Məlumatlarda yanlışlıq var. *-lu bölmələri dəqiqliklə doldurun.');
	}else{
		if(req.body.game.team == 1 && req.body.game.teamName){
			teamsUsersModel
			.find({teamId: req.body.game.teamName, userId: req.user._id, 'offer.confirmed': true}, '_id')
			.count(function (err, count){
			  	// if (err) return done(null, null);//handleError(err);
			  	if(count<0){
					res.status(400).send('- Belə komanda mövcud deyil və ya siz heyətində deyilsiniz.');
				}else{
					if(err){
						res.status(400).send('Xəta N3!');
					}else{
						var created = {
							by  : req.user._id,
							date: new Date()
						}
						var connected = [];

						if(req.body.game.team == 1){
							var i = 0; 
							teamsUsersModel
							.find({teamId: req.body.game.teamName, 'offer.confirmed': true}, '_id userId teamId')
							.populate({path: 'teamId',select: '_id name'})
							.exec(function (err, data){
								if(err) res.status(400).send('Xəta N4!');
								if(data.length){
									var teamName = data[0].teamId.name;
									console.log('==============')
									data.forEach(function(item){
									i++;
									connected.push(item.userId);
									if(data.length == i){
										var newGame = new gamesModel({
							  				team: req.body.game.team,
							  				teamId: req.body.game.teamName,
							  				division: req.body.game.division, 
							  				countPlayers: req.body.game.countMembers,
							  				plan: req.body.game.plan,
							  				stadium: req.body.game.stadium,
							  				date: req.body.game.date,
							  				created: created,
							  				status: 0,
							  				connected : connected
				                        });
					                    newGame.save(function(err,callback){
					                        if(err){
					                        	console.log(err);
					                            res.status(400).send('Xəta N4');
					                        }else{
					                        	console.log(callback);
					                        	/************* POST NEW GAME *************************************/
					                        	req.acquaintances.push(req.user._id);
					                        	var newItem = new postsModel({
											  		type: "56d747f689ee3d057686297b",
													userId: req.user._id,
													to: req.acquaintances,
													teamId: req.body.game.teamName,
													gameId: callback._id,
													seenBy: [],
											  	});			
											  	newItem.save(function(err,saved){
											        if(err){
											        	console.log(err);
											            res.status(400).send('Xəta N4');
											        }else{
											        	if(saved){
											        		var data = {
											        			acquaintances: req.acquaintances,
											        			data: {
											        				userId: {
											            				_id: req.user._id,
											            				username: req.user.username,
											            				name: req.user.name,
											            				img: req.user.img
											            			},
											            			post: {
											            				text: saved.text,
											            				teamId: {
											            					'_id': saved.teamId,
											            					'name': teamName
											            				},
																		gameId: saved.gameId,
											            				type: req.posttypes[saved.type],
											            				date: saved.date
											            			}
											        			}
											        		}
											        		prepareEmitMultipleUsers(data,'new-post');
											        		res.json({ post: data.post, sender: data.sender });
											        	}else{
											        		res.status(400).send('Xəta N5');
											        	}
											        }
											    });
					                        	
					                        	/************* POST NEW GAME *************************************/
					                        }
					                    });
									}
								});
								}else{
									res.status(400).send(false);
								}
							});
						}
					}
				}	
			});
		}else{
			var newGame = new gamesModel({
  				team: req.body.game.team,
  				division: req.body.game.division, 
  				countPlayers: req.body.game.countMembers,
  				plan: req.body.game.plan,
  				stadium: req.body.game.stadium,
  				date: req.body.game.date,
  				created: created,
  				status: 0
            });
            newGame.save(function(err,callback){
                if(err){
                    res.status(400).send('Xəta N4');
                }else{
                	res.send({callback: 'success',id: callback._id});
                }
            });
		}
	}
});

router.get('/shortlist/:team', function(req, res) {
	if(req.params.team){
		gamesModel
		.find({
			'status': 0,
			'team' : req.params.team
		})
		.populate([{path:'teamId',select: '_id name img'},{path:'created.by',select: '_id username img'}])
		.sort([['created.date', 'descending']])
		.limit(5)
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N4');
			}else{
				if(data.length){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});
	}else{
		gamesModel
		.find({
			'status': 0
		})
		.populate([{path:'teamId',select: '_id name img'},{path:'created.by',select: '_id username img'}])
		.sort([['created.date', 'descending']])
		.limit(10)
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N4');
			}else{
				if(data.length){
					res.json(data);
				}else{
					res.send(false);
				}
			}
		});
	}
});
router.get('/more/:id', function(req, res) {
	if(req.params.id){
		gamesModel
		.findOne({
			_id: req.params.id
		})
		.populate([
				{path:'teamId',select: '_id name img statistics'},
				{path:'oppTeamId',select: '_id name img statistics'},
				{path:'created.by',select: '_id username img'},
				{
					path:'comments', 
					options: { 
						sort: { 'lastUpdate': -1 }, 
						//limit: 2 
					}
				},
		])
		.exec(function (err, data) {
			if(err){
				res.status(400).send('Xəta N4');
			}else{
				if(data){
					commentsModel.populate(
						data, 
						{ path: 'comments.senderId',model: 'users',select: '_id username img comment date' }, 
						function (err,callback){
							commentsModel.populate(
								callback, 
								{ 
									path: 'comments.replies',
									model: 'comments',
									select: '_id comment date senderId',
									options: { 
										//limit: 4 , 
										sort: { 'date': -1 } 
									}
								}, 
								function (err,callback2){
									commentsModel.populate(
										callback2, 
										{ path: 'comments.replies.senderId',model: 'users',select: '_id username img' }, 
										function (err,callback3){
											res.json(callback3);
										}
									);
								}
							);
						}
					);
				}else{
					res.send(false);
				}
			}
		});
	}else{
		res.status(400).send('- Oyun tapılmadı');
	}
});


// router.post('/members/checkIsJoined',function(req,res){
// 	if(req.isAuthenticated()){
// 		teamsUsersModel
// 		.findOne({
// 			'userId':req.user._id,'teamId':req.body.teamId
// 		}, '_id offer')
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xeta N1');
// 			}else{
// 				if(data){
// 					if(data.offer.confirmed==true){
// 						res.send(true);
// 					}else{
// 						if(data.offer.from==false){
// 							res.send('2');
// 						}else{
// 							res.send('3');
// 						}
// 					}
// 				}else{
// 					res.send(false);
// 				}
// 			}
// 		});
// 	}else{
// 		res.send(false);
// 	}
// });
// router.post('/members/join', function(req, res) {
// 	if(req.isAuthenticated()){	
// 		if(req.body.teamId){
// 			teamsUsersModel
// 			.findOne({
// 				'userId':req.user._id,'teamId':req.body.teamId
// 			}, '_id offer')
// 			.exec(function (err, data){
// 			  	if(data){
// 		  			//teklif gonderilib ve teklifden imtina edir
// 		  			teamsUsersModel.findOneAndRemove({
// 		  				'userId':req.user._id,'teamId':req.body.teamId
// 		  			}, function(err){
// 		  				if(err){
// 		  					res.status(400).send('Xəta N3');
// 		  				}else{
// 		  					res.send(false);
// 		  				}
// 		  			});
// 			  	}else{
// 			  		//teklif gonder
// 			  		var offer = { date: new Date(), confirmed: false,by:req.user._id,from:false };
// 					var newReg = new teamsUsersModel({
// 			  				userId: req.user._id,
// 			  				teamId: req.body.teamId,
// 			  				offer: offer
//                         });
//                     newReg.save(function(err){
//                         if(err){
//                             res.status(400).send('Xəta N4');
//                         }else{
//                             res.send('2');
//                         }
//                     });
// 			  	}
// 			});
// 		}else{
// 			res.status(400).send('- Komanda seçilməyib');
// 		}
// 	}else{
// 		res.status(400).send('- Əməliyyatı yerinə yetirmək üçün qeydiyyatdan keçməyiniz və ya sayta həvəskar kimi daxil olmanız mütləqdir.');
// 	}
// });


// router.post('/members/acceptJoining', function(req, res) {
// 	if(req.isAuthenticated()){	
// 		if(req.body.reqId){
// 			teamsUsersModel
// 			.findOne({
// 				_id:req.body.reqId
// 			}, '_id teamId offer')
// 			.exec(function (err, data){
// 			  	if(!err){
// 				  	if(data){ 
// 			  			//teklif movcuddur
// 			  			teamsUsersModel.findOne({
// 			  				'teamId':data.teamId,'userId':req.user._id
// 			  			},'_id', function(err,data2){
// 			  				if(err){
// 			  					res.status(400).send('Xəta N2');
// 			  				}else{
// 			  					if(data2){
// 			  						data.offer.confirmed = true;
// 			  						data.offer.by = req.user._id;
// 			  						data.offer.confirmDate = new Date();
// 			  						data.save();
// 			  						res.send(true);
// 			  					}else{
// 			  						res.status(400).send('- Əməliyyatı yerinə yetirməyə hüququnuz yoxdur');
// 			  					}
// 			  				}
// 			  			});
// 				  	}else{
// 				  		res.status(400).send('- Komandaya qoşulma təklifi təyin olunmadı');
// 				  	}
// 			  	}else{
// 			  		res.status(400).send('- Xəta N1');
// 			  	}
// 			});
// 		}else{
// 			res.status(400).send('- Komanda seçilməyib');
// 		}
// 	}else{
// 		res.status(400).send('- Əməliyyatı yerinə yetirmək üçün qeydiyyatdan keçməyiniz və ya sayta həvəskar kimi daxil olmanız mütləqdir.');
// 	}
// });

// router.post('/members/rejectJoining', function(req, res) {
// 	if(req.isAuthenticated()){	
// 		if(req.body.reqId){
// 			teamsUsersModel
// 			.findOne({
// 				_id:req.body.reqId
// 			}, '_id teamId offer')
// 			.exec(function (err, data){
// 			  	if(!err){
// 				  	if(data){ 
// 			  			//teklif movcuddur
// 			  			teamsUsersModel.findOne({
// 			  				'teamId':data.teamId,'userId':req.user._id,captain:true
// 			  			},'_id', function(err,data2){
// 			  				if(err){
// 			  					res.status(400).send('Xəta N2');
// 			  				}else{
// 			  					if(data2){
// 			  						data.remove();
// 			  						res.send(true);
// 			  					}else{
// 			  						res.status(400).send('- Əməliyyatı yerinə yetirməyə hüququnuz yoxdur');
// 			  					}
// 			  				}
// 			  			});
// 				  	}else{
// 				  		res.status(400).send('- Komandaya qoşulma təklifi təyin olunmadı');
// 				  	}
// 			  	}else{
// 			  		res.status(400).send('- Xəta N1');
// 			  	}
// 			});
// 		}else{
// 			res.status(400).send('- Komanda seçilməyib');
// 		}
// 	}else{
// 		res.status(400).send('- Əməliyyatı yerinə yetirmək üçün qeydiyyatdan keçməyiniz və ya sayta həvəskar kimi daxil olmanız mütləqdir.');
// 	}
// });


// router.get('/list/', function(req, res) {

// 	teamsModel
// 	.find({},'_id img name statistics.points',{sort: {'statistics.points': 1 }})
// 	.exec(function (err, data) {
// 		if(err){
// 			res.status(400).send('Xəta N3');
// 		}else{
// 			if(data.length){
// 				res.json(data);
// 			}else{
// 				res.send(false);
// 			}
// 		}
// 	});
// });

// router.get('/myteams/', function(req, res) {
// 	if(!req.isAuthenticated()){
// 		res.status(400).send('- Komandalarınızın siyahısına baxmaq üçün sayta həvəskar kimi daxil olmalısız');
// 	}else{
// 		teamsUsersModel
// 		.find({
// 			'userId': req.user._id, 'offer.confirmed': true
// 		}, 'teamId')
// 		.populate('teamId','_id img name statistics')
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xəta N3');
// 			}else{
// 				if(data.length){
// 					res.json(data);
// 				}else{
// 					res.send(false);
// 				}
// 			}
// 		});
// 	}
// });

// router.get('/myteams/list/:name', function(req, res) {
// 	if(!req.isAuthenticated()){
// 		res.status(400).send('- Komandalarınızın siyahısına baxmaq üçün sayta həvəskar kimi daxil olmalısız');
// 	}else{
// 		if(req.params.name){
// 			teamsUsersModel
// 			.find({
// 				'userId': req.user._id, 'offer.confirmed': true
// 			}, 'teamId')
// 			.populate('teamId','_id img name statistics',{ 'name': {'$regex' : req.params.name} })
// 			.limit(7)
// 			.exec(function (err, data) {
// 				if(err){
// 					res.status(400).send('Xəta N3');
// 				}else{
// 					if(data.length){
// 						res.json(data);
// 					}else{
// 						res.send(false);
// 					}
// 				}
// 			});
// 		}
// 	}
// });

// router.post('/delImg', function(req, res) {
// 	teamsModel
// 	.findOne({name : req.body.teamName}, 'img',function (err, data) {
// 	  	// if (err) return done(null, null);//handleError(err);
// 	  	if(err){
// 			res.status(400).send('Xəta N1!');
// 		}else{
// 			if(data.img!="http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/teams/noTeamPhotoHeveskarAz.png"){
// 				cloudinary.uploader.destroy('teams/'+req.body.teamName, function(result) {
// 				 	data.img = "http://res.cloudinary.com/heveskar-az/image/upload/v1436305685/teams/noTeamPhotoHeveskarAz.png"
// 	            	data.save();
// 	            	res.send(true);
// 				});
// 			}else{
// 				res.status(400).send('Sizin komanda şəkliniz yoxdur');
// 			}	
// 		}
// 	});
// });

// router.post('/uploadImg', function(req, res) {
// 	if(req.isAuthenticated()){
// 		if(req.body.data && req.body.teamName){
// 			cloudinary.uploader.upload(req.body.data, function(result) { 
// 				teamsModel
// 				.findOne({name: req.body.teamName},'img',function(err,doc){
// 					if(err){
// 						res.status(400).send('Xəta N2!');
// 					}else{
// 						doc.img = result.url;
// 						doc.save();
// 						res.send(result.url);
// 					}
// 				});
// 			},{ public_id: "teams/"+req.body.teamName});
// 		}else{
// 			res.status(400).send('Xəta var! Lütfən, şəkil seçin!');
// 		}
// 	}else{
// 		res.status(400).send('Xəta N1');
// 	}
// });

// router.post('/update', function(req, res) {
// 	if(req.isAuthenticated()){
// 		if(req.body.teamName && req.body.teamId && req.body.teamName.length >= 4 && req.body.teamName.length <= 15){
// 			req.body.teamName = req.body.teamName.toLowerCase();
// 			teamsUsersModel
// 			.findOne({'userId': req.user._id,'teamId': req.body.teamId,'offer.confirmed':true}, '_id')
// 			.exec(function (err, data) {
// 				if(!err){
// 					if(data){
// 						teamsModel
// 						.find({_id: { $ne : req.body.teamId },
// 							name:req.body.teamName
// 						}, '_id')
// 						.exec(function (err, data2) {
// 						  	if(data2.length){
// 								res.status(400).send('- Bu adda komanda mövcuddur.Lütfən, başqa ad seçin.');
// 							}else{
// 								if(err){
// 									res.status(400).send('Xəta N3!');
// 								}else{
// 									teamsModel
// 									.findOne({_id: req.body.teamId},'name',function(err,doc){
// 										if(err){
// 											res.status(400).send('Xəta N2!');
// 										}else{
// 											doc.name = req.body.teamName;
// 											doc.save();
// 											res.send('- Dəyişiklik uğurla yerinə yetirildi.');
// 										}
// 									});
// 								}
// 							}	
// 						});
// 					}else{
// 						res.status(400).send('- Sizin əməliyyatı yerinə yetirmək hüququnuz yoxdur');	
// 					}
// 				}else{
// 					res.status(400).send('Xəta N4!');
// 				}
// 			});
// 		}else{
// 			res.status(400).send('- Komanda adı ən az 4, ən çox 15 hərfdən ibarət olmalıdır.')
// 		}
// 	}else{
// 		res.status(400).send('- Əməliyyatı yerinə yetirmək üçün qeydiyyatdan keçməyiniz və ya sayta həvəskar kimi daxil olmanız mütləqdir.')
// 	}
// });

// router.get('/info/:name', function(req, res) {
// 	if(!req.params.name){
// 		res.status(400).send('Xəta N1');
// 	}else{
// 		req.params.name = req.params.name.toLowerCase();
// 		teamsModel
// 		.findOne({
// 			name: req.params.name
// 		}, '_id img createdBy name statistics')
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xəta N3');
// 			}else{
// 				if(data){
// 					teamsUsersModel
// 					.find({ 'teamId': data._id },'userId captain offer')
// 					.populate(
// 						{path:'userId',select: '_id username img position'}
// 					)
// 					.exec(function (err2, data2) {
// 						var members = [];
// 						var wantedMembers = [];
// 						var captain = false;
// 						if(err2){
// 							res.status(400).send('Xəta N2');
// 						}else{
// 							if(data2){
// 								var i=0;
// 								data2.forEach(function(item){
// 									i++;
// 									if(item.offer.confirmed==true){
// 										members.push({userId:item.userId,captain:item.captain})
// 									}
// 									if(item.offer.confirmed==false && item.offer.from==false){
// 										wantedMembers.push({userId:item.userId,reqId:item._id})
// 									}
// 									if(req.isAuthenticated()){
// 										if(captain==false && item.captain==true && req.user._id==item.userId._id){
// 											captain = true;
// 										}
// 									}
// 									if(i==data2.length){
// 										res.json({
// 											captain:captain,dataTeam:data,dataMembers: members,
// 											wantedMembers: wantedMembers
// 										});
// 									}
// 								});
// 							}
// 						}
// 					});
// 				}else{
// 					res.json(null);
// 				}
// 			}
// 		});
// 	}
// });
// router.post('/checkTeamName', function(req, res) {
// 	req.body.name = req.body.name.toLowerCase();
// 	teamsModel
// 	.find({name: req.body.name}, '_id')
// 	.exec(function (err, data) {
// 	  	// if (err) return done(null, null);//handleError(err);
// 	  	if(data.length){
// 			res.send(false); //qeydiyyat mumkun deyil
// 		}else{
// 			if(err){
// 				res.status(500).send('Xəta var!');
// 			}else{
// 				res.send(true); //qeydiyyat mumkundur
// 			}
// 		}	
// 	});
// });


// router.post('/addUser', function(req, res) {
// 	if(req.isAuthenticated()){	
// 		if(req.body.userId && req.body.teamId){
// 			teamsUsersModel
// 			.find({teamId: req.body.teamId,userId: req.body.userId}, '_id')
// 			.exec(function (err, data){
// 			  	if(data.length){
// 					res.status(500).send('- Seçilmiş oyunçu bu komandanın heyətində var və ya təklif göndərilib');
// 				}else{
// 					if(err){
// 						res.status(500).send('Xəta N3!');
// 					}else{
// 						var offer = { from: true, by: req.user._id, date: new Date(), confirmed: false };
// 						var newReg = new teamsUsersModel({
// 				  				teamId: req.body.teamId,
// 				  				userId: req.body.userId,
// 				  				captain: false,
// 				  				offer: offer
// 	                        });
// 	                    newReg.save(function(err){
// 	                        if(err){
// 	                            res.status(500).send('Xəta N4');
// 	                        }else{
// 	                            res.send('- Oyunçuya komandaya qoşulmaq üçün təklif göndərildi');
// 	                        }
// 	                    });
// 					}
// 				}	
// 			});
// 		}else{
// 			res.status(500).send('- Komanda və ya oyunçu seçilməyib');
// 		}
// 	}else{
// 		res.status(500).send('- Əməliyyatı yerinə yetirmək üçün qeydiyyatdan keçməyiniz və ya sayta həvəskar kimi daxil olmanız mütləqdir.');
// 	}
// });

/*
router.post('/checkPhone', function(req, res) {
	usersModel
	.find({phone: req.body.phone}, 'phone')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
			res.send(false); //qeydiyyat mumkun deyil
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});
router.post('/checkEmail', function(req, res) {
	usersModel
	.find({email: req.body.email}, 'email')
	.exec(function (err, data) {
	  	// if (err) return done(null, null);//handleError(err);
	  	if(data.length){
			res.send(false); //qeydiyyat mumkun deyil
		}else{
			if(err){
				res.status(500).send('Xəta var!');
			}else{
				res.send(true); //qeydiyyat mumkundur
			}
		}	
	});
});


/* GET home page. *
router.get('/info', function(req, res) {
	res.json({
		isAuthenticated: req.isAuthenticated(),
		user: req.user
	});
});

*/
module.exports = router;
