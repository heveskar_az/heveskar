var express = require('express');
var socket = require('socket.io');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file

var mongoose = require('mongoose');
var fs = require('fs');
var app = express();
var server = require('http').Server(app);
var io = socket.listen(server);

//mongoose connection 
	mongoose.connect(config.database);
	//models
		var usersModel = require('./models/users.js');
		var roomsModel = require('./models/rooms.js');
//
app.set('superSecret', config.secret);

app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({ extended: true },{limit: '10mb'}));

//enable CORS
	app.all('/*', function(req, res, next) {
	  // CORS headers
	  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
	  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	  // Set custom headers for CORS
	  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	  if (req.method == 'OPTIONS') {
	    res.status(200).end();
	  } else {
	    next();
	  }
	});
//

// get an instance of the router for api routes
	var apiRoutes = express.Router(); 
	app.use('/api', apiRoutes);
	//socket settings
		apiRoutes.use(function(req, res, next) { req.io = io; next();	});
		// var users = [];
		
		//Important functions
			module.exports.getUserSocket = function(sockets, userId){
				var userSockets = Object.keys(sockets).filter(function (key) { 
			        return sockets[key].userData && sockets[key].userData._id == userId;
			    }).map(function(key) {
			        return sockets[key];
			    });
				return userSockets;
		    }
		    module.exports.joinRoom = function(socket, roomId){
				if(socket){
					if(socket.rooms[roomId] == undefined){
						socket.join(roomId)
					}
					
					roomsModel.update({_id: roomId},{$addToSet: {connectedUsers: socket.userData._id}},{upsert:true},function(err){
				        if(err){
				                console.log(err);
				        }else{
				        	socket.currentRoom = roomId;
				                console.log("Successfully added");
				        }
					});
					// io.in(roomId).emit('new-user',{userData: socket.userData, roomId: roomId})
					io.emit('joined-room',{userData: socket.userData, roomId: roomId})
					console.log(socket.userData.teamName+ ' joined to '+ roomId)
				}
				return true;
		    }
		//
		io.sockets.on('connection', function (socket) {
			console.log('+++ '+socket.id + ' connected new user');
			
			if(socket.userData == undefined) socket.emit('call-authenticate');
			
			socket.on('authenticate', function(token) {
				if (socket.userData != undefined) return false;
				if (token) {
				    // verifies secret and checks exp
				    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
				      	if (err) {
				        	console.log('Failed to authenticate token');
				      	} else {
				        	socket.userData = decoded.user;
				        	console.log(socket.userData.teamName+ ' connected');
				        	
		        	        socket.broadcast.emit('set-online', socket.userData);
		        	        usersModel.findOne({'_id': decoded.user._id},'subscribedRooms',function(err, userModel){
		        	        	if(userModel.subscribedRooms.length > 0){
		        	        		socket.subscribedRooms = [];
			        	        	userModel.subscribedRooms.forEach(function(roomId){
			        	        		if(socket.rooms[roomId] == undefined){
											socket.join(roomId)
											socket.subscribedRooms.push(roomId)
											console.log(socket.userData.teamName+ ' joined to '+ roomId)
											// socket.emit('subscribed-rooms',roomId);
										}
			        	        	})
		        	        	}
		        	        });
				      	}
			    	});
			  	} else {
				    console.log('No token provided')
				}	 
			});
			socket.on('log-out', function(token) {
				if (token) {
				    // verifies secret and checks exp
				    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
				      	if (err) {
				        	console.log('Not logged user');
				      	} else {
				        	socket.disconnect();
				      	}
			    	});
			  	} else {
				    console.log('No token provided to log out')
				}	 
			});
			
			
			socket.on('leave-room', function(roomId) {
				if(socket.subscribedRooms && socket.subscribedRooms.indexOf(roomId) == -1) socket.leave(roomId)
				console.log(socket.userData.teamName+' left from '+ roomId)
				socket.broadcast.emit('left-room',{'roomId': roomId, 'userData': socket.userData })
				roomsModel.update({_id: roomId},{$pull: {connectedUsers: socket.userData._id}},{upsert:true},function(err){
			        if(err){
			                console.log(err);
			        }else{
			        	socket.currentRoom = undefined;
		                console.log("Successfully removed");
			        }
				});
			});
			
			socket.on('disconnect', function () {
		      setTimeout(function(){
				
				if(socket.userData){
					var onlineUsers = Object.keys(io.sockets.sockets).filter(function (key) { 
				        return io.sockets.sockets[key].userData !== undefined && io.sockets.sockets[key].userData._id == socket.userData._id;
				    }).map(function(key) {
				        return io.sockets.sockets[key].userData;
				    });
					console.log('--- '+socket.id+ ' disconnected')
					// var onlineUsers = Object.keys(io.sockets.sockets).map(function (key) { return io.sockets.sockets[key].userData });
					if((onlineUsers.length > 0 )) return false;
					
					if(socket.currentRoom != undefined){
						roomsModel.update({_id: socket.currentRoom},{$pull: {connectedUsers: socket.userData._id}},{upsert:true},function(err){
					        if(err){
					                console.log(err);
					        }else{
					        	socket.broadcast.emit('left-room',{'roomId': socket.currentRoom, 'userData': socket.userData })
					        	socket.currentRoom = undefined;
				                console.log("Successfully removed");
					        }
						});
					}
					
					
					console.log(socket.userData.teamName+ ' disconnected'); 
					socket.broadcast.emit('set-offline', {userData: socket.userData,subscribedRooms: socket.subscribedRooms });
		    	}
				else	console.log(socket.id+ ' non-authenticated user disconnected');
		      },3000)
			});
		});
	//
	//route middleware to verify a token
		module.exports.checkUser = function(req, res, next) {
		  // check header or url parameters or post parameters for token
		  var token = req.headers['x-access-token'];
		
		  // decode token
		  if (token) {
		
		    // verifies secret and checks exp
		    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
		      if (err) {
		        return res.status(401).send({ success: false, message: 'Failed to authenticate token.' });    
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.user = decoded.user;    
		        req.isAuthenticated = decoded.isAuthenticated;    
		        next();
		      }
		    });
		
		  } else {
		
		    // if there is no token
		    // return an error
		    return res.status(401).send({ 
		        success: false, 
		        message: 'No token provided.' 
		    });
		  }
		};
		module.exports.isGuest = function(req, res, next) {
		  // check header or url parameters or post parameters for token
		  var token = req.body.token || req.query.token || req.headers['x-access-token'];
		
		  // decode token
		  if (token) {
		
		    // verifies secret and checks exp
		    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
		      if (err) {
		        req.isAuthenticated = false;    
		        next();  
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.user = decoded.user;    
		        req.isAuthenticated = decoded.isAuthenticated;    
		        next();
		      }
		    });
		
		  } else {
		    req.isAuthenticated = false;    
			next();  
		  }
		};
	//
	//routes requieres token
		apiRoutes.use('/auth', require('./routes/auth'));
		apiRoutes.use('/rooms', require('./routes/rooms'));
	//
//


// load our public/index.html file
	// app.get('*', function(req, res) {
	//     res.sendFile(__dirname+'/public/index.html'); 
	// });

/********************************/
server.listen(8080, function(){ 
    console.log('listening on *:8080');
});