var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var newSchema = new Schema({
	// _id         : {type: Schema.ObjectId},
    title		: {type: String, trim: true},
    creator_id 	: {type: Schema.Types.ObjectId,ref:'users'},
    create_date : {type: Date, default: Date.now},
    password    : {type: String, trim: true},
    countPlayers: {type: Number},
    connectedUsers   : [{type: Schema.Types.ObjectId,ref:'users'}],
    comments    : [{type: Schema.Types.ObjectId,ref:'comments'}],
    status 		: {type: Number, default: 0},
});

module.exports = mongoose.model('rooms',newSchema);