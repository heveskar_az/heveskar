var mongoose = require('mongoose'), 
    Schema = mongoose.Schema;
    // mongoosastic = require('mongoosastic'),
    // elasticsearch = require('elasticsearch');

var newSchema = new Schema({
	// _id: {type : Schema.ObjectId},
	fb_id: {type: String, trim : true},
	firstname: {type: String, trim : true},
	lastname: {type: String, trim : true},
    email: {type: String, trim : true},
    // phone: {type: String, trim : true},
    // name: {type: String, trim : true,es_boost: 2.0},
    password: {type: String, trim : true},
    // birthday: {type: String, trim : true},
    gender: {type: String},
    // location: { type: [Number], es_type: 'geo_point', es_indexed: true, index: true },  
    // address: {type: String, trim : true},
    // img: {type: String, trim : true},
    // position: {type: Number},
    // level: {type: Number}
    // avatar: {type: String, trim : true},
    // id: {type: Number},
    online: {type: Number},
    lastDate:{type:Date},
    createdDate:{type:Date}
    // subscribedRooms:  [{type: Schema.Types.ObjectId,ref:'games'}],
});
// var esClient = new elasticsearch.Client({host: 'http://127.0.0.1:9200'});
// usersSchema.plugin(mongoosastic,{
//     esClient: esClient
// });

// var Users = mongoose.model('users', usersSchema)
//   , stream = Users.synchronize()
//   , count = 0;
 
// stream.on('data', function(err, doc){
//   count++;
// });
// stream.on('close', function(){
//   console.log('indexed ' + count + ' documents!');
// });
// stream.on('error', function(err){
//   console.log(err);
// });

module.exports = mongoose.model('users',newSchema);

