app.factory('socket', function ($rootScope,localStorageService) {
    var token = localStorageService.get('authorizationData');
    // var socket = null;
    
    // var socket = io("https://heveskar-heveskar.c9users.io:8080/",{query:"token="+token, "secure": true, "forceNew": false}).connect();
    
    var socket = io.connect();
    
    // if(token != null && token != undefined){
    //     token = token.token;
    //     // var socket = io("https://heveskar-heveskar.c9users.io:8080/",{query:"token="+token, "secure": true, "forceNew": false}).connect();
    // }
    return {
        on: function (eventName, callback) {
          socket.on(eventName, function () {  
            var args = arguments;
            $rootScope.$apply(function () {
              callback.apply(socket, args);
            });
          });
        },
        emit: function (eventName, data, callback) {
            socket.emit(eventName, data, function () {
                var args = arguments;
                $rootScope.$apply(function () {
                  if (callback) {
                    callback.apply(socket, args);
                  }
                });
            })
        },
        unSubscribe: function(listener){
          socket.removeAllListeners(listener);
        }
    };
});