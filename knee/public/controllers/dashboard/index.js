app.controller('dashboardIndexController',function($http,$scope,$filter,$uibModal,$rootScope,API,socket,$sce,$state,authService){
	// $scope.getPostText = function(text,gameId){
	// 	return $sce.trustAsHtml(text.replace('_gameId_', gameId));
	// }
	
	// $(function(){
	// 	$('#createGameModal .datepicker').datepicker({
	// 		weekStart:1,
	// 		orientation: "top-left",
	// 		format: 'yyyy-mm-dd',
	// 	});
	// });
	
	$scope.memberCount = {
		0: 'Tutum',
		1: '10 nəfər',
		2: '12 nəfər',
		3: '14 nəfər',
		4: '16 nəfər',
		5: '22 nəfər'
	}
	$scope.payment = {
		0: 'Ödəniş',
		1: 'Yarı-yarı',
		2: 'Yer pulu'
	}
	
	$scope.hours = {
		// 0: 'Saat (-dan)',
		1: '00:00',
		2: '01:00',
		3: '02:00',
		4: '03:00',
		5: '04:00',
		6: '05:00',
		7: '06:00',
		8: '07:00',
		9: '08:00',
		10: '09:00',
		11: '10:00',
		12: '11:00',
		13: '12:00',
		14: '13:00',
		15: '14:00',
		16: '15:00',
		17: '16:00',
		18: '17:00',
		19: '18:00',
		20: '19:00',
		21: '20:00',
		22: '21:00',
		23: '22:00',
		24: '23:00'
	}
	
	
	if($scope.authentication.isAuth){
		$scope.onlineUsers = [];
		
		//get games
			$http.get(API+'games/shortlist').success(function(data){
				$scope.games = data;
			}).error(function(err){
				// // alert(err);
			});
		
		// $state.go('index', {}, { reload: true });
		
		socket.on('left-room',function(data){
			for(i in $scope.games){
				if($scope.games[i]._id == data.roomId){
					if($scope.games[i].connectedUsers != undefined){ 
						$scope.games[i].connectedUsers = $scope.games[i].connectedUsers.filter(function (users) { return users._id != data.userData._id });
					}	
					return;
				}
			}
		})
		socket.on('joined-room',function(data){
			for(i in $scope.games){
				if($scope.games[i]._id == data.roomId){
					if($scope.games[i].connectedUsers == undefined) $scope.games[i].connectedUsers = [];
					$scope.games[i].connectedUsers.push(data.userData);
					return;
				}
			}
		})
		
		$scope.$on('$destroy', function (event) {
	        socket.unSubscribe('left-room');
	        socket.unSubscribe('joined-room');
	    });
		
	}else{
		$state.go('login', {}, { reload: true });
		//get games
		// $http.get(API+'games/shortlist/1').success(function(data){
		// 	$scope.games = data;
		// });
		// $http.get(API+'games/shortlist/2').success(function(data){
		// 	$scope.mixgames = data;
		// });
	}
	
	// socket.on('new-post',function(data){
	// 	var post = data.post;
	// 	post.userId = data.userId;
	// 	if(!$scope.posts){
	// 		$scope.posts = [];
	// 	}
	// 	$scope.posts.unshift(post);
	// })
	
	// alert('index');
	// $http.get(DOMAIN+'users/info').success(function(data){
	// 	if(data.isAuthenticated){
	// 		$scope.isAuthenticated = data.isAuthenticated;
	// 		$rootScope.isAuthenticated = $scope.isAuthenticated;
	// 		//get posts 
	// 		$scope.posts = [{id:1},{id:2}];
	// 	}else{
	// 		$rootScope.isAuthenticated = false;
	// 		$http.get(DOMAIN+'games/shortlist/1').success(function(data){
	// 			$scope.games = data;
	// 		}).error(function(err){
	// 			alert(err);
	// 		});

	// 		$http.get(DOMAIN+'games/shortlist/2').success(function(data){
	// 			$scope.mixgames = data;
	// 		}).error(function(err){
	// 			alert(err);
	// 		});
	// 	}
	// }).error(function(err){
	// 	$scope.isAuthenticated = false;
	// 	$rootScope.isAuthenticated = false;
	// 	$http.get(DOMAIN+'games/shortlist/1').success(function(data){
	// 		$scope.games = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});

	// 	$http.get(DOMAIN+'games/shortlist/2').success(function(data){
	// 		$scope.mixgames = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});
	// 	alert(err);
	// });

	return $scope;
});