var express = require('express');
var	router = express.Router();
var checkUser = require("../app").checkUser;
// var acquaintances = require("../app").acquaintances;
// var posttypes = require("../app").posttypes;
// var prepareEmitMultipleUsers = require("../app").prepareEmitMultipleUsers;
var getUserSocket = require("../app").getUserSocket;
var joinRoom = require("../app").joinRoom;
var roomsModel = require('../models/rooms.js');
var usersModel = require('../models/users.js');
// var gameOffersModel = require('../models/gameOffers.js');
// var teamsUsersModel = require('../models/teamsUsers.js');
// var gameCommentsModel = require('../models/gameComments.js');

// var postsModel = require('../models/posts.js');
// cloudinary
	// var cloudinary = require('cloudinary');
	// cloudinary.config({ 
	//   cloud_name: 'heveskar-az', 
	//   api_key: '696691114969959', 
	//   api_secret: 'vhoJSa5CaIdb-IzqMuy2wazbwb8' 
	// });
//

router.get('/list', checkUser, function (req, res) {
    // do something with req.user
    roomsModel
    .find({'status': 0})
    .populate([{path: 'creator_id', select: '_id fb_id firstname lastname'},{path: 'connectedUsers', select: '_id fb_id firstname lastname'}])
    .sort([['createdDate','descending']])
    .limit(10)
    .exec(function(err, data){
    	if(err){
    		res.status(400).send(err)
    	}else{
    		res.json(data);
    	}
    })
  }
);

// router.get('/connectedUsers/:id', function(req, res) {
// 	if(req.params.id){
// 		gamesModel
// 		.findOne({_id: req.params.id}, '_id connected')
// 		.populate([{path:'connected',select: '_id username img'}])
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xəta N4');
// 			}else{
// 				if(data){
// 					res.json(data);
// 				}else{
// 					res.send(false);
// 				}
// 			}
// 		});
// 	}else{
// 		res.status(400).send('Oyun tapılmadı');
// 	}
// });

// router.post('/deleteComment', checkUser,function(req, res) {
// 	var removerIsGameCreator = false;
// 	if(!req.body.commentId || !req.body.gameId){
// 		res.status(400).send('Məlumatlarda yanlışlıq var.');
// 	}else{
// 		gamesModel
// 		.findOne({_id: req.body.gameId}, '_id created.by comments')
// 		.exec(function(err,data){
// 			console.log(data);
// 			console.log(req.user._id);
// 			console.log('------------');
// 			console.log(req.body.commentId);
// 			if(data.created.by == req.user._id){
// 				removerIsGameCreator = true;
// 			}

// 			commentsModel
// 			.findOne({_id: req.body.commentId},'senderId')
// 			.exec(function(err,data2){
// 				if(err){
//                     res.status(400).send('Xəta N4');
//                 }else{
//                 	if(data2){
//                 		if(removerIsGameCreator != true){
//                 			if(data2.senderId == req.user._id){
//                 				data2.remove(function(err){
//                     				gamesModel.findOneAndUpdate({ _id: req.body.gameId }, 
// 	                    				{$pull: {comments: req.body.commentId}}, function(err, data){
// 								        if(err) {
// 								          return res.status(500).json({'error' : 'error in deleting address'});
// 								        }

// 								        res.json(req.body.commentId);

// 							      	});
//                     			});
//                 			}
//                 		}else{

//                 			data2.remove(function(err){
//                 				gamesModel.findOneAndUpdate({ _id: req.body.gameId }, 
//                     				{$pull: {comments: req.body.commentId}}, function(err, data){
// 							        if(err) {
// 							          return res.status(500).json({'error' : 'error in deleting address'});
// 							        }

// 							        res.json(req.body.commentId);

// 						      	});
//                 			});
//                 		}
// 					}
//                 }
// 			});
// 		});
// 	}
// });
// router.post('/newComment', checkUser, function(req, res) {
// 	if(!req.body.comment || !req.body.gameId){
// 		res.status(400).send('Məlumatlarda yanlışlıq var.');
// 	}else{
// 		gamesModel
// 		.findOne({_id: req.body.gameId,status: 0}, '_id connected comments')
// 		.exec(function(err,data){
// 			if(!data.opponent){
// 				// console.log(data);
// 				// // return false;
// 				// var newComment = {
// 				// 	senderId 	: req.user._id,
// 				// 	comment 	: req.body.comment,
// 				// 	date 		: new Date(),
// 				// 	replies		: []
// 				// }
// 				// data.comments.push(newComment);
// 				// console.log(data);
// 				// data.save(function(err){
// 				// 	if(err){
//     //                     res.status(400).send('Xəta N4');
//     //                 }else{
//     //                 	res.send({callback: 'success'});
//     //                 }
// 				// });
// 			}else{
// 				if(data.connected.indexOf(req.user._id) != -1 ){
					
// 				}else{
// 					res.status(400).send('- Sizin bu oyuna rəy yazmaq üçün icazəniz yoxdur!');
// 					return false;
// 				}
// 			}
// 			var newComment = new commentsModel({
//   				senderId 	: req.user._id,
// 				comment 	: req.body.comment,
// 				date 		: new Date(),
// 				replies		: []
//             });

//             newComment.save(function(err,callback){
//                 if(err){
//                     res.status(400).send('Xəta N4');
//                 }else{
//                 	if(req.body.commentId){
// 						commentsModel
// 						.findOne({_id: req.body.commentId}, '_id replies')
// 						.exec(function(err,dataComment){
// 							if(dataComment){
// 								dataComment.replies.push(callback._id);
// 								dataComment.lastUpdate = new Date();
// 								dataComment.save(function(err,dataCommentData){
// 									commentsModel.populate(callback, {path: 'senderId', model: 'users'},function(err,finalData){
// 										if(finalData){
// 											res.send(finalData);
// 										}
// 		                        	});
// 								});
// 							}
// 						});
//             		}else{
//             			data.comments.push(callback._id);
// 						data.save(function(err){
// 							if(err){
// 	                            res.status(400).send('Xəta N4');
// 	                        }else{
// 	                        	commentsModel.populate(callback, {path: 'senderId', model: 'users'},function(err,finalData){
// 									if(finalData){
// 										res.send(callback);
// 									}
// 	                        	});
// 	                        }
// 						});
//             		}
//                 }
//             });
// 		});
// 	}
// });



// router.post('/acceptUserRequest', checkUser,function(req, res) {
// 	if(req.body.offerId && req.body.gameId){
// 		gamesModel
// 		.findOne({_id: req.body.gameId,'created.by': req.user._id, opponent: false, status: 0 })
// 		.exec(function(err,gameData){
// 			if(gameData){
// 				gameOffersModel
// 				.findOne({_id: req.body.offerId, gameId: req.body.gameId, 'offer.confirmed': false })
// 				.exec(function(err,data){
// 					if(data){
// 						var offer = {
// 							date: data.offer.date,
// 							confirmed: true,
// 							confirmedDate: new Date()
// 						}
// 						data.offer = offer;
// 						data.save(function(err){
// 							if(err){ 
// 								res.status(400).send('- Xəta N1!');
// 							}else{
// 								gameData.opponent = true;
// 								gameData.oppTeamId = data.senderTeamId;
// 								gameData.save(function(err){
// 									if(err){
// 										res.status(400).send('- Xəta N2!');
// 									}else{
// 										res.send( {callback: 'success'} );
// 									}
// 								});
// 							}
// 						});
// 					}else{
// 						res.status(400).send('- Təklif tapılmadı.');
// 					}
// 				});
// 			}else{
// 				res.status(400).send('- Oyun tapılmadı və ya artıq rəqib seçilib.');
// 			}
// 		});
// 	}else{
// 		res.status(400).send('- Məlumatlarda yanlışlıq var.');
// 	}
// 	// acquaintancesModel
// 	// .findOne({
// 	// 	'receiverId': req.user._id, 'offer.confirmed': false
// 	// }, '_id')
// 	// .exec(function (err, data) {
// 	// 	if(err){
// 	// 		res.status(400).send('Xəta N3');
// 	// 	}else{
// 	// 		if(data){
// 	// 			data.offer.confirmed = true;
// 	// 			data.offer.confirmedDate = new Date();
// 	// 			data.save();
// 	// 			res.send('- Təklif sizin tərəfinizdən uğurla qəbul edildi.')
// 	// 		}else{
// 	// 			res.status(400).send('- Təklif təyin olunmadı.');
// 	// 		}
// 	// 	}
// 	// });
// });


// router.get('/showOffers/:id', checkUser, function(req, res) {
// 	if(!req.params.id){
// 		res.status(400).send('Məlumatlarda yanlışlıq var. Komandanızı seçin.');
// 	}else{

// 		gamesModel
// 		.find({_id: req.params.id,'created.by': req.user._id }, '_id')
// 		.count(function(err,count){
// 			if(count<0){
// 				res.status(400).send({});
// 			}else{
// 				gameOffersModel
// 				.find({gameId: req.params.id,'offer.confirmed': false })
// 				.populate([{path:'senderTeamId',select: '_id name img'},{path:'senderId',select: '_id username img'}])
// 				.sort([['offer.date', 'descending']])
// 				.exec(function(err,data){
// 					if(data){
// 						res.send(data);
// 					}
// 				});
// 			}
// 		});
// 	}
// });


// router.post('/newOffer', checkUser, function(req, res) {
// 	if(!req.body.offer.teamId || !req.body.offer.gameId){
// 		res.status(400).send('Məlumatlarda yanlışlıq var. Komandanızı seçin.');
// 	}else{
// 		teamsUsersModel
// 		.find({teamId: req.body.offer.teamId, userId: req.user._id, 'offer.confirmed': true}, '_id')
// 		.count(function (err, count1){
// 		  	// if (err) return done(null, null);//handleError(err);
// 		  	if(count1<0){
// 				res.status(400).send('- Belə komanda mövcud deyil və ya siz heyətində deyilsiniz.');
// 			}else{
// 				if(err){
// 					res.status(400).send('Xəta N3!');
// 				}else{
// 					gamesModel
// 					.find({_id: req.body.offer.gameId,status: 0, opponent: false}, '_id')
// 					.count(function(err,count2){
// 						if(count2<0){
// 							res.status(400).send('- Oyun tapılmadı və ya artıq rəqib tapılıb!');
// 						}else{
// 							gameOffersModel
// 							.find({gameId: req.body.offer.gameId,senderTeamId: req.body.offer.teamId}, '_id')
// 							.count(function(err,countOffers){

// 								if(countOffers>0){
// 									res.status(400).send('- Seçdiyiniz komanda tərəfindən artıq təklif göndərilib!');
// 								}else{

// 									var newOffer = new gameOffersModel({
// 						  				gameId  	: 		req.body.offer.gameId,
// 						  				senderId    : 		req.user._id, 
// 						  				senderTeamId: 		req.body.offer.teamId,
// 						  				text		: 		req.body.offer.text
// 			                        });
// 				                    newOffer.save(function(err,callback){
// 				                        if(err){
// 				                            res.status(400).send('Xəta N4');
// 				                        }else{
// 				                        	res.send({callback: 'success'});
// 				                        }
// 				                    });
// 								}
// 							});
// 						}
// 					});
// 				}
// 			}	
// 		});
// 	}
// });



// router.post('/new', checkUser, function(req, res) {
// 	console.log(req.body.game);
// 	if(req.body.game.memberCount == 0 || req.body.game.payment == 0 ||
// 			(req.body.game.payment == 2 && (req.body.game.winner == '' || req.body.game.loser == '')) || 
// 			req.body.game.date == '' || req.body.game.fromHours == 0 || req.body.game.toHours == 0 ||
// 			req.body.game.stadium == ''
// 			// || [5, 6, 7, 8].indexOf(req.body.game.countMembers) == -1
// 	){
// 		res.status(400).send('Məlumatlarda yanlışlıq var. Bütün sahələri dəqiqliklə doldurun.');
// 	}else{
// 		// if(req.body.game.team == 1 && req.body.game.teamName){
// 		// 	teamsUsersModel
// 		// 	.find({teamId: req.body.game.teamName, userId: req.user._id, 'offer.confirmed': true}, '_id')
// 		// 	.count(function (err, count){
// 		// 	  	// if (err) return done(null, null);//handleError(err);
// 		// 	  	if(count<0){
// 		// 			res.status(400).send('- Belə komanda mövcud deyil və ya siz heyətində deyilsiniz.');
// 		// 		}else{
// 		// 			if(err){
// 		// 				res.status(400).send('Xəta N3!');
// 		// 			}else{
// 		// 				var created = {
// 		// 					by  : req.user._id,
// 		// 					date: new Date()
// 		// 				}
// 		// 				var connected = [];

// 		// 				if(req.body.game.team == 1){
// 		// 					var i = 0; 
// 		// 					teamsUsersModel
// 		// 					.find({teamId: req.body.game.teamName, 'offer.confirmed': true}, '_id userId teamId')
// 		// 					.populate({path: 'teamId',select: '_id name'})
// 		// 					.exec(function (err, data){
// 		// 						if(err) res.status(400).send('Xəta N4!');
// 		// 						if(data.length){
// 		// 							var teamName = data[0].teamId.name;
// 		// 							console.log('==============')
// 		// 							data.forEach(function(item){
// 		// 							i++;
// 		// 							connected.push(item.userId);
// 		// 							if(data.length == i){
// 		// 								var newGame = new gamesModel({
// 		// 					  				team: req.body.game.team,
// 		// 					  				teamId: req.body.game.teamName,
// 		// 					  				division: req.body.game.division, 
// 		// 					  				countPlayers: req.body.game.countMembers,
// 		// 					  				plan: req.body.game.plan,
// 		// 					  				stadium: req.body.game.stadium,
// 		// 					  				date: req.body.game.date,
// 		// 					  				created: created,
// 		// 					  				status: 0,
// 		// 					  				connected : connected
// 		// 		                        });
// 		// 			                    newGame.save(function(err,callback){
// 		// 			                        if(err){
// 		// 			                        	console.log(err);
// 		// 			                            res.status(400).send('Xəta N4');
// 		// 			                        }else{
// 		// 			                        	console.log(callback);
// 		// 			                        	/************* POST NEW GAME *************************************/
// 		// 			                        	req.acquaintances.push(req.user._id);
// 		// 			                        	var newItem = new postsModel({
// 		// 									  		type: "56d747f689ee3d057686297b",
// 		// 											userId: req.user._id,
// 		// 											to: req.acquaintances,
// 		// 											teamId: req.body.game.teamName,
// 		// 											gameId: callback._id,
// 		// 											seenBy: [],
// 		// 									  	});			
// 		// 									  	newItem.save(function(err,saved){
// 		// 									        if(err){
// 		// 									        	console.log(err);
// 		// 									            res.status(400).send('Xəta N4');
// 		// 									        }else{
// 		// 									        	if(saved){
// 		// 									        		var data = {
// 		// 									        			acquaintances: req.acquaintances,
// 		// 									        			data: {
// 		// 									        				userId: {
// 		// 									            				_id: req.user._id,
// 		// 									            				username: req.user.username,
// 		// 									            				name: req.user.name,
// 		// 									            				img: req.user.img
// 		// 									            			},
// 		// 									            			post: {
// 		// 									            				text: saved.text,
// 		// 									            				teamId: {
// 		// 									            					'_id': saved.teamId,
// 		// 									            					'name': teamName
// 		// 									            				},
// 		// 																gameId: saved.gameId,
// 		// 									            				type: req.posttypes[saved.type],
// 		// 									            				date: saved.date
// 		// 									            			}
// 		// 									        			}
// 		// 									        		}
// 		// 									        		prepareEmitMultipleUsers(data,'new-post');
// 		// 									        		res.json({ post: data.post, sender: data.sender });
// 		// 									        	}else{
// 		// 									        		res.status(400).send('Xəta N5');
// 		// 									        	}
// 		// 									        }
// 		// 									    });
					                        	
// 		// 			                        	/************* POST NEW GAME *************************************/
// 		// 			                        }
// 		// 			                    });
// 		// 							}
// 		// 						});
// 		// 						}else{
// 		// 							res.status(400).send(false);
// 		// 						}
// 		// 					});
// 		// 				}
// 		// 			}
// 		// 		}	
// 		// 	});
// 		// }else{
// 			var plan = {
// 				type: req.body.game.payment,
// 				winner: req.body.game.winner,
// 				loser: req.body.game.loser
// 			}
// 			var newGame = new gamesModel({
//   				userId: req.user._id,
//   				countPlayers: req.body.game.memberCount, 
//   				plan: plan,
//   				stadium: req.body.game.stadium,
//   				date: new Date(req.body.game.date),
//   				fromHours: req.body.game.fromHours,
//   				toHours: req.body.game.toHours,
//   				status: 0
//             });
//             newGame.save(function(err,callback){
//                 if(err){
//                     res.status(400).send('Xəta N4');
//                 }else{
//                 	res.send({success: 1,id: callback._id,message: 'Yeni oyunu uğurla yaratdınız, yaratdığınız oyuna yönlədirilirsiniz...'});
//                 }
//             });
// 		// }
// 	}
// });

// router.get('/shortlist', function(req, res) {
// 	if(req.params.team){
// 		gamesModel
// 		.find({
// 			'status': 0,
// 			'team' : req.params.team
// 		})
// 		.populate([{path:'teamId',select: '_id name img'},{path:'created.by',select: '_id username img'}])
// 		.sort([['created.date', 'descending']])
// 		.limit(5)
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xəta N4');
// 			}else{
// 				if(data.length){
// 					res.json(data);
// 				}else{
// 					res.send(false);
// 				}
// 			}
// 		});
// 	}else{
// 		gamesModel
// 		.find({
// 			'status': 0
// 		})
// 		.populate([{path:'userId',select: '_id teamName'},{path: 'connectedUsers', select: '_id teamName'}])
// 		.sort([['create_date', 'descending']])
// 		.limit(10)
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xəta N4');
// 			}else{
				
// 				if(data.length){
// 					res.json(data);
// 				}else{
// 					res.send(false);
// 				}
// 			}
// 		});
// 	}
// });
// router.get('/more/:id', checkUser, function(req, res) {
// 	if(req.params.id){
// 		gamesModel
// 		.findOne({
// 			_id: req.params.id
// 		})
// 		.populate([
// 				{path:'userId',select: '_id teamName'},
// 				{path:'connectedUsers',select: '_id teamName'},
// 				// {path:'oppTeamId',select: '_id name img statistics'},
// 				// {path:'created.by',select: '_id username img'},
// 				// {
// 				// 	path:'comments', 
// 				// 	options: { 
// 				// 		sort: { 'lastUpdate': -1 }, 
// 				// 		//limit: 2 
// 				// 	}
// 				// },
// 		])
// 		.exec(function (err, data) {
// 			if(err){
// 				res.status(400).send('Xəta N4');
// 			}else{
// 				if(data){
					
// 					joinRoom(getUserSocket(req.io.sockets.sockets,req.user._id)[0],data._id);
					
// 					var connectedUsers = Object.keys(data.connectedUsers).filter(function (key) { 
// 				        return data.connectedUsers[key]._id !== undefined && data.connectedUsers[key]._id == req.user._id;
// 				    }).map(function(key) {
// 				        return data.connectedUsers[key];
// 				    });
// 				    console.log(connectedUsers);
					
// 					if(connectedUsers.length <= 0){
// 						data.connectedUsers.push({"_id": req.user._id, "teamName": req.user.teamName});	
// 					}
// 					// usersModel.findByIdAndUpdate(
// 					//     req.user._id,
// 					//     {$push: {"subscribedRooms": data._id }},
// 					//     {safe: true, upsert: true}
// 					// );
// 					res.json({success: 1, game: data});
// 					// commentsModel.populate(
// 					// 	data, 
// 					// 	{ path: 'comments.senderId',model: 'users',select: '_id username img comment date' }, 
// 					// 	function (err,callback){
// 					// 		commentsModel.populate(
// 					// 			callback, 
// 					// 			{ 
// 					// 				path: 'comments.replies',
// 					// 				model: 'comments',
// 					// 				select: '_id comment date senderId',
// 					// 				options: { 
// 					// 					//limit: 4 , 
// 					// 					sort: { 'date': -1 } 
// 					// 				}
// 					// 			}, 
// 					// 			function (err,callback2){
// 					// 				commentsModel.populate(
// 					// 					callback2, 
// 					// 					{ path: 'comments.replies.senderId',model: 'users',select: '_id username img' }, 
// 					// 					function (err,callback3){
// 					// 						res.json(callback3);
// 					// 					}
// 					// 				);
// 					// 			}
// 					// 		);
// 					// 	}
// 					// );
// 				}else{
// 					res.send(false);
// 				}
// 			}
// 		});
// 	}else{
// 		res.status(400).send('- Oyun tapılmadı');
// 	}
// });



module.exports = router;