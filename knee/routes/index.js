var express = require('express');
var	router = express.Router();

var checkUser = require("../app").checkUser;
// router.get('/', function(req, res) {
// 	res.render('index.html',{
// 		isAuthenticated: req.isAuthenticated(),
// 		user: req.user
// 	});
// });

router.get('/online-users', checkUser, function(req, res) {
    var onlineUsers = Object.keys(req.io.sockets.sockets).filter(function (key) { 
        return req.io.sockets.sockets[key].userData && req.io.sockets.sockets[key].userData._id !== req.user._id;
    }).map(function(key) {
        return req.io.sockets.sockets[key].userData;
    });
    res.json({success: true, users: onlineUsers});
});
router.post('/logout', checkUser, function(req, res) {
    
    console.log(req.io.socket);
    
    res.send('ok');
    
    // req.io.sockets.sockets
    // res.json({success: true, users: onlineUsers});
});

module.exports = router;
