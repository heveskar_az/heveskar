var express = require('express')
	, router = express.Router()
	, passport = require('passport')
	// , FacebookStrategy  = require("passport-facebook").Strategy
	, FacebookTokenStrategy = require('passport-facebook-token')
	, config = require('../config.js')
	, jwt    = require('jsonwebtoken');

var checkUser = require("../app").checkUser;

//Require Models
var usersModel = require('../models/users.js');

//passport setup
router.use(passport.initialize())

passport.serializeUser(function(user, done) {
	console.log(user)
  done(null, user);
});
passport.deserializeUser(function(obj, done) {
	console.log(obj)
  done(null, obj);
});

// Use the FacebookStrategy within Passport.
passport.use(new FacebookTokenStrategy({
    clientID: config.facebook_api_key,
    clientSecret: config.facebook_api_secret
  }, 
  function(accessToken, refreshToken, profile, done) {
    usersModel.findOne({fb_id: profile.id}, '_id fb_id firstname lastname email gender online lastDate createdDate' ,function (error, user) {
    	if(user){
    		console.log('===')
    		console.log(user)
    		return done(null, user);	
    	}else{
    		return done(null, profile);	
    	}
    });
  }
));


//login with passport facebook token
router.post('/facebook',
  passport.authenticate('facebook-token'),
  function (req, res) {
  	console.log(req.user)
  	if(req.user){
  		if(req.user._id){
  			var token = jwt.sign({'user':req.user,'isAuthenticated': true}, config.secret, {
	          //expiresIn: 3600*24 // expires in 24 hours
	        });
	        console.log(1)
	        res.json({
	          success: true,
	          token: token,
	          user: req.user
	        });
  		}else if(req.user.id){
	  		var user  = new usersModel({
	  			fb_id: req.user.id,
	  			firstname: req.user.name.givenName,
	  			lastname: req.user.name.familyName,
	  			email: req.user._json.email,
	  			gender: req.user.gender,
	  			online: 0,
	  			createdDate: new Date(),
	  			lastDate: new Date(),
	  			password: ""
	  		});
	  		user.save(function(err){
	  			if(err){
	  				console.log(3)
	  				res.send(401).json(err);
	  			}else{
	  				var token = jwt.sign({'user': user,'isAuthenticated': true}, config.secret, {
			          //expiresIn: 3600*24 // expires in 24 hours
			        });
			        console.log(2)
			        res.json({
			          success: true,
			          token: token,
			          user: user
			        });
	  			}
	  		})
	  	}else{
	  		console.log(4)
  			res.send(401)
  		}
  	}else{
  		console.log(5)
  		res.send(401)
  	}
  }
);


//Login with Facebook
// router.get('/facebook', passport.authenticate('facebook-token'));

//Facebook callback
// router.get('/fb-callback',passport.authenticate('facebook-token'), function(req, res) {
// 	res.json(req.user);
// });

//Login POST Method
router.post('/login',function(req, res){
    
    
    
	usersModel.findOne({ 'teamName': req.body.teamName},'teamName _id password',function (err, user) {
	  	if (err) throw err;
	  	if(!user){
	  		if(req.body.teamName =='' || req.body.password ==''){
	  			return res.status(401).send({ success: false, message: 'Zəhmət olmasa məlumatlarınızı daxil edin' });
	  		}
	  		
	  		var newUser = new usersModel({
  				teamName: req.body.teamName,
  				password: req.body.password,
            });
	  		newUser.save(function(err,callback){
                if(err){
                    res.status(400).send({message: 'Xəta N4'});
                }else{
                	// res.send({callback: 'success',id: callback._id});
                	
                	callback.password = '';
			        var token = jwt.sign({'user':callback,'isAuthenticated': true}, app.get('superSecret'), {
			          expiresIn: 3600*24 // expires in 24 hours
			        });
			
			        // return the information including token as JSON
			        res.json({
			          success: true,
			          message: 'Uğurla daxil oldunuz! Yönlədirilir...',
			          token: token,
			          data:{
			          	'teamName' : callback.teamName,
			          	'_id' : callback._id
			          }
			        });
                	
                	
                }
            });
	  	}else{
  			if(user.password != req.body.password){
				return res.status(401).send({ success: false, message: 'Daxil etdiyiniz şifrə bu komanda üçün yanlışdır' });
  			}else{
  				// if user is found and password is right
		        // create a token
		        user.password = '';
		        var token = jwt.sign({'user':user,'isAuthenticated': true}, app.get('superSecret'), {
		          expiresIn: 3600*24 // expires in 24 hours
		        });
		
		        // return the information including token as JSON
		        res.json({
		          success: true,
		          message: 'Uğurla daxil oldunuz! Yönlədirilir...',
		          token: token,
		          data:{
		          	'teamName' : user.teamName,
		          	'_id' : user._id
		          }
		        });
  			}
	  	}
	})
});
		
		
router.get('/logout', function(req, res) {
	// req.logout();
	// res.redirect('/');
	res.send('ok');
});

module.exports = router;