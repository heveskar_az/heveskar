var app = angular.module('PF',['ui.router','ui.bootstrap','LocalStorageModule']);

app.config(function($stateProvider, $urlRouterProvider,$httpProvider){
	$httpProvider.interceptors.push('authInterceptorService');
	$urlRouterProvider.otherwise('/');
	$stateProvider
		.state('index',{
			url: "/",
			templateUrl: "templates/index.html",
			controller: 'IndexController',
			controllerAs: 'IC'
		})
		.state('about',{
			url: "/about",
			templateUrl: "templates/about.html",
			// controller: 'MainController'
		})
		.state('contact',{
			url: "/contact",
			templateUrl: "templates/contact.html"
		})
		.state('stadiums',{
			url: "/stadiums",
			templateUrl: "templates/stadiums.html",
			controller: 'StadiumsController',
			controllerAs: 'SC'
		})
		.state('stadiumSchedule',{
			url: "/stadiums/schedule/:id",
			templateUrl: "templates/stadiumSchedule.html",
			controller: 'StadiumScheduleController',
			controllerAs: 'SSC',
			resolve: {
				id: ['$stateParams', function($stateParams){
		          	return $stateParams.id;
		      	}]
			}
		})
		.state('profile',{
			url: "/users/profile/:username",
			templateUrl: "templates/userProfile.html",
			controller: 'userProfileController',
			controllerAs: 'uPC',
			resolve: {
				username: ['$stateParams', function($stateParams){
		          	return $stateParams.username;
		      	}]
			}
		})
		.state('teams',{
			url: "/teams/",
			templateUrl: "templates/teams.html",
			controller: 'teamsController',
			controllerAs: 'tC'
		})
		.state('teamInfo',{
			url: "/teams/info/:name",
			templateUrl: "templates/teamInfo.html",
			controller: 'teamInfoController',
			controllerAs: 'tIC',
			resolve: {
				name: ['$stateParams', function($stateParams){
		          	return $stateParams.name;
		      	}]
			}
		})
		.state('myteams',{
			url: "/teams/myteams",
			templateUrl: "templates/userTeams.html",
			controller: 'myTeamsController',
			controllerAs: 'mTC'
		})
		.state('myrequests',{
			url: "/teklif/:page",
			templateUrl: "templates/userRequests.html",
			controller: 'myRequestsController',
			controllerAs: 'mRC'
		})
		.state('games',{
			url: "/games/:id",
			templateUrl: "templates/gamesIndex.html",
			controller: 'gamesController',
			// params:{ id: 1 }
		})
		.state('gamesMore',{
			url: "/games/more/:id",
			templateUrl: "templates/gamesMore.html",
			controller: 'gameController',
			resolve: {
				id: ['$stateParams', function($stateParams){
		          	return $stateParams.id;
		      	}]
			}
		})
		.state('products',{
			url: "/products",
			templateUrl: "templates/products.html",
			// controller: 'productsController'
		})
})
.run(function (authService) {
    authService.fillAuthData();
})
.run(function (acquaintancesService) {
    acquaintancesService.getAcquaintances();
});