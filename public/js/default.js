/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function(){
   $('.moreWithEye,.mytooltips').tooltip();

   $('body').on('mouseenter','.userimg-thumb-box',function(){
    $(this).find('.userimg-thumb-box-overlay').css('visibility','visible');
  });
  $('body').on('mouseleave','.userimg-thumb-box',function(){
    $(this).find('.userimg-thumb-box-overlay').css('visibility','hidden');
  });
  $('body').on('focus','#search-box input',function(){
      if($(this).val() == 'Axtar...'){
          $(this).val('');
      }
  });
  $('body').on('blur','#search-box input',function(){
      if($(this).val() == ''){
          $(this).val('Axtar...');
      }
  });
   // $( "body" ).on( "click", "#registration", function(e) {
   //      e.preventDefault();
   //      $.ajax({
   //          url: "users/registration",
   //          cache: false
   //      })
   //          .done(function( html ) {
   //            $( ".modal-content" ).html( html );
   //            $( ".modal-dialog" ).css('width','500px');
   //            $( ".modal-content" ).css('margin','0px auto');
   //            $( ".modal-content" ).animate({width:'500px'},300);
   //          });
   // });
   
   // $("body").on("submit", ".ajaxForm", function(e) {
   //      e.preventDefault();
   //      var data=$(this).serializeArray();
   //      $.ajax({
   //          type: "POST",
   //          url: "/users/registration",
   //          cache: false,
   //          data:data
   //      })
   //          .done(function( html ) {
   //              $( ".modal-content" ).html( html );
   //          });
   // });
   
   //game/new
   $( "body" ).on( "click", ".cedvel", function(e) {
        e.preventDefault();
        var stadion=$(this).attr('stadion');
        $.ajax({
            url: "stadion/table/"+stadion,
            cache: false
        })
            .done(function( html ) {
              $( ".modal-content" ).html( html );
//              $( ".modal-dialog" ).css('width','500px');
//              $( ".modal-content" ).css('margin','0px auto');
//              $( ".modal-content" ).animate({width:'500px'},300);
            });
   });
   $( "body" ).on( "click", ".take.btn-primary", function(e) {
        e.preventDefault();
        var dt=$(this).attr('datetime');
        $('span.glyphicon').removeClass('glyphicon-time');
        $('span.glyphicon').addClass('glyphicon-ok');
        $('.cedvel').removeClass('btn-primary');
        $('.cedvel').addClass('btn-success');
        $('#datetime').val(dt);
   });
   $("body").on("change", "input[name='komanda']", function(e) {
        if($(this).val()==1){
            $('.komanda2').css('display','none');
            $('.komanda').fadeIn('slow');
        }else{
            $('.komanda').css('display','none');
            $('.komanda2').fadeIn('slow');
        }
   });
});
