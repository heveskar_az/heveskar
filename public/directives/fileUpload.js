app.directive("fileread", function ($http,API,authService) {
    return {
      scope: {
        fileread: "="
      },
      link: function (scope, element, attributes) {
        element.bind("change", function (changeEvent) {
          var readers = [] ,
              files = changeEvent.target.files ,
              datas = [] ;
          scope.uploading = true;
          for ( var i = 0 ; i < files.length ; i++ ) {
            readers[ i ] = new FileReader();
            readers[ i ].onload = function (loadEvent) {
              datas.push( loadEvent.target.result );
              if ( datas.length === files.length ){
                // scope.$apply(function () {
                  scope.fileread = datas;
                  		$http.post(API+'users/uploadImg',{data:scope.fileread[0]}).success(function(data){
                  			scope.uploadedImg = data;
                  			authService.setNewProfileImg(data).then(function(img){
                  			  location.reload();
                  			});
                  		}).error(function(err){
                  			alert(err);
                  			scope.fileread = false;
                  		});
                // });
              }
            }
            readers[ i ].readAsDataURL( files[i] );
          }
        });

      }
    }
  });
