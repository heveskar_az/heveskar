app.directive('maybeAcquaintance',function(){
	return {
		restrict: 'A',
		templateUrl: 'partials/maybe-acquaintance.html',
		controller: 'maybeAcqCtrl'
	}
})
.controller('maybeAcqCtrl',function($http,$log,$uibModal,$rootScope,API,$scope, authService,socket,acquaintancesService){
    $http.get(API+'users/maybe-acquaintances').success(function(data){
		console.log(data.hits.hits);
		$scope.maybeAcquaintances = data.hits.hits;
	}).error(function(err){
		console.log(err);
	});
	
	$scope.addAcquaintance = function(userId){
		acquaintancesService.setNewUserToList(userId)
		.then(function(response){
			if(response.msg == 'requestSent'){
				$scope.acquaintances[userId] = response.data;	
			}
			if(response.msg == 'deleted'){
				delete $scope.acquaintances[userId];
			}
			if(response.msg == 'added'){
				$scope.acquaintances[userId] = response.data;
			}
		},function(err){
				
		});
	}
	
});