app.directive('searchDirective',function(){
	return {
		restrict: 'A',
		templateUrl: 'partials/search.html',
		controller: 'searchCtrl',
		controllerAs: 'searchCtrl'
	};
})


.controller('searchCtrl',function($http,$log,$uibModal,$rootScope,$scope, authService,socket,API){
	$scope.search = function(){
		$http.get(API+'users/search/'+$scope.keyword).success(function(data){
			if(data.hits){
				$scope.searchResults = data.hits.hits;	
			}else{
				$scope.searchResults = [];
			}
		}).error(function(err){
			// alert(err);
		});
	}
	
	// $scope.countMessages = 0;
	// $scope.messages = function(){
	// 	$scope.messagesModalIsOpen = true;
	// 	var modalInstance = $uibModal.open({
	//       animation: true,
	//       templateUrl: 'partials/messagesModal.html',
	//       controller: 'messagesModalCtrl',
	//       size: 'xs'
	//       // resolve: {
	//       //   id: function () {
	//       //   	return id;
	//       //   }
	//       // }
	//     }); 
	    
	//     modalInstance.result.then(function (selectedItem) {
	//       $scope.messagesModalIsOpen = false;
	//     });
	// };
	
	// $scope.login = function(){
	// 	var modalInstance = $uibModal.open({
	//       animation: true,
	//       templateUrl: 'partials/loginModal.html',
	//       controller: 'loginModalCtrl',
	//       size: 'xs'
	//       // resolve: {
	//       //   id: function () {
	//       //   	return id;
	//       //   }
	//       // }
	//     }); 
	// };
	// $rootScope.$on('openLoginModal',function(event){
	// 	$scope.login();
	// });
	
	// $scope.logout = function(){
	// 	authService.logOut().then(function(){
	// 		location.reload();
	// 	});
	// }
	
	// if($scope.authentication.isAuth){
	// 	$http.get(API+'users/notifications/list').success(function(data){
	// 		$scope.notificationsLs = data;
	// 		console.log($scope.notificationsLs.length);
	// 	}).error(function(err){
	// 		alert(err);
	// 	});
		
	// 	$http.get(API+'requests/myrequests/requestsCount').success(function(data){
	// 		$scope.countRequests = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});
		
	// 	$http.get(API+'conversations/count').success(function(data){
	// 		$scope.countMessages = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});
	// }
	// $rootScope.$on('messages:read',function(event,num) {
	// 	console.log(num+'num');
	//     $scope.countMessages =$scope.countMessages - parseInt(num);
	//     console.log($scope.countMessages+'num_countmessages')
	// });
	// socket.on('acquaintance-requestSent',function(data){
	// 	$scope.notificationsLs.unshift(data);
	// })
	// socket.on('messages:new',function(data){
	// 	console.log($scope.countMessages+'count messages')
	// 	$scope.countMessages++;
	// 	if($scope.messagesModalIsOpen){
	// 		$rootScope.$broadcast('messages:new',data);	
	// 	}
	// })
	// socket.on('messages:read',function(data){
	// 	console.log(data)
	// });
	return $scope;
})