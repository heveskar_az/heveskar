app.directive('loginPanel',function(){
	return {
		restrict: 'A',
		templateUrl: 'partials/login.html',
		controller: 'loginCtrl'
	}
})

.controller('loginCtrl',function($http,$log,$uibModal,$rootScope,API,$scope, authService,socket,$sce){
	$scope.createUiSref = function(text,username){
		return $sce.trustAsHtml(text.replace('_username_', username));
	}
	
	$scope.countMessages = 0;
	$scope.messages = function(){
		$scope.messagesModalIsOpen = true;
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/messagesModal.html',
	      controller: 'messagesModalCtrl',
	      size: 'xs'
	      // resolve: {
	      //   id: function () {
	      //   	return id;
	      //   }
	      // }
	    }); 
	    
	    modalInstance.result.then(function (selectedItem) {
	      $scope.messagesModalIsOpen = false;
	    });
	};
	
	$scope.login = function(){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/loginModal.html',
	      controller: 'loginModalCtrl',
	      size: 'xs'
	      // resolve: {
	      //   id: function () {
	      //   	return id;
	      //   }
	      // }
	    }); 
	};
	$rootScope.$on('openLoginModal',function(event){
		$scope.login();
	});
	
	$scope.logout = function(){
		authService.logOut().then(function(){
			location.reload();
		});
	}
	
	$scope.readNotifications = function(){
		if(!$scope.newNotificationsLs.length) return false;
		Array.prototype.unshift.apply($scope.notificationsLs, $scope.newNotificationsLs);
		$scope.newNotificationsLs = [];
		$http.post(API+'users/notifications/read-all',{})
		.success(function(response){
			console.log(response)
		}).error(function(err){
			console.log(err);
		});
	}
	
	if($scope.authentication.isAuth){
		$scope.notificationsLs = [];
		$scope.newNotificationsLs = [];
		$http.get(API+'users/notifications/list').success(function(data){
			$scope.notificationsLs = data;
		}).error(function(err){
			console.log(err);
		});
		$http.get(API+'users/notifications/not-read-list').success(function(data){
			data.forEach(function(item){
				$scope.newNotificationsLs.unshift(item);
			});
		}).error(function(err){
			console.log(err);
		});
		
		$http.get(API+'requests/myrequests/requestsCount').success(function(data){
			$scope.countRequests = data;
		}).error(function(err){
			console.log(err);
		});
		
		$http.get(API+'conversations/count').success(function(data){
			$scope.countMessages = data;
		}).error(function(err){
			console.log(err);
		});
	}
	$rootScope.$on('messages:read',function(event,num) {
		console.log(num+'num');
	    $scope.countMessages =$scope.countMessages - parseInt(num);
	    console.log($scope.countMessages+'num_countmessages')
	});
	socket.on('notifications-new',function(data){
		alert()
		$scope.newNotificationsLs.unshift(data);
	})
	socket.on('messages:new',function(data){
		console.log($scope.countMessages+'count messages')
		$scope.countMessages++;
		if($scope.messagesModalIsOpen){
			$rootScope.$broadcast('messages:new',data);	
		}
	})
	// socket.on('messages:read',function(data){
	// 	console.log(data)
	// });
	return $scope;
})

.controller('loginModalCtrl',function($scope,$http,$uibModal,$uibModalInstance,DOMAIN,authService,$state,acquaintancesService){
	$scope.user = {username:'',password:''};
	
	$scope.registration = function(){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/registration.html',
	      controller: 'registrationModalCtrl',
	      size: 'sm'
	      // resolve: {
	      //   id: function () {
	      //   	return id;
	      //   }
	      // }
	    }); 
	};
	
	$scope.checkAuth = function(){
		if($scope.user.username!=''){
			$http.post(API+'users/checkUsername',{
				username: $scope.user.username	
			}).success(function(data){
				if(data==true){
					$scope.usernameErr = false;
				}else{
					$scope.usernameErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.submit = function(user){
		if(user.username!='' && user.password!=''){
		 	authService.login(user).then(function (response) {
	 			$state.go($state.current.name, {}, { reload: true });
				location.reload();
        	},
         	function (err) {
     			$scope.msg = err.message;
         	});
		}
	}
	
	// $scope.login = function(){
	// 	if(!$scope.emailErr && !$scope.phoneErr && !$scope.usernameErr && !$scope.samepasswordErr){
	// 		$http.post(API+'users/register',{
	// 			user: $scope.user
	// 		}).success(function(data){
	// 			alert(data);
	// 			$scope.user = {username:'',phone:'',email:''};
	// 			$modalInstance.close(data);
	// 		}).error(function(err){
	// 			alert(err);
	// 		});
	// 	}else{
	// 		alert('xeta var');
	// 	}
	// }

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
  	};

	return $scope;
})


.controller('registrationModalCtrl',function($scope,$http,$uibModalInstance,API){
	$scope.user = {
		username:'',
		phone:'',
		email:'',
		address: '', 
		lat: 0, lon: 0
	};
	$scope.gPlace;
    
  	$scope.checkGeoPermission = function(){
	  	if (navigator.geolocation) {
        	navigator.geolocation.getCurrentPosition(function(position){
        		$scope.user.lat = position.coords.latitude;
        		$scope.user.lon = position.coords.longitude;
	            var latlng = new google.maps.LatLng($scope.user.lat, $scope.user.lon);
	            var geocoder = new google.maps.Geocoder();
	            geocoder.geocode({ 'latLng': latlng }, function (results, status) {
	                if (status == google.maps.GeocoderStatus.OK) {
	                    if (results[1]) {
	                    	$scope.$apply(function() {
			                    $scope.user.address = results[1].formatted_address;
			                });
	                    }
	                }
	            });
        	});
	    }
  	}
	
	$scope.checkGeoPermission();
	
	$scope.checkUsername = function(){
		if($scope.user.username!=''){
			$http.post(API+'users/checkUsername',{
				username: $scope.user.username	
			}).success(function(data){
				if(data==true){
					$scope.usernameErr = false;
				}else{
					$scope.usernameErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.checkPhone = function(){
		if($scope.user.phone){
			$http.post(API+'users/checkPhone',{
				phone: $scope.user.phone	
			}).success(function(data){
				if(data==true){
					$scope.phoneErr = false;
				}else{
					$scope.phoneErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.checkEmail = function(){
		if($scope.user.email){
			$http.post(API+'users/checkEmail',{
				email: $scope.user.email	
			}).success(function(data){
				if(data==true){
					$scope.emailErr = false;
				}else{
					$scope.emailErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.checkPasswords = function(){
		if($scope.user.password && $scope.user.samepassword){
			if($scope.user.password==$scope.user.samepassword){
				$scope.samepasswordErr = false;
			}else{
				$scope.samepasswordErr = true;
			}
		}
	}

	$scope.register = function(){
		if(!$scope.emailErr && !$scope.phoneErr && !$scope.usernameErr && !$scope.samepasswordErr){
			if($scope.user.address == '' || !$scope.user.lat || !$scope.user.lon){
				$scope.addressErr = true;
				return false;
			}else{
				$scope.addressErr = false;
			}
			$http.post(API+'users/register',{
				user: $scope.user
			}).success(function(data){
				$scope.user = {username:'',phone:'',email:''};
				$uibModalInstance.close(data);
			}).error(function(err){
				alert(err);
			});
		}else{
			alert('xeta var');
		}
	}

	$scope.cancel = function () {
	    $uibModalInstance.dismiss('cancel');
  	};

	return $scope;
});

