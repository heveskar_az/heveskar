app.controller('messagesModalCtrl',function ($scope,$uibModalInstance,$http,$state,API,acquaintancesService,authService,$rootScope,socket) {
	$scope.authentication = authService.authentication;
	$scope.messagesModal = 'opened';
	$scope.conversations = [];
	$scope.messages = [];
	
 	$scope.notSorted = function(obj){
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    }
	
	$scope.openConversations = function () {
		$scope.conversations = [];
		$scope.conversationId = 0;
	    $scope.messagesModal = 'toConversations';
	    $http.get(API+'conversations/list/').success(function(data){
			$scope.loadedConversations = true;
			// $scope.conversations = data;
			data.forEach(function(item){
				$scope.conversations[item._id] = item;
			})
			console.log($scope.conversations)
		}).error(function(err){
			$scope.loadedMsgs = true;
			alert(err);
		});
  	};
	
	$scope.openConversations();
	$scope.cancel = function () {
	    $uibModalInstance.dismiss()
  	};
  	
  	$scope.openConversation = function (conversationId,peerId) {
  		$scope.messages = [];
  		$scope.conversationId = conversationId;
  		$scope.peerId = peerId;
	    $scope.messagesModal = 'goMsgs';
	    $http.get(API+'conversations/messages/'+conversationId+'/'+peerId).success(function(data){
			$scope.loadedMsgs = true;
			$scope.messages = data.messages;
			$http
				.post(API+'conversations/read-all',{conversationId: $scope.conversationId})
				.success(function(num){
					console.log(num+'num1')
					$rootScope.$broadcast('messages:read',num);	
				}).error(function(err){
					console.log(err)
				});
		}).error(function(err){
			$scope.loadedMsgs = true;
			alert(err);
		});
  	};
  	$scope.send = function (conversationId,peerId) {
  		if($scope.message.trim() == ''){
  			return false;
  		}
  		var newMsg = {
  			'sender': $scope.authentication.userData,
  			'message': $scope.message
  		}
  		
  		
  		if(!$scope.messages){
  			$scope.messages = [];
  			$scope.messages.push(newMsg);
  		}else{
  			$scope.messages.unshift(newMsg)	
  		}
  		$scope.message = '';
	    $http.post(API+'conversations/send/',
    		{
    			conversationId: $scope.conversationId,
    			peerId: $scope.peerId,
    			message: newMsg.message
    		}
	    ).success(function(data){
			// $scope.items.push(data.newMsg);
			console.log(data);
		}).error(function(err){
			alert(err);
		});
  	};
  	
  	$scope.acquaintances = acquaintancesService.acquaintances.list;
	
	
	$rootScope.$on('messages:new',function(event,data) {
		// alert('wq12')
	    data.newMsg.sender = data.sender;
	    if(!$scope.conversationId){
	    	
	    	if($scope.conversations[data.newMsg.conversationId]){
	    		$scope.conversations[data.newMsg.conversationId].lastMessage = data.newMsg;
	    		var temp = $scope.conversations[data.newMsg.conversationId];
	    		delete $scope.conversations[data.newMsg.conversationId];
	    		$scope.conversations.unshift(temp);
	    	}
	    	// var newMsg = $('a[conv_id="'+data.newMsg.conversationId+'"]');
	    	// $('a[conv_id="'+data.newMsg.conversationId+'"]').remove();
	    	// newMsg.parent().prepend(newMsg);
	    	// alert('wq1')
	    	// if($scope.conversations){
	    	// 	alert('wq')
	    	// 	delete $scope.conversationId[data.newMsg.conversationId];
	    	// }
	    	return false;
	    }	
	    if($scope.conversationId == data.newMsg.conversationId){
	    	$rootScope.$broadcast('messages:read',1);
	    	$scope.messages.unshift(data.newMsg);
	    	$http.post(API+'conversations/read-all',{conversationId: $scope.conversationId});
	    }
	});
	
	socket.on('messages:read',function(data){
		console.log(data);
		if($scope.conversationId == data.conversationId){
			$scope.messages[0].seenBy = [data.readerId];
		}
	});
	
	return $scope;

});