app.directive('googleplace', function() {
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, model) {
            var options = {
                types: [],
                componentRestrictions: {}
            };
            scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

            google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                scope.user.lat = scope.gPlace.getPlace().geometry.location.lat();
                scope.user.lon = scope.gPlace.getPlace().geometry.location.lng();
                
                scope.$apply(function() {
                    model.$setViewValue(element.val());                
                });
            });
        }
    };
});