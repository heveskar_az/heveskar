app.directive('newGame',function(){
	return {
		restrict: 'A',
		templateUrl: 'partials/gamesNew.html',
		controller: ['$http','$log','$uibModal',function($http,$log,$uibModal){
			var newGame = this;

			newGame.newGame = function(){

				var modalInstance = $uibModal.open({
			      animation: true,
			      templateUrl: 'partials/gamesNewModal.html',
			      controller: 'openNewGameModalCtrl',
			      controllerAs: 'nGMC',
			      size: 'lg',
			      // resolve: {
			      //  	teamId:  function(){
			      //  		return $scope.team._id
			      //  	}
			      // } 
			    }); 
			}

			// $http.get('/users/info').success(function(data){
			// 	login.info = data;
			// 	if(data.isAuthenticated){
					
			// 	}
			// });

		}],
		controllerAs: 'newGameCtrl'
	}
});

app.controller('openNewGameModalCtrl',function ($scope,$uibModalInstance,$http,$state,API) {
	
	$scope.game = {};
	$scope.game.plan = {};
	$scope.game.plan.type = 1;
	$scope.game.team = 2;
	$scope.game.countMembers = 6;

	$scope.teamList = function(){
		$scope.teams = [];
		$http.get(API+'teams/myteams/list/'+ $scope.game.teamName).success(function(data){
			if(!data){
				return false;
			}
			data.forEach(function(d){
				if(d.teamId != null){
					$scope.teams.push(d);
				}
			});
		}).error(function(err){
			alert(err);
		});
	}

	$scope.selectTeam = function(selectedTeam){
		$scope.game.teamName = selectedTeam._id;
		$scope.selectedTeam = selectedTeam;
		delete $scope.teams;
	}
	$scope.unselectTeam = function(){
		delete $scope.game.teamName;
		delete $scope.selectedTeam;
		delete $scope.teams;
	}

	$scope.submit = function(){
		if(!$scope.game.team  || !$scope.game.countMembers || !$scope.game.countMembers || 
			!$scope.game.plan.type ||
				($scope.game.plan.type == 2 && (!$scope.game.plan.winner || !$scope.game.plan.loser)) || 
				($scope.game.team == 1 && !$scope.game.teamName) || 
				($scope.game.team == 2 && !$scope.game.division) 
		){
			$scope.error = 'Məlumatlarda yanlışlıq var. *-lu bölmələri dəqiqliklə doldurun.';
			return false;
		}
		$http.post(API+'games/new',{
			game: $scope.game
		}).success(function(data){
			if(data.callback=='success'){
				// $state.go("game",{id:data._id});
				$state.go("games");
				$uibModalInstance.close();
			}
		}).error(function(err){
			alert(err);
		});
	}

	$scope.hstep = 1;
  	$scope.mstep = 0;
  	$scope.ismeridian = false;
	var d = new Date();
	d.setHours( 9 );
	d.setMinutes( 0 );
	$scope.game.date = d;

	$scope.closeModal = function(){
		$uibModalInstance.close();
	}

	return $scope;

});