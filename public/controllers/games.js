app.controller('gamesController',function($http,$scope,$uibModal,$timeout,$stateParams,API){
	$scope.games = [];
	$scope.pageId = $stateParams.id;
	if($stateParams.id == 2){
		$http.get(API+'games/shortlist/2').success(function(data){
			$scope.games = data;
		}).error(function(err){
		alert(err);
		});
	}else{
		$http.get(API+'games/shortlist/1').success(function(data){
			$scope.games = data;
		}).error(function(err){
			alert(err);
		});
	}



	return $scope;
});
app.controller('gameController', function($http,$scope,$uibModal,$timeout,id,API){

	$scope.id = id;
	$scope.replyInput = [];
	$scope.reply = [];

	$scope.replies = [];

	$scope.init = function(){
		$http.get(API+'games/more/'+id).success(function(data){
			$scope.game = data;
			// $(".commentsArea").mCustomScrollbar();

			$scope.game.comments.forEach(function(i){
				$scope.replies[i._id] = [];
				i.replies.forEach(function(ii){
					$scope.replies[i._id].push(ii);
				});
			});

		}).error(function(err){
			$scope.error = err;
		});

		$http.get(API+'games/showOffers/'+id).success(function(data){
			$scope.offers = data;
		}).error(function(err){
			$scope.error = err;
		});

		$http.get(API+'games/connectedUsers/'+id).success(function(data){
			$scope.connectedUsers = data;
		}).error(function(err){
			$scope.error = err;
		});
	}

	$scope.init();

	$scope.makeOffer = function(){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/gamesOffer.html',
	      controller: 'offerModalCtrl',
	      controllerAs: 'nGMC',
	      size: 'xs',
	      resolve: {
	       	gameId:  function(){
	       		return $scope.id
	       	}
	      } 
	    }); 
	}

	$scope.acceptUserRequest = function(offerId){
		$http.post(API+'games/acceptUserRequest',{offerId: offerId,gameId: $scope.id})
		.success(function(data){	
			if(data.callback == 'success'){
				$scope.success = '';
				$scope.init();
			}
		}).error(function(err){
			$scope.error = err;
		});
	}

	$scope.rejectUserRequest = function(reqId){
		$http.post(API+'requests/myrequests/rejectUserRequest',{reqId: reqId}).success(function(data){			
			alert(data);
			$scope.init();
		}).error(function(err){
			$scope.error = err;
		});
	}

	$scope.sendComment = function(commentId){

		if(commentId){
			$http.post(API+'games/newComment',{comment: $scope.reply[commentId], gameId: $scope.id, commentId: commentId})
			.success(function(data){	
				if(data){
					$scope.success = '';
					delete $scope.reply[commentId];

					$scope.replies[commentId].push(data);
					// $scope.init();
				}
			}).error(function(err){
				$scope.error = err;
			});

		}else{
			$http.post(API+'games/newComment',{comment: $scope.comment, gameId: $scope.id})
			.success(function(data){	
				if(data){
					$scope.success = '';
					delete $scope.comment;

					$scope.game.comments.push(data);
					// $scope.init();
				}
			}).error(function(err){
				$scope.error = err;
			});
		}
	}

	$scope.reply = function(commentId){
		$scope.replyInput[commentId] = true;
	}

	$scope.deleteComment = function(commentId){
		$http.post(API+'games/deleteComment',{commentId: commentId,gameId: $scope.id})
		.success(function(data){	
			console.log(data);
		}).error(function(err){
			// $scope.error = err;
		});
	}

	return $scope;
});

app.controller('offerModalCtrl', function($scope,$uibModalInstance,$http,$state,gameId,API){

	$scope.offer = {};

	$scope.offer.gameId = gameId;

	$scope.teamList = function(){
		$scope.teams = [];
		$http.get(API+'teams/myteams/list/'+ $scope.game.teamName).success(function(data){
			data.forEach(function(d){
				if(d.teamId != null){
					$scope.teams.push(d);
				}
			});
			console.log($scope.teams);
		}).error(function(err){
			$scope.error = err;
		});
	}

	$scope.selectTeam = function(selectedTeam){
		$scope.offer.teamId = selectedTeam._id;
		$scope.selectedTeam = selectedTeam;
		delete $scope.teams;
	}
	$scope.unselectTeam = function(){
		delete $scope.offer.teamId;
		delete $scope.selectedTeam;
		delete $scope.teams;
	}

	$scope.submit = function(){

		if(!$scope.offer.teamId){
			$scope.error = 'Məlumatlarda yalnışlıq var. Komandanızı seçin.';
			return false;
		}
		$http.post(API+'games/newOffer',{
			offer: $scope.offer
		}).success(function(data){
			if(data.callback=='success'){

				$scope.success = 'Təklifiniz uğurla göndərildi';
				// $state.go("game",{id:data._id});
				// $state.go("games");
				// $uibModalInstance.close();
			}else{
				$scope.error = data;
			}
		}).error(function(err){
			$scope.error = err;
		});
	}

	// $scope.hstep = 1;
 //  	$scope.mstep = 0;
 //  	$scope.ismeridian = false;
	// var d = new Date();
	// d.setHours( 9 );
	// d.setMinutes( 0 );
	// $scope.game.date = d;

	$scope.closeModal = function(){
		$uibModalInstance.close();
	}

	return $scope;

});


// app.controller('openNewGameModalCtrl',['$scope','$uibModalInstance','$http', function ($scope,$uibModalInstance,$http) {
	
// 	// $scope.teamId = teamId;	

// 	// $scope.listOfUsers = function(){
// 	// 	$http.post(API+'users/list/',{username:$scope.username}).success(function(data){
// 	// 		$scope.users = data.data;
// 	// 	}).error(function(err){
// 	// 		alert(err);
// 	// 	});
// 	// };
// 	// $scope.addIntoTeam = function(userId){
// 	// 	$http.post(API+'teams/addUser',{
// 	// 		userId: userId,
// 	// 		teamId: $scope.teamId
// 	// 	}).success(function(data){
// 	// 		$scope.success = data;
// 	// 	}).error(function(err){
// 	// 		$scope.error = err;
// 	// 	});
// 	// };
// 	// $scope.closeModal = function(){
// 	// 	$uibModalInstance.close();
// 	// }
// }]);
// app.controller('teamsController',['$http','$scope','$uibModal',
// function($http,$scope,$uibModal){
	
// 	$http.get(API+'teams/list/').success(function(data){
// 		$scope.teams = data;
// 	}).error(function(err){
// 		alert(err);
// 	});


// 	return $scope;
// }]);

// app.controller('myTeamsController',['$http','$scope','$uibModal',
// function($http,$scope,$uibModal){
// 	$http.get(API+'teams/myteams/').success(function(data){
// 		$scope.teams = data;
// 	}).error(function(err){
// 		alert(err);
// 	});

// 	$scope.newTeam = function(){
// 		var modalInstance = $uibModal.open({
// 	      animation: true,
// 	      templateUrl: 'partials/newTeam.html',
// 	      controller: 'openNewTeamModalCtrl',
// 	      controllerAs: 'nTC',
// 	      size: 'lg'
// 	    }); 
// 	}

// 	return $scope;
// }]);
// app.controller('openNewTeamModalCtrl', function ($scope, $uibModalInstance,$http,$state) {
// 	$scope.checkTeamName = function(){
// 		if($scope.name!=''){
// 			$http.post(API+'teams/checkTeamName',{
// 				name: $scope.name
// 			}).success(function(data){
// 				if(data==true){
// 					$scope.teamNameErr = false;
// 				}else{
// 					$scope.teamNameErr = true;
// 				}
// 			}).error(function(err){
// 				alert(err);
// 			});
// 		}
// 	}

// 	$scope.newTeamRegister = function(){
// 		if($scope.name && !$scope.teamNameErr){
// 			$http.post(API+'teams/register',{
// 				name: $scope.name
// 			}).success(function(data){
// 				$uibModalInstance.close(data);
// 				$state.go("teamInfo",{name:data});
// 			}).error(function(err){
// 				$scope.errorMsg = err;
// 			});
// 		}else{
// 			$scope.errorMsg = 'Komanda adını daxil edin';
// 		}
// 	}
// 	return $scope;
// });
