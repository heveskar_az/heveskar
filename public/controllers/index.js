app.controller('IndexController',function($http,$scope,$filter,$uibModal,$rootScope,API,socket,$sce){
	$scope.getPostText = function(text,gameId){
		return $sce.trustAsHtml(text.replace('_gameId_', gameId));
	}
	if($scope.authentication.isAuth){
		
		//get news feed
		$http.get(API+'posts/posts').success(function(data){
			$scope.posts = data;
		}).error(function(err){
			// // alert(err);
		});
		
		$scope.post = function(){
			if(!$scope.postText || $scope.postText.trim() == '') return false;
			$http.post(API+'posts/post',{'text': $scope.postText}).success(function(data){
				$scope.postText = '';
			}).error(function(err){
				// // alert(err);
			});
		}
		
		
	}else{
		//get games
		$http.get(API+'games/shortlist/1').success(function(data){
			$scope.games = data;
		});
		$http.get(API+'games/shortlist/2').success(function(data){
			$scope.mixgames = data;
		});
	}
	
	socket.on('new-post',function(data){
		var post = data.post;
		post.userId = data.userId;
		if(!$scope.posts){
			$scope.posts = [];
		}
		$scope.posts.unshift(post);
	})
	
	// alert('index');
	// $http.get(DOMAIN+'users/info').success(function(data){
	// 	if(data.isAuthenticated){
	// 		$scope.isAuthenticated = data.isAuthenticated;
	// 		$rootScope.isAuthenticated = $scope.isAuthenticated;
	// 		//get posts 
	// 		$scope.posts = [{id:1},{id:2}];
	// 	}else{
	// 		$rootScope.isAuthenticated = false;
	// 		$http.get(DOMAIN+'games/shortlist/1').success(function(data){
	// 			$scope.games = data;
	// 		}).error(function(err){
	// 			alert(err);
	// 		});

	// 		$http.get(DOMAIN+'games/shortlist/2').success(function(data){
	// 			$scope.mixgames = data;
	// 		}).error(function(err){
	// 			alert(err);
	// 		});
	// 	}
	// }).error(function(err){
	// 	$scope.isAuthenticated = false;
	// 	$rootScope.isAuthenticated = false;
	// 	$http.get(DOMAIN+'games/shortlist/1').success(function(data){
	// 		$scope.games = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});

	// 	$http.get(DOMAIN+'games/shortlist/2').success(function(data){
	// 		$scope.mixgames = data;
	// 	}).error(function(err){
	// 		alert(err);
	// 	});
	// 	alert(err);
	// });

	return $scope;
});