app.controller('teamInfoController', function($http,$scope,$uibModal,name,$timeout,$state,API,acquaintancesService){
	$scope.isJoined = false;
	$scope.checkMember = function(){
		if($scope.authentication.isAuth){
			$http.post(API+'teams/members/checkIsJoined',{
				teamId: $scope.team._id
			}).success(function(data){
				$scope.isJoined = data;
			}).error(function(err){
				//alert(err);
			});	
		}
	}

	$scope.init = function(){
		$http.get(API+'teams/info/'+name).success(function(data){
			$scope.team = data.dataTeam;
			$scope.previousTeamInfo.name = data.dataTeam.name;
			$scope.members = data.dataMembers;
			$scope.wantedMembers = data.wantedMembers;
			$scope.captain = data.captain;
			$scope.checkMember();
		}).error(function(err){
			//alert(err);
		});
	}

	$scope.joinTeam = function(){
		$http.post(API+'teams/members/join',{
			teamId: $scope.team._id
		}).success(function(data){
			if(data == 'deleted'){
				delete $scope.isJoined;
				delete $scope.captain;
			}
			if(data == 'isMember'){
				$scope.isJoined = 1;
			}
			if(data == 'sentRequest'){
				$scope.isJoined = 2;
			}
			$scope.init();
		}).error(function(err){
			//alert(err);
		});
	}

	$scope.acceptJoining = function(reqId){
		$http.post(API+'teams/members/acceptJoining',{
			reqId: reqId
		}).success(function(data){
			$scope.init();
		}).error(function(err){
			//alert(err);
		});
	}
	$scope.rejectJoining = function(reqId){
		$http.post(API+'teams/members/rejectJoining',{
			reqId: reqId
		}).success(function(data){
			$scope.init();
		}).error(function(err){
			//alert(err);
		});
	}
	$scope.removeJoined = function(userId){
		$http.post(API+'teams/members/removeJoined',{
			userId: userId,
			teamId: $scope.team._id
		}).success(function(data){
			$scope.init();
		}).error(function(err){
			//alert(err);
		});
	}



	$scope.previousTeamInfo = {};
	$scope.init();
	
	
	$scope.addAcquaintance = function(userId){
		acquaintancesService.setNewUserToList(userId)
		.then(function(response){
			if(response.msg == 'requestSent'){
				$scope.acquaintances[userId] = response.data;	
			}
			if(response.msg == 'deleted'){
				delete $scope.acquaintances[userId];
			}
			if(response.msg == 'added'){
				$scope.acquaintances[userId] = response.data;
			}
		},function(err){
				
		});
	}

	$scope.editForm = false;
	$scope.changeNameFunc = function(){
		if($scope.editForm)	{		$scope.editForm=false;$scope.team.name = $scope.previousTeamInfo.name; 	}
		else				{		$scope.editForm=true;													}
	}

	$scope.save = function(){
		$http.post(API+'teams/update',{
			teamName: $scope.team.name,
			teamId: $scope.team._id
		}).success(function(data){
			$scope.successFeedback = data;
			$scope.previousTeamInfo.name = $scope.team.name;
			$scope.editForm = false;
			$timeout(function(){
				$scope.saveFeedback = false;
				$scope.team.name = $scope.team.name.toLowerCase();
				$state.go("teamInfo",{name:$scope.team.name});
			}, 3000);
		}).error(function(err){
			$scope.dangerFeedback = err;
			$timeout(function(){
				$scope.dangerFeedback = false;
			}, 3000);
		});
	}

	$scope.newPlayer = function(){

		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/newPlayer.html',
	      controller: 'openNewPlayerModalCtrl',
	      controllerAs: 'nPMC',
	      size: 'lg',
	      resolve: {
	       	teamId:  function(){
	       		return $scope.team._id
	       	}
	      } 
	    }); 
	}

	$scope.openFull = function(url){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/openFullModal.html',
	      controller: 'openFullModalCtrl',
	      size: 'lg',
	      resolve: {
	        url: function () {
	          return url;
	        }
	      }
	    }); 
	}
	$scope.delImg = function(){
		$http.post(API+'teams/delImg',{teamName: $scope.team.name}).success(function(data){
			location.reload();
		}).error(function(err){
			//alert(err);
		});
	}

	$scope.uploadFile = function(files) {
		$scope.uploadedImg = true;
		files[0]

		var reader = new FileReader();

		reader.onload  = function(e){
			var dataUrl = reader.result;
			$timeout(function() {
		  		var width=0;
		  		var widthofBar=document.getElementById('uploadingAvatarBar').offsetWidth;
		      	var longness=step=widthofBar/100;
		      	var progressInterval=setInterval(function () {
		      		width++;
		      		longness=longness+step;        
					$('#uploadingAvatar').attr('aria-valuenow',width);
					$('#uploadingAvatar').width(longness+'px');
					if(width==100){
						width=0;
						longness=0;
					}
		      	}, 100);
		  		$http.post(API+'teams/uploadImg',{data:dataUrl,teamName: $scope.team.name}).success(function(data){
		  			$scope.uploadedImg = data;
		  			clearInterval(progressInterval);
		  			location.reload();
		  		}).error(function(err){
		  			//alert(err);
		  			clearInterval(progressInterval);
		  			$scope.fileread = false;
		  		});


		  	}, 10);
		}

		reader.readAsDataURL(files[0]);
	};

	return $scope;
});

app.controller('openNewPlayerModalCtrl', function ($scope,$uibModalInstance,$http,teamId,API,$rootScope) {
	
	$scope.teamId = teamId;	
	$scope.selectedUser = false;
	$scope.listOfUsers = function(){
		$http.post(API+'users/list/',{username:$scope.username}).success(function(data){
			$scope.users = data.data;
		}).error(function(err){
			// //alert(err);
		});
	};
	$scope.addIntoTeam = function(userId){
		$http.post(API+'teams/addUser',{
			userId: userId,
			teamId: $scope.teamId
		}).success(function(data){
			$rootScope.$broadcast('newNotification', {'title': 'Əhsən!','alert': data});
		}).error(function(err){
			$rootScope.$broadcast('newNotification', {'title': 'Xəta var!','alert': err});
		});
	};
	$scope.selectUser = function(user){
		if($scope.selectedUser == user) $scope.selectedUser = false;
		else $scope.selectedUser = user;
	}
	$scope.cancel = function(){
		$uibModalInstance.close();
	}
});
app.controller('teamsController', function($http,$scope,$uibModal,API,gmap){
	
	$http.get(API+'teams/list/').success(function(data){
		$scope.teams = data;
		data.forEach(function(team){
			console.log(team);
			gmap.addTeamMarker(team.location[0],team.location[1],team.img,team.name);	
		});
	}).error(function(err){
		// //alert(err);
	});

	google.maps.event.addDomListener(
		window, 
		'load', 
		gmap.init(
			$scope.authentication.userData.location[0],
			$scope.authentication.userData.location[1],
			13
		)
	);
	gmap.addUserMarker($scope.authentication.userData.location[0],$scope.authentication.userData.location[1],$scope.authentication.userData.username);

	return $scope;
});

app.controller('myTeamsController',function($http,$scope,$uibModal,API){
	$http.get(API+'teams/myteams/').success(function(data){
		$scope.teams = data;
		$scope.ajaxSuccess = true;
	}).error(function(err){
		// //alert(err);
	});

	$scope.newTeam = function(){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/newTeam.html',
	      controller: 'openNewTeamModalCtrl',
	      controllerAs: 'nTC',
	      size: 'lg'
	    }); 
	}

	return $scope;
});
app.controller('openNewTeamModalCtrl', function ($scope, $uibModalInstance,$http,$state,API) {
	$scope.checkTeamName = function(){
		if($scope.name!=''){
			$http.post(API+'teams/checkTeamName',{
				name: $scope.name
			}).success(function(data){
				if(data==true){
					$scope.teamNameErr = false;
				}else{
					$scope.teamNameErr = true;
				}
			}).error(function(err){
				$scope.teamNameErr = err;
			});
		}
	}

	$scope.newTeamRegister = function(){
		if($scope.name && !$scope.teamNameErr){
			$http.post(API+'teams/register',{
				name: $scope.name
			}).success(function(data){
				$uibModalInstance.close(data);
				$state.go("teamInfo",{name:data});
			}).error(function(err){
				$scope.errorMsg = err;
			});
		}else{
			$scope.errorMsg = 'Komanda adını daxil edin';
		}
	}
	return $scope;
});
