app.controller('AppController',function($http,$scope,$location,$filter,$rootScope,DOMAIN,authService,$timeout,acquaintancesService,socket){
    $scope.notifications = [];
    $scope.logout = function(){
		authService.logOut()
		// .then(function(){
			location.reload();
		// });
	}
	
	$rootScope.$on('newNotification', function (event, data) {
		$scope.notifications.push({'title': data.title,'alert': data.alert});
		$timeout(function() {
	        $scope.notifications.splice(0,1);
	    }, 5000);	  	
	});
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        $scope.isStateLoading = true;
	});	
	$rootScope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
        delete $scope.isStateLoading;
	});
    
    
	$scope.authentication = authService.authentication;
	$scope.acquaintances = acquaintancesService.acquaintances.list;
	return $scope;
});