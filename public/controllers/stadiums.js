app.controller('StadiumScheduleController',['$http','$scope','$filter','$uibModal','id', function($http,$scope,$filter,$uibModal,id){
	$scope.date = new Date();

	$http.get('/stadion/stadium/info/'+id).success(function(data){
		$scope.stadium = data;
		$scope.showSchedule();
	});

	$scope.showSchedule = function(){
		$scope.dateF = $filter('date')($scope.date, 'yyyy-MM-dd');
		if(id && $scope.date){
			$http.post('/stadion/schedule', 
				{	
					id: id,
					date:$scope.dateF,
				}
			).
		  	success(function(data) {
		  		$scope.schedule = [];
		  		for(i=0;i<=23;i++){
		  			if(i<10){	saat = '0'+i+':00';	}else{	saat= i+':00';}
		  			var tempObj = { 
		  				row: i,status: 0,saat: saat, stadion: $scope.stadium.stad_id.name+'-'+$scope.stadium.name,
		  				price: $scope.stadium.schedule[saat].price , payment: ''
		  			}
		  			angular.forEach(data,function(row){
						if(row.time==saat){
							tempObj.status = row.status;
							// tempObj.price = row.price; 
							// tempObj.payment = row.payment; 
							return false;
						}
					});
					if($scope.stadium.schedule[saat].work){
						$scope.schedule.push(tempObj);
					}
		  		};

		  	}).
		  	error(function(data) {
		  		alert(data);
			    $scope.schedule = {}
		  	});
		}
	};

	$scope.bookPlace = function(saat){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/booking.html',
	      controller: 'bookingModalInstanceCtrl',
	      size: 'sm',
	      resolve: {
	        item: function () {
	        	return {
        			stadium: id,
        			stadiumName: $scope.stadium.name,
					date: $scope.dateF,
					saat: saat,
					price: $scope.stadium.schedule[saat].price
	        	}
	        }
	      }
	    });
	    modalInstance.result.then(function(data) { //make sure you are declaring your promise
	      	$scope.show();
     	});
	}
	return $scope;
}]);
app.controller('bookingModalInstanceCtrl',['$http','$scope','$uibModalInstance','item',function($http,$scope,$uibModalInstance,item){
	$http.post('/stadion/schedule/register', {	
			stadium: item.stadium,
			stadiumName: item.stadiumName,
			date: item.date,
			saat: item.saat,
			price: item.price
	}).success(function(data){
		if(data){
			$scope.item = data;
			$scope.item.stadium = item.stadium;
			$scope.item.stadiumName = item.stadiumName;
		}else{
			$scope.item = {
				stadium: item.stadium,
				stadiumName: item.stadiumName,
				date: item.date,
				saat: item.saat,
				price: item.price
			};
		}
	}).
	error(function(){
		$scope.item = {
			stadium: item.stadium,
			stadiumName: item.stadiumName,
			date: item.date,
			saat: item.saat,
			price: item.price
		};
	});	

	$scope.save = function () {
		$http.post('/stadion/schedule/update', {	
			item: $scope.item
		}).success(function(data){
			$uibModalInstance.close(data);
		}).error(function(data){
			$uibModalInstance.close("Xəta var");
		});
  	};

  	$scope.delete = function () {
		$http.post('/stadion/schedule/delete', {	
			item: $scope.item
		}).success(function(data){
			$uibModalInstance.close(data);
		}).error(function(data){
			$uibModalInstance.close("Xəta var");
		});
  	};

  	$scope.cancel = function () {
  		console.log($scope.item);
	    $uibModalInstance.dismiss('cancel');
  	};
}]);


app.controller('StadiumsController',function($http,$scope,$uibModal,gmap){
	
	$http.get('/stadion/list/').success(function(data){
		$scope.stadiums = data;
	}).error(function(err){
		alert(err);
	});
	$http.get('/stadion/stadiums/list/').success(function(data){
		$scope.allStadiums = data;
	}).error(function(err){
		alert(err);
	});

	google.maps.event.addDomListener(
		window, 
		'load', 
		gmap.init(
			$scope.authentication.userData.location[0],
			$scope.authentication.userData.location[1],
			13
		)
	);
	gmap.addUserMarker($scope.authentication.userData.location[0],$scope.authentication.userData.location[1],$scope.authentication.userData.username);
	

	$scope.openImages = function(id){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/openStadImages.html',
	      controller: 'openStadImagesCtrl',
	      size: 'lg',
	      resolve: {
	        id: function () {
	        	return id;
	        }
	      }
	    }); 
	};

	return $scope;
});
app.controller('openStadImagesCtrl', function ($scope, $uibModalInstance,$http, id) {
	$http.get('/stadion/stadium/images/'+id).success(function(data){
		$scope.images = data.images;
	}).error(function(err){
		alert(err);
	});
	return $scope;
});