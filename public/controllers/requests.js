app.controller('myRequestsController',function($http,$scope,$uibModal,API,$stateParams,acquaintancesService){
	$scope.pages = ['heveskar','oyun','komanda'];
	$scope.init = function(){
		if($scope.authentication.isAuth){
			$http.get(API+'requests/myrequests/'+$scope.page).success(function(data){
				$scope.items = data;
			}).error(function(err){
				// alert(err);
			});
		}
	}
	if($scope.pages.indexOf($stateParams.page) != -1) $scope.page = $stateParams.page;
	else $scope.page = 'heveskar';
	$scope.init();

	$scope.acceptUserRequest = function(userId){
		acquaintancesService.setNewUserToList(userId)
		.then(function(response){
			if(response.msg == 'added'){
				$scope.acquaintances[userId] = response.data;
			}
			$scope.init();
		},function(err){
			
		});
	}
	$scope.rejectUserRequest = function(userId){
		acquaintancesService.setNewUserToList(userId,true)
		.then(function(response){
			if(response.msg == 'added'){
				$scope.acquaintances[userId] = response.data;
			}
			$scope.init();
		},function(err){
			
		});
	}

	$scope.acceptTeamRequest = function(reqId){
		$http.post(API+'requests/myrequests/acceptTeamRequest',{reqId: reqId}).success(function(data){			
			// alert(data);
			$scope.init();
		}).error(function(err){
			// alert(err);
		});
	}
	$scope.rejectTeamRequest = function(reqId){
		$http.post(API+'requests/myrequests/rejectTeamRequest',{reqId: reqId}).success(function(data){			
			// alert(data);
			$scope.init();
		}).error(function(err){
			// alert(err);
		});
	}

	return $scope;
});