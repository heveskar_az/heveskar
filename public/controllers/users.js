app.controller('userProfileController',function($http,$scope,$filter,$uibModal,username,$timeout,API,acquaintancesService,gmap){

	$scope.checkAcquaintance = function(){
		$http.post(API+'users/acquaintances/check',{
			userId: $scope.userInfo.data._id
		}).success(function(data){
			$scope.isAcquaintance = data;
		}).error(function(err){
			alert(err);
		});
	}

	$scope.addAcquaintance = function(){
		acquaintancesService.setNewUserToList($scope.userInfo.data._id)
		.then(function(response){
			$scope.isAcquaintance = response.msg;
		},function(err){
				
		});
	}
	
	$scope.acquaintances = function(){
		$http.post(API+'users/acquaintances/list',{
			userId: $scope.userInfo.data._id
		}).success(function(data){
			$scope.acquaintances = data;
		}).error(function(err){
			alert(err);
		});
	}
	
	$scope.teams = function(){
		$http.post(API+'users/teams/list/count',{
			userId: $scope.userInfo.data._id
		}).success(function(data){
			$scope.teams = data;
		}).error(function(err){
			$scope.teams = '-'; 
		});
	}

	$scope.previousUserInfo = {};
	$http.get(API+'users/profile/'+username).success(function(data){
		$scope.userInfo = data;
		$scope.previousUserInfo.username = $scope.userInfo.data.username;
		$scope.previousUserInfo.name = $scope.userInfo.data.name;
		$scope.previousUserInfo.phone = $scope.userInfo.data.phone;
		$scope.previousUserInfo.email = $scope.userInfo.data.email;
		$scope.previousUserInfo.age = $scope.userInfo.data.age;
		$scope.previousUserInfo.gender = $scope.userInfo.data.gender;
		$scope.previousUserInfo.address = $scope.userInfo.data.address;
		$scope.previousUserInfo.location = $scope.userInfo.data.location;
		$scope.previousUserInfo.position = $scope.userInfo.data.position;
		$scope.previousUserInfo.level = $scope.userInfo.data.level;
		$scope.previousUserInfo.newPassword = $scope.userInfo.data.newPassword;
		
		$scope.checkAcquaintance();
		$scope.acquaintances();
		$scope.teams();
		google.maps.event.addDomListener(
			window, 
			'load', 
			gmap.init(
				$scope.userInfo.data.location[0],
				$scope.userInfo.data.location[1],
				14
			)
		);
		gmap.addUserMarker($scope.userInfo.data.location[0],$scope.userInfo.data.location[1],$scope.userInfo.data.username);
	}).error(function(err){
		console.log(err);
	});

	$scope.editForm = false;
	$scope.edit = function(){
		if($scope.editForm){
			$scope.editForm  = false;
		}else{
			$scope.editForm  = true;
		}
	}

	$scope.save = function(){
		$http.post(API+'users/update',{
			data: $scope.userInfo.data
		}).success(function(data){
			$scope.saveFeedback = true;
			$scope.previousUserInfo.username = $scope.userInfo.data.username;
			$scope.previousUserInfo.name = $scope.userInfo.data.name;
			$scope.previousUserInfo.phone = $scope.userInfo.data.phone;
			$scope.previousUserInfo.email = $scope.userInfo.data.email;
			$scope.previousUserInfo.age = $scope.userInfo.data.age;
			$scope.previousUserInfo.gender = $scope.userInfo.data.gender;
			$scope.previousUserInfo.address = $scope.userInfo.data.address;
			$scope.previousUserInfo.position = $scope.userInfo.data.position;
			$scope.previousUserInfo.level = $scope.userInfo.data.level;
			$scope.editForm = false;
			$timeout(function(){
				$scope.saveFeedback = false;
			}, 2000);
		}).error(function(err){
			alert(err);
		});
	}
	$scope.checkUsername = function(){
		if($scope.userInfo.data.username!=''){
			$http.post(API+'users/checkUsername',{
				username: $scope.userInfo.data.username	
			}).success(function(data){
				if(data==true){
					$scope.usernameErr = false;
				}else{
					$scope.usernameErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.checkPhone = function(){
		if($scope.user.phone){
			$http.post('/users/checkPhone',{
				phone: $scope.user.phone	
			}).success(function(data){
				if(data==true){
					$scope.phoneErr = false;
				}else{
					$scope.phoneErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.checkEmail = function(){
		if($scope.user.email){
			$http.post(API+'users/checkEmail',{
				email: $scope.user.email	
			}).success(function(data){
				if(data==true){
					$scope.emailErr = false;
				}else{
					$scope.emailErr = true;
				}
			}).error(function(err){

			});
		}
	}
	$scope.checkPasswords = function(){
		if($scope.userInfo.data.newPassword && $scope.userInfo.data.repeatNewPassword){
			if($scope.userInfo.data.newPassword==$scope.userInfo.data.repeatNewPassword){
				$scope.samepasswordErr = false;
			}else{
				$scope.samepasswordErr = true;
			}
		}
	}
	$scope.openFull = function(url){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/openFullModal.html',
	      controller: 'openFullModalCtrl',
	      size: 'lg',
	      resolve: {
	        url: function () {
	          return url;
	        }
	      }
	    }); 
	}
	$scope.delImg = function(){
		$http.post('users/delImg',{}).success(function(data){
			location.reload();
		}).error(function(err){
			alert(err);
		});
	}


	$scope.acquaintancesList = function(){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/acquaintancesList.html',
	      // controller: 'acqListModalCtrl',
	      size: 'xs'
	      // resolve: {
	      //   id: function () {
	      //   	return id;
	      //   }
	      // }
	    }); 
	}
	$scope.teamsList = function(){
		var modalInstance = $uibModal.open({
	      animation: true,
	      templateUrl: 'partials/teamsList.html',
	      controller: 'teamsListModalCtrl',
	      size: 'xs',
	      resolve: {
	        id: function () {
	        	return $scope.userInfo.data._id;
	        }
	      }
	    }); 
	}
	
	$scope.uploading = false;
	$scope.changeImg = function(){
	    $timeout(function() {
	    	angular.element('#upload').trigger('click');
	  	}, 100);
	};

	return $scope;
});

app.controller('teamsListModalCtrl', function ($scope, $uibModalInstance, id,$http,$rootScope) {
	$scope.isAuthenticated = $rootScope.isAuthenticated;
	$http.post('/users/teams/list',{
		userId: id
	}).success(function(data){
		$scope.teams = data;
	}).error(function(err){
		$scope.teams = {};
	});
	return $scope;
});

app.controller('openFullModalCtrl', function ($scope, $uibModalInstance, url) {
	$scope.url = url;
});