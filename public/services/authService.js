'use strict';
app.factory('authService', ['$http', '$q', 'localStorageService','DOMAIN', function ($http, $q, localStorageService,DOMAIN) {

    var serviceBase = DOMAIN;
    var authServiceFactory = {};

    var _authentication = {
        isAuth: false,
        userData : false
    };

    // var _saveRegistration = function (registration) {

    //     _logOut();

    //     return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
    //         return response;
    //     });

    // };

    var _login = function (loginData) {

        // var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();
        $http.post(serviceBase+'api/login', loginData).success(function (response) {
            localStorageService.set('authorizationData', { 
                token: response.token, 
                userData: response.data 
            });
            _authentication.isAuth = true;
            _authentication.userData = response.data;

            deferred.resolve(response);

        }).error(function (err, status) {
            _logOut();
            deferred.reject(err);
        });

        return deferred.promise;

    };
    
    var _setNewProfileImg = function (img) {
        var deferred = $q.defer();
        var authData = localStorageService.get('authorizationData');
        authData.userData.img = img;
        localStorageService.set('authorizationData', { 
            token: authData.token, 
            userData: authData.userData,
            isAuth: true,
        });
        deferred.resolve(img);
        return deferred.promise;
    };

    var _logOut = function () {

        localStorageService.remove('authorizationData');
        localStorageService.remove('acquaintances');

        _authentication.isAuth = false;
        _authentication.userData = false;

    };

    var _fillAuthData = function () {

        var authData = localStorageService.get('authorizationData');
        if (authData)
        {
            _authentication.isAuth = true;
            _authentication.userData = authData.userData;
        }

    }

    // authServiceFactory.saveRegistration = _saveRegistration;
    authServiceFactory.login = _login;
    authServiceFactory.logOut = _logOut;
    authServiceFactory.fillAuthData = _fillAuthData;
    authServiceFactory.setNewProfileImg = _setNewProfileImg;
    authServiceFactory.authentication = _authentication;

    return authServiceFactory;
}]);