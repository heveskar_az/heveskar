app.factory('gmap', function () {
    var map = null;
    var mapFactory = {};
    
    var _init = function(lat,lng,zoom){
      
      // Basic options for a simple Google Map
      // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
      var mapOptions = {
          // How zoomed in you want the map to start at (always required)
          zoom: zoom,

          // The latitude and longitude to center the map (always required)
          center: new google.maps.LatLng(lat, lng),
          disableDefaultUI: true,
          // How you would like to style the map. 
          // This is where you would paste any style found on Snazzy Maps.
          styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}]
      };
      
      // Get the HTML DOM element that will contain your map 
      // We are using a div with id="map" seen below in the <body>
      var mapElement = document.getElementById('map');
      map = new google.maps.Map(mapElement, mapOptions);
      
      google.maps.event.addDomListener(window, "resize", function() {
  		 	var center = map.getCenter();
  		 	google.maps.event.trigger(map, "resize");
  		 	map.setCenter(center); 
  		});
    }
    var _addUserMarker = function(lat,lng,title){
      // Create the Google Map using our element and options defined above
      // var image = {
      //   url: img,
      //   // This marker is 20 pixels wide by 32 pixels high.
      //   scaledSize: new google.maps.Size(35, 35),
      //   // The origin for this image is (0, 0).
      //   origin: new google.maps.Point(0, 0),
      //   // The anchor for this image is the base of the flagpole at (0, 32).
      //   anchor: new google.maps.Point(0, 0)
      // };
  
      // Let's also add a marker while we're at it
      var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat, lng),
          map: map,
          // icon: image,
          title: title
      });
    }
    
    var _addTeamMarker = function(lat,lng,img,title){
      // Create the Google Map using our element and options defined above
      var image = {
        url: img,
        // This marker is 20 pixels wide by 32 pixels high.
        scaledSize: new google.maps.Size(35, 35),
        // The origin for this image is (0, 0).
        origin: new google.maps.Point(0, 0),
        // The anchor for this image is the base of the flagpole at (0, 32).
        anchor: new google.maps.Point(0, 0)
      };
  
      // Let's also add a marker while we're at it
      var marker = new google.maps.Marker({
          position: new google.maps.LatLng(lat, lng),
          animation: google.maps.Animation.BOUNCE,
          map: map,
          icon: image,
          // title: title,
          // label: title
      });
      marker.addListener('click', function() {
        map.setZoom(14);
        map.setCenter(marker.getPosition());
      });
    }
    
    mapFactory.init = _init;
    mapFactory.addUserMarker = _addUserMarker;
    mapFactory.addTeamMarker = _addTeamMarker;
    
    return mapFactory;
});