'use strict';
app.factory('acquaintancesService', function ($http, $q, localStorageService,API) {
    var acquaintancesFactory = {};
    
    var _acquaintances = {
        isLoaded : false,
        list : {}
    };

    var _getAcquaintances = function () {
        
        if(!localStorageService.get('authorizationData')){
            _acquaintances.list = {};
            _acquaintances.isLoaded = true;
            return false;
        }
        // var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

        var deferred = $q.defer();
        $http.post(API+'users/acquaintances/ids', 
            { 'userId' : localStorageService.get('authorizationData').userData._id }
        ).success(function (response) {
            if(response){
                var i =  0;
                response.forEach(function(item){
					if(item.senderId._id!=localStorageService.get('authorizationData').userData._id){
						_acquaintances.list[item.senderId._id] = {
						    'userData': item.senderId,
						    'offer' : { 
						        'sender': true,
						        'confirmed':item.offer.confirmed
						    }
						};
					}else{
					    _acquaintances.list[item.receiverId._id] = {
						    'userData': item.receiverId,
						    'offer' : { 
						        'sender': false,
						        'confirmed':item.offer.confirmed
						    }
						};
					}
					i++;
					if(i==response.length){
					    _acquaintances.isLoaded = true;
					   // localStorageService.set('acquaintances', _acquaintances);
					   // console.log(localStorageService.get('acquaintances'));
					}
				});
            }
            deferred.resolve(true);
        }).error(function (err, status) {
            deferred.reject(err);
        });

        return deferred.promise;

    };

    var _removeUserFromList = function () {
        // localStorageService.remove('authorizationData');
    };

    var _setNewUserToList = function (userId,reject) {
        var deferred = $q.defer();
        $http.post(API+'users/acquaintances/add',{
			userId: userId,
			reject: reject
		}).success(function(response){
		    if(response.msg == 'requestSent'){
		        _acquaintances.list[userId] = response.data;
		    }
		    if(response.msg == 'deleted'){
		        if(_acquaintances.list[userId]){
		            delete _acquaintances.list[userId];   
		        }
		    }
		    if(response.msg == 'added'){
		        _acquaintances.list[userId] = response.data;
		    }
		  //  localStorageService.set('acquaintances', _acquaintances);
		    deferred.resolve(response);
		}).error(function(err){
			// alert(err);
			deferred.reject(err);
		});
		
		return deferred.promise;
    }

    // var _getAcquaintances = function () {
    //     var locAcquaintances = localStorageService.get('acquaintances');
    //     if (locAcquaintances)
    //     {
    //         _acquaintances.isLoaded = locAcquaintances.isLoaded;
    //         _acquaintances.list = locAcquaintances.list;
    //     }
    // }
    
    // var _getAcquaintanceStatus = function (userId) {
    //     var deferred = $q.defer();
    //     if(_acquaintances[userId]){
    //     	deferred.resolve(_acquaintances[userId]);
    //     }else{
    //     	deferred.resolve({'msg':);
    //     }
        
    //     return deferred.promise;
    // }


    // acquaintancesFactory.setAcquaintances = _setAcquaintances;
    acquaintancesFactory.removeUserFromList = _removeUserFromList;
    acquaintancesFactory.setNewUserToList = _setNewUserToList;
    acquaintancesFactory.getAcquaintances = _getAcquaintances;
    // acquaintancesFactory.getAcquaintanceStatus = _getAcquaintanceStatus;
    acquaintancesFactory.acquaintances = _acquaintances;

    return acquaintancesFactory;
});