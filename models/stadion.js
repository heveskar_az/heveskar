var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stadionSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    // username: {type: String, trim : true},
    // password: {type: String, trim : true},
    address: {type: String, trim : false},
    phone: {type: String, trim : false},
    mobile: {type: String, trim : false},
    name: {type: String, trim : false},
    controllerName: {type: String, trim : false},
    // avatar: {type: String, trim : true},
    stadiums : [{type: Schema.Types.ObjectId}]
    
    // id: {type: Number},
    // online: {type: Number},
    // lastDate:{type:Date}
});

module.exports = mongoose.model('stadion',stadionSchema,'stadion');

