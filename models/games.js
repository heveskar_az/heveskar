var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gamesSchema = new Schema({
	// _id         : {type: Schema.ObjectId},
	team		: {type: Number},
    teamId 		: {type: Schema.Types.ObjectId,ref:'teams'},
    division 	: {type: Number},
    countPlayers: {type: Number},
    plan		: {
    	type    : {type: Number},
    	winner	: {type: Number},
    	loser	: {type: Number}
    },
    stadium		: {type: String, trim: true},
    date 		: {type: Date},
    created		: {
		by		: {type: Schema.Types.ObjectId,ref:'users'},
    	date	: {type: Date, default: Date.now}
    },
    connected   : [
                  {type: Schema.Types.ObjectId,ref:'users'}
    ],
    opponent    : {type: Boolean, default: false },
    oppTeamId   : {type: Schema.Types.ObjectId,ref:'teams'},
    status 		: {type: Number, default: 0},

    comments    : [{type: Schema.Types.ObjectId,ref:'comments'}]

});

module.exports = mongoose.model('games',gamesSchema);