var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var collection = new Schema({
    // _id: {type : Schema.ObjectId},
	name: { type: String },
    text: { type: String },
    state: { type: String }
});

module.exports = mongoose.model('notificationtypes',collection);

