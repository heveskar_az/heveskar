var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var teamsUsersSchema = new Schema({
	// _id: {type : Schema.ObjectId},
	teamId: {type: Schema.Types.ObjectId,ref:'teams'},
    userId: {type: Schema.Types.ObjectId,ref:'users'},
    captain: {type: Boolean},
    offer: {
        from: {type: Boolean}, // true=> from team; false=> from player
        by: {type: Schema.Types.ObjectId,ref:'users'},
        date:{type: Date},
        confirmed: {type: Boolean},
        confirmDate: {type: Date}
    }
});

module.exports = mongoose.model('teamsusers',teamsUsersSchema);

