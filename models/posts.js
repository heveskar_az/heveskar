var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    type: {type: Schema.Types.ObjectId,ref: 'posttypes'},
	userId: {type: Schema.Types.ObjectId,ref:'users'},
	teamId: {type: Schema.Types.ObjectId,ref:'teams'},
	gameId: {type: Schema.Types.ObjectId,ref:'games'},
	to: [{type: Schema.Types.ObjectId,ref:'users'}],
	seenBy: [{type: Schema.Types.ObjectId,ref:'users'}],
	text: {type: String },
	imgs: [{type: String }],
    date:{type: Date, default: Date.now}
});

module.exports = mongoose.model('posts',schema);

