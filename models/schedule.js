var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var scheduleSchema = new Schema({
	stad_id: {type : Schema.Types.ObjectId},
    date: {type: String,trim: true},
    time: {type: String,trim: true},
    stadium: {type: Schema.Types.ObjectId},
    status: {type: Number,trim: true},
    client_id: {
    	ID: {type: Number},
    	name: {type: String, trim : true},
    	email: {type: String, trim : true},
    	phone: {type: String, trim : true}
    },
    price: {type: Number,trim: true},
    payment: {type: Number,trim: true},
    createdDate: {type:Date},
    updatedDate: {type:Date}
    // payme: {type: String, trim : true},
    // id: {type: Number},
    // lastDate:{type:Date}
});

module.exports = mongoose.model('schedule',scheduleSchema,'schedule');

