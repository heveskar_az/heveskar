var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var collection = new Schema({
    conversationId: { type: Schema.Types.ObjectId,ref: 'conversations'},
    sender 	: { type: Schema.Types.ObjectId,ref:'users'},
    message: { type: String },
    seenBy: [{type: Schema.Types.ObjectId,ref:'users'}],
    date  : {type: Date, default: Date.now},
});

module.exports = mongoose.model('messages',collection);

