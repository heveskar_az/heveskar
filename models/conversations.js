var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var collection = new Schema({
    users 	: [{type: Schema.Types.ObjectId,ref:'users'}],
    date   		: {type: Date, default: Date.now},
    lastMessage: {type: Schema.Types.ObjectId, ref:'messages' }
});

module.exports = mongoose.model('conversations',collection);