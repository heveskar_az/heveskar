var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var collection = new Schema({
    // _id: {type : Schema.ObjectId},
	name: { type: String },
    text: { type: String },
    icon: { type: String }
});

module.exports = mongoose.model('posttypes',collection);

