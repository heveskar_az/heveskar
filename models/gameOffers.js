var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gameOffersSchema = new Schema({
	gameId: {type: Schema.Types.ObjectId,ref:'games'},

	senderId: {type: Schema.Types.ObjectId,ref:'users'},
	senderTeamId: {type: Schema.Types.ObjectId,ref:'teams'},
	text: {type: String},
    offer: {
        date:{type: Date, default: Date.now},
        confirmed: {type: Boolean, default: false},
        confirmDate: {type: Date}
    }
});

module.exports = mongoose.model('gameoffers',gameOffersSchema);

