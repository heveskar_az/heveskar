var mongoose = require('mongoose'),
    mongoosastic = require('mongoosastic');
var Schema = mongoose.Schema;

var collection = new Schema({
	// _id: {type : Schema.ObjectId},
	senderId: {type: Schema.Types.ObjectId,ref:'users',es_indexed:true},
    receiverId: {type: Schema.Types.ObjectId,ref:'users',es_indexed:true},
    offer: {
        date:{type: Date},
        confirmed: {type: Boolean},
        confirmDate: {type: Date}
    }
});

collection.plugin(mongoosastic);

var Book = mongoose.model('acquaintances', collection)
  , stream = Book.synchronize()
  , count = 0;
 
stream.on('data', function(err, doc){
  count++;
});
stream.on('close', function(){
  console.log('indexed ' + count + ' documents!');
});
stream.on('error', function(err){
  console.log(err);
});

module.exports = mongoose.model('acquaintances',collection);

