var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var notifications = new Schema({
	itemId: {type: Schema.Types.ObjectId},
    type: {type: Schema.Types.ObjectId,ref: 'notificationtypes'},
	senderId: {type: Schema.Types.ObjectId,ref:'users'},
	to: [{type: Schema.Types.ObjectId,ref:'users'}],
	seenBy: [{type: Schema.Types.ObjectId,ref:'users'}],
    date:{type: Date, default: Date.now}
});

module.exports = mongoose.model('notifications',notifications);

