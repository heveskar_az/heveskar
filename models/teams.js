var mongoose = require('mongoose'),
    mongoosastic = require('mongoosastic');
var Schema = mongoose.Schema;

var collection = new Schema({
	// _id: {type : Schema.ObjectId},
	name: {type: String, trim : true,es_boost: 2.0},
    createdBy: {type: Schema.Types.ObjectId,ref:'users'},
    members: [{type: Schema.Types.ObjectId,ref:'users'}],
    img: {type: String, trim : true},
    statistics: {
        played: {type: Number},
        won: {type: Number},
        lost: {type: Number},
        draw: {type: Number},
        points: {type: Number}
    },
    location: { type: [Number], es_type: 'geo_point', es_indexed: true, index: true },  
    address: {type: String, trim : true},
    createdDate:{type:Date}
});

collection.plugin(mongoosastic);

module.exports = mongoose.model('teams',collection);

