var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var stadiumsSchema = new Schema({
    _id: {type: Schema.Types.ObjectId},
    name: {type: String, trim : true},
    type: {type: Boolean},
    stad_id: {type: Schema.Types.ObjectId,ref:'stadion'},
    schedule: {},
    images: {},
    status: {type: Boolean},
    // mobile: {type: String, trim : false},
    // avatar: {type: String, trim : true},
    // stadiums : [Schema.Types.ObjectId],
    
    // id: {type: Number},
    // online: {type: Number},
    // lastDate:{type:Date}
});

module.exports = mongoose.model('stadiums',stadiumsSchema,'stadiums');

