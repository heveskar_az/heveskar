var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var commentsSchema = new Schema({
	senderId	: {type: Schema.Types.ObjectId,ref:'users'},
	comment 	: {type: String},
    date   		: {type: Date, default: Date.now},
    replies 	: [{type: Schema.Types.ObjectId,ref:'comments'}],
    lastUpdate	: {type: Date, default: Date.now}
});

module.exports = mongoose.model('comments',commentsSchema);

