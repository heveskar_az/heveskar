var express = require('express');
var socket = require('socket.io');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');

var jwt    = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config = require('./config'); // get our config file

var mongoose = require('mongoose');
var fs = require('fs');
var app = express();
var server = require('http').Server(app);
var io = socket.listen(server);


var acquaintancesModel = require('./models/acquaintances.js');
var postTypesModel = require('./models/post-type.js');

//mongoose connection 
	mongoose.connect(config.database);
	//models
		var usersModel = require('./models/users.js');
		// var teamsModel = require('./models/teams.js');
		// var teamsUsersModel = require('./models/teamsUsers.js');
		// var gamesModel = require('./models/games.js');
		// var acquaintancesModel = require('./models/acquaintances.js');
		// var stadionModel = require('./models/stadion.js');
		// var stadiumsModel = require('./models/stadiums.js');
		// var scheduleModel = require('./models/schedule.js');
//
app.set('superSecret', config.secret);

// app.use(morgan('dev'));
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(require('less-middleware')(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(bodyParser.json({limit: '10mb'}));
app.use(bodyParser.urlencoded({ extended: true },{limit: '10mb'}));

//enable CORS
	app.all('/*', function(req, res, next) {
	  // CORS headers
	  res.header("Access-Control-Allow-Origin", "*"); // restrict it to the required domain
	  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	  // Set custom headers for CORS
	  res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
	  if (req.method == 'OPTIONS') {
	    res.status(200).end();
	  } else {
	    next();
	  }
	});
//

// get an instance of the router for api routes
	var apiRoutes = express.Router(); 
	app.use('/api', apiRoutes);
	
	//important gets and posts
		apiRoutes.post('/login',function(req, res){
			usersModel.findOne({ 'email': req.body.username},'username _id name img password address location',function (err, user) {
			  	if (err) throw err;
			  	if(!user){
			  		return res.status(401).send({ success: false, message: 'Authentication failed. User not found.' });
			  	}else if(user){
		  			if(user.password != req.body.password){
						return res.status(401).send({ success: false, message: 'Authentication failed. Wrong password.' });
		  			}else{
		  				// if user is found and password is right
				        // create a token
				        user.password = '';
				        var token = jwt.sign({'user':user,'isAuthenticated': true}, app.get('superSecret'), {
				          expiresIn: 3600*24 // expires in 24 hours
				        });
				
				        // return the information including token as JSON
				        res.json({
				          success: true,
				          message: 'Enjoy your token!',
				          token: token,
				          data:{
				          	'img' : user.img,
				          	'username' : user.username,
				          	'name' : user.name,
				          	'_id' : user._id,
				          	'location' : user.location,
				          	'address' : user.address
				          }
				        });
		  			}
			  	}
			})
		});
		apiRoutes.get('/logout', function(req, res) {
			// req.logout();
			// res.redirect('/');
			res.send('ok');
		});
	//
	//socket settings
		apiRoutes.use(function(req, res, next) { req.io = io; next();	});
		var users = [];
		
		//Important functions
			module.exports.prepareEmit = function(data,action){
				console.log('okdsaasa');
		        if (users['u_'+data.opponentId]) {
		            for(socketId in users['u_'+data.opponentId]){
		                users['u_'+data.opponentId][socketId].emit(action, data);
		            }
		        }
		    }
	        module.exports.prepareEmitMultipleUsers = function(data,action){
		        data.acquaintances.forEach(function(element){
		            if (users['u_'+element]) {
		                for(socketId in users['u_'+element]){
		                    users['u_'+element][socketId].emit(action,data.data);
		                }
		            }
		        });
		    }
			function count(arr){
		        var i = 0;
		        for (var id in arr) {
		            i++;
		        }
		        return i;
		    }
		//
		io.sockets.on('connection', function (socket) {
			var userId = null; // istifadeci id-i
		    var token = socket.handshake.query.token; // istifadeci id-i
		    var u_userId = null;
	        if (token) {
			    // verifies secret and checks exp
			    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
			      	if (err) {
			        	// return res.status(403).send({ success: false, message: 'Failed to authenticate token.' });    
			        	console.log('Failed to authenticate token');
			      	} else {
			        	// if everything is good, save to request for use in other routes
			        	// req.user = decoded.user;    
			        	// req.isAuthenticated = decoded.isAuthenticated;    
			        	// next();
			        	userId = decoded.user._id;
			        	u_userId = 'u_'+decoded.user._id;
			        	
			        	if (!(users[u_userId])) {
		                    users[u_userId] = [];
		                    users[u_userId][socket.id] = socket;
		
		                    console.log("Connected UserId: " + userId);
		                    io.sockets.emit('set-online', userId);
		                } else {
		                    users[u_userId][socket.id] = socket;
		                }   
			      	}
		    	});
		  	} else {
			    // if there is no token
			    // return an error
			    // return res.status(403).send({ 
			    //     success: false, 
			    //     message: 'No token provided.' 
			    // });
			    console.log('No token provided')
			}
		});
	//
	//route middleware to verify a token
		module.exports.checkUser = function(req, res, next) {
		  // check header or url parameters or post parameters for token
		  var token = req.body.token || req.query.token || req.headers['x-access-token'];
		
		  // decode token
		  if (token) {
		
		    // verifies secret and checks exp
		    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
		      if (err) {
		        return res.status(403).send({ success: false, message: 'Failed to authenticate token.' });    
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.user = decoded.user;    
		        req.isAuthenticated = decoded.isAuthenticated;    
		        next();
		      }
		    });
		
		  } else {
		
		    // if there is no token
		    // return an error
		    return res.status(403).send({ 
		        success: false, 
		        message: 'No token provided.' 
		    });
		  }
		};
		module.exports.acquaintances = function(req, res, next) {
		  	var acquaintances = [];
		  	var query = {
			    "filtered" : {
			        "query" : {
			            "term" : { "offer.confirmed" : true }
			        },
			        "filter" : {
			            "or" : [
			                {
			                    "term" : { "senderId" : req.user._id }
			                },
			                {
			                    "term" : { "receiverId" : req.user._id }
			                }
			            ]
			        }
			    }
			}
			acquaintancesModel.search(query, {}, function(err, users) { /* ... */
		 		var i = 0;
				if(users.hits.hits.length){
					users.hits.hits.forEach(function(user){
						i++;
						if(user._source.senderId == req.user._id){
							acquaintances.push(user._source.receiverId);
						}else{
							acquaintances.push(user._source.senderId);
						}
						if(i == users.hits.hits.length){
							req.acquaintances = acquaintances
			 				next()
						}
					})
				}else{
					req.acquaintances = acquaintances
	 				next()
				}
		 	})
		};
		module.exports.acquaintancesWithRequests = function(req, res, next) {
		  	var acquaintances = [];
		  	var query = {
			    "filtered" : {
			        "filter" : {
			            "or" : [
			                {
			                    "term" : { "senderId" : req.user._id }
			                },
			                {
			                    "term" : { "receiverId" : req.user._id }
			                }
			            ]
			        }
			    }
			}
			acquaintancesModel.search(query, {}, function(err, users) { /* ... */
		 		var i = 0;
				if(users.hits.hits.length){
					users.hits.hits.forEach(function(user){
						i++;
						if(user._source.senderId == req.user._id){
							acquaintances.push(user._source.receiverId);
						}else{
							acquaintances.push(user._source.senderId);
						}
						if(i == users.hits.hits.length){
							req.acquaintances = acquaintances
			 				next()
						}
					})
				}else{
					req.acquaintances = acquaintances
	 				next()
				}
		 	})
		};
		module.exports.posttypes = function(req, res, next) {
		  	var posttypes = [];
			postTypesModel.find()
			.exec(function(err,items){
				var i = 0;
				items.forEach(function(item){
					i++;
					posttypes[item._id] = item;
					if(i == items.length){
						req.posttypes = posttypes;
		 				next()
					}
				})
			});
		};
		module.exports.isGuest = function(req, res, next) {
		  // check header or url parameters or post parameters for token
		  var token = req.body.token || req.query.token || req.headers['x-access-token'];
		
		  // decode token
		  if (token) {
		
		    // verifies secret and checks exp
		    jwt.verify(token, app.get('superSecret'), function(err, decoded) {      
		      if (err) {
		        req.isAuthenticated = false;    
		        next();  
		      } else {
		        // if everything is good, save to request for use in other routes
		        req.user = decoded.user;    
		        req.isAuthenticated = decoded.isAuthenticated;    
		        next();
		      }
		    });
		
		  } else {
		    req.isAuthenticated = false;    
			next();  
		  }
		};
	//
	//routes requieres token
		apiRoutes.use('/index', require('./routes/index'));
		apiRoutes.use('/users', require('./routes/users'));
		apiRoutes.use('/stadion', require('./routes/stadion'));
		apiRoutes.use('/teams', require('./routes/teams'));
		apiRoutes.use('/games', require('./routes/games'));
		apiRoutes.use('/requests', require('./routes/requests'));
		apiRoutes.use('/conversations', require('./routes/conversations'));
		apiRoutes.use('/posts', require('./routes/posts'));
	//
//


// load our public/index.html file
	app.get('*', function(req, res) {
	    res.sendFile(__dirname+'/public/index.html'); 
	});

/********************************/


//checking users status
	setInterval(function () {
	    var existSockets = [];
	    io.sockets.sockets.forEach(function (s) {
	        existSockets.push(s.id)
	    });
	    for (var id in users) {
	        var i = 0;
	        var userIsOnline = false;
	        for(socketId in users[id]){
	            i++;
	            if(i != count(users[id])){
	                if(existSockets.indexOf(users[id][socketId].id) >= 0) {
	                    userIsOnline = true;
	                }else{
	                    delete users[id][socketId];
	                    i--;
	                    userIsOnline = false;
	                }
	            }else{
	                if (existSockets.indexOf(users[id][socketId].id) >= 0) {
	                    userIsOnline = true;
	                }else{
	                    if(userIsOnline != true){
	                        delete users[id];
	                        console.log('- ' + id.split("_")[1] + ' disconnected');
	                        io.sockets.emit('set-offline', id.split("_")[1] ); // broadcast edilir
	                        // Profile.update( { 'online': 0, 'last_time' : sequelize.fn('NOW')},{ where: { 'user_id': parseInt(id.split("_")[1]) } } );
	                    }else{
	                        delete users[id][socketId];
	                        i--;
	                        userIsOnline = false;
	                    }
	                }
	            }
	        }
	    }
	}, 30000);
//


server.listen(8080, function(){ 
    console.log('listening on *:8080');
});